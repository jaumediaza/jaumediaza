/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.stanko;

public final class R {
    private R() {}

    public static final class bool {
        private bool() {}

        public static int isTablet = 0x7f050001;
    }
    public static final class string {
        private string() {}

        public static int last_update_test_preferences = 0x7f150002;
        public static int loading = 0x7f150003;
        public static int no = 0x7f150004;
        public static int ok = 0x7f150005;
        public static int please_wait = 0x7f150006;
        public static int update_test = 0x7f150008;
        public static int yes = 0x7f150009;
        public static int you_are_not_updated_message = 0x7f15000a;
        public static int you_are_not_updated_title = 0x7f15000b;
        public static int you_are_updated_message = 0x7f15000c;
        public static int you_are_updated_title = 0x7f15000d;
    }
}
