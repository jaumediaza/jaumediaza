package com.atendus.permissions;

import com.atendus.permissions.listeners.ButtonListener;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents content for one page of Paper Onboarding
 */
public class AtendusOnboardingPage implements Serializable {

    private String titleText;
    private String descriptionText;
    private int bgColor;
    //private int contentIconRes;
    private int bottomBarIconRes;
    private String buttonText;
    private ButtonListener buttonAction;

    public AtendusOnboardingPage() {
    }

    public AtendusOnboardingPage(
            String titleText,
            String descriptionText,
            int bgColor,
            //int contentIconRes,
            int bottomBarIconRes,
            String buttonText,
            ButtonListener listener) {
        this.bgColor = bgColor;
        //this.contentIconRes = contentIconRes;
        this.bottomBarIconRes = bottomBarIconRes;
        this.descriptionText = descriptionText;
        this.titleText = titleText;
        this.buttonText = buttonText;
        this.buttonAction = listener;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    /*public int getContentIconRes() {
        return contentIconRes;
    }

    public void setContentIconRes(int contentIconRes) {
        this.contentIconRes = contentIconRes;
    }*/

    public int getBottomBarIconRes() {
        return bottomBarIconRes;
    }

    public void setBottomBarIconRes(int bottomBarIconRes) {
        this.bottomBarIconRes = bottomBarIconRes;
    }

    public void setButtonText(String text) {
        this.buttonText = text;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonAction(ButtonListener listener){
        this.buttonAction = listener;
    }

    public ButtonListener getButtonAction() {
        return this.buttonAction;
    }

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AtendusOnboardingPage that = (AtendusOnboardingPage) o;

        if (bgColor != that.bgColor) return false;
        //if (contentIconRes != that.contentIconRes) return false;
        if (bottomBarIconRes != that.bottomBarIconRes) return false;
        if (!Objects.equals(titleText, that.titleText))
            return false;
        return Objects.equals(descriptionText, that.descriptionText);
    }

    @Override
    public int hashCode() {
        int result = titleText != null ? titleText.hashCode() : 0;
        result = 31 * result + (descriptionText != null ? descriptionText.hashCode() : 0);
        result = 31 * result + bgColor;
        //result = 31 * result + contentIconRes;
        result = 31 * result + bottomBarIconRes;
        return result;
    }

    @Override
    public String toString() {
        return "PaperOnboardingPage{" +
                "titleText='" + titleText + '\'' +
                ", descriptionText='" + descriptionText + '\'' +
                ", bgColor=" + bgColor +
                //", contentIconRes=" + contentIconRes +
                ", bottomBarIconRes=" + bottomBarIconRes +
                '}';
    }
}