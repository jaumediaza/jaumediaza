package com.atendus.permissions.listeners;

public interface AtendusOnboardingOnChangeListener {
    void onPageChanged(int oldElementIndex, int newElementIndex);
}