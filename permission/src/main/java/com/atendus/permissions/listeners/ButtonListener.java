package com.atendus.permissions.listeners;

public interface ButtonListener {
    void onClick();
}
