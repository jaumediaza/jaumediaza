package com.atendus.permissions.listeners;

public interface AtendusOnboardingOnRightOutListener {
    void onRightOut();
}