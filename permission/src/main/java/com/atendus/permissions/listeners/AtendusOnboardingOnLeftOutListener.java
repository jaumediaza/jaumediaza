package com.atendus.permissions.listeners;

public interface AtendusOnboardingOnLeftOutListener {
    void onLeftOut();
}