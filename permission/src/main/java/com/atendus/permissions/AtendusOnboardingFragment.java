package com.atendus.permissions;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.atendus.permissions.listeners.AtendusOnboardingOnChangeListener;
import com.atendus.permissions.listeners.AtendusOnboardingOnLeftOutListener;
import com.atendus.permissions.listeners.AtendusOnboardingOnRightOutListener;

import java.util.ArrayList;

/**
 * Ready to use AtendusOnboarding fragment
 */
public class AtendusOnboardingFragment extends Fragment {

    private static final String ELEMENTS_PARAM = "elements";

    private AtendusOnboardingOnChangeListener mOnChangeListener;
    private AtendusOnboardingOnRightOutListener mOnRightOutListener;
    private AtendusOnboardingOnLeftOutListener mOnLeftOutListener;
    private ArrayList<AtendusOnboardingPage> mElements;


    public static AtendusOnboardingFragment newInstance(ArrayList<AtendusOnboardingPage> elements) {
        AtendusOnboardingFragment fragment = new AtendusOnboardingFragment();
        Bundle args = new Bundle();
        args.putSerializable(ELEMENTS_PARAM, elements);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mElements = (ArrayList<AtendusOnboardingPage>) getArguments().get(ELEMENTS_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.onboarding_main_layout, container, false);

        // create engine for onboarding element
        AtendusOnboardingEngine mAtendusOnboardingEngine = new AtendusOnboardingEngine(view.findViewById(R.id.onboardingRootView), mElements, getActivity().getApplicationContext());
        // set listeners
        mAtendusOnboardingEngine.setOnChangeListener(mOnChangeListener);
        mAtendusOnboardingEngine.setOnLeftOutListener(mOnLeftOutListener);
        mAtendusOnboardingEngine.setOnRightOutListener(mOnRightOutListener);

        return view;
    }

    public void setElements(ArrayList<AtendusOnboardingPage> elements) {
        this.mElements = elements;
    }

    public ArrayList<AtendusOnboardingPage> getElements() {
        return mElements;
    }

    public void setOnChangeListener(AtendusOnboardingOnChangeListener onChangeListener) {
        this.mOnChangeListener = onChangeListener;
    }

    public void setOnRightOutListener(AtendusOnboardingOnRightOutListener onRightOutListener) {
        this.mOnRightOutListener = onRightOutListener;
    }

    public void setOnLeftOutListener(AtendusOnboardingOnLeftOutListener onLeftOutListener) {
        this.mOnLeftOutListener = onLeftOutListener;
    }
}