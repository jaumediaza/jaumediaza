package com.atendus.app.utilidades;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.constraint.ConstraintLayout;
import android.util.Log;

import com.atendus.app.ActivityPrincipal;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.clases.ConfiguracionDesconexion;
import com.atendus.app.clases.Desconexion;
import com.atendus.app.clases.NotificacionInfo;
import com.atendus.app.clases.Telefono;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.atendus.app.ActivityPermisos.callPermission;
import static com.atendus.app.ActivityPermisos.contactPermission;

public class PhoneStateManager {

    //Patrón de funcionamiento de singletón
    private static final PhoneStateManager instance = new PhoneStateManager();

    private PhoneStateManager() {
    }

    public static PhoneStateManager getInstance() {
        return instance;
    }


    private ConfiguracionDesconexion desconexion = new ConfiguracionDesconexion();
    private boolean isLocked = false;
    private boolean isDesconectado = false;
    private Activity currentActivity;
    private ConstraintLayout ly_main;

    private boolean isDeviceAdmin = false;
    private List<AplicacionPermitida> appsPermitidas;
    private List<String> appsPermitidasPaquete = new ArrayList<>();
    private List<Telefono> tfnosPermitidos = new ArrayList<>();
    private List<String> tfnosPermitidosNumeros = new ArrayList<>();
    private NotificacionInfo.NotificacionInfoList notificacioines = new NotificacionInfo.NotificacionInfoList();
    private ActivityPrincipal activityPrincipal;
    private long millisUntilFinished;
    private boolean fromProgramarDesconexion = false;

    //indica si la desconexión se ha activado desde el backend por otro usuario y por tanto no puede pararse
    private boolean editable = true;

    public ActivityPrincipal getActivityPrincipal() {
        return activityPrincipal;
    }

    public void setActivityPrincipal(ActivityPrincipal activityPrincipal) {
        this.activityPrincipal = activityPrincipal;
    }

    public ConfiguracionDesconexion getDesconexion() {
        return desconexion;
    }

    public void setDesconexion(ConfiguracionDesconexion desconexion) {
        this.desconexion = desconexion;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public boolean isDesconectado() {
        return isDesconectado;
    }

    public void setDesconectado(boolean desconectado) {
        isDesconectado = desconectado;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public ConstraintLayout getLy_main() {
        return ly_main;
    }

    public void setLy_main(ConstraintLayout ly_main) {
        this.ly_main = ly_main;
    }

    public boolean isDeviceAdmin() {
        return isDeviceAdmin;
    }

    public void setDeviceAdmin(boolean deviceAdmin) {
        isDeviceAdmin = deviceAdmin;
    }

    public long getMillisUntilFinished() {
        return millisUntilFinished;
    }

    public void setMillisUntilFinished(long millisUntilFinished) {
        this.millisUntilFinished = millisUntilFinished;
    }

    public boolean isFromProgramarDesconexion() {
        return fromProgramarDesconexion;
    }

    public void setFromProgramarDesconexion(boolean fromProgramarDesconexion) {
        this.fromProgramarDesconexion = fromProgramarDesconexion;
    }

    public void guardarDatosEnAplicacion(Context c) {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(c);
        app.setDesconexionActual(desconexion);
        app.setModoDesconectaActivado(isDesconectado);
        app.setDeviceAdmin(isDeviceAdmin);
        app.setAppsPermitidas(appsPermitidas);
        app.setMillisUntilFinished(millisUntilFinished);
        app.setNotificacioines(notificacioines);
        //app.setDesconexionEditable(editable);

        app.guardarEnPreferencias(c);
    }

    public void cargarDatosDeAplicacion(Context c) {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(c);
        desconexion = app.getDesconexionActual();
        isDesconectado = app.isModoDesconectaActivado();
        isDeviceAdmin = app.isDeviceAdmin();
        tfnosPermitidos = app.getTfnosPermitidos();
        millisUntilFinished = app.getMillisUntilFinished();
        notificacioines = app.getNotificacioines();
        //editable = app.isDesconexionEditable();


    }

    public void cargarAplicacionesInstaladas() {
        new Thread() {
            @Override
            public void run() {

                appsPermitidas = getAllInstalledApps();
                ordenarAplicacionesPermitidas();
                for (int i = 0; i < appsPermitidas.size(); ++i) {
                    appsPermitidas.get(i).setId(i);
                }
            }

        }.start();
    }


    public void cargarListinTelefonicoThread(final ContentResolver cr, final Context c) {
        new Thread() {
            @Override
            public void run() {

                cargarListinTelefonico(cr, c);

            }
        }.start();
    }

    private boolean isUserApp(ApplicationInfo ai) {
        if (ai.sourceDir.startsWith("/data/app/")) {
            return true;
        } else
            return ai.packageName.contains("dialer") || ai.packageName.contains("call") || ai.packageName.contains("message") || ai.packageName.contains("sms") || ai.packageName.contains("messaging");
    }

    private List<AplicacionPermitida> getAllInstalledApps() {
        List<AplicacionPermitida> appInfo = new ArrayList<>();

        try {
            //get a list of installed apps.
            PackageManager pm = null;
            if (currentActivity != null)
                pm = currentActivity.getPackageManager();
            List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

            appInfo.clear();

            Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(currentActivity);
            List<AplicacionPermitida> appsDePreferencias = app.getAppsPermitidas();

            if (appsDePreferencias.size() > 0) {
                for (ApplicationInfo packageInfo : packages) {

                    if (isUserApp(packageInfo) && !packageInfo.packageName.contains("contacts") && !packageInfo.packageName.equals("com.atendus.app")) {

                        AplicacionPermitida newapp = new AplicacionPermitida(packageInfo
                                .loadLabel(pm).toString(), false, false, true);
                        newapp.setDrawable(packageInfo.loadIcon(pm));
                        newapp.setPackageName(packageInfo.packageName);
                        for (int i = 0; i < appsDePreferencias.size(); ++i) {
                            if (newapp.getPackageName().equals(appsDePreferencias.get(i).getPackageName())) {
                                newapp.setPermitida(appsDePreferencias.get(i).isPermitida());
                                newapp.setUsoDuranteDesconecta(appsDePreferencias.get(i).isUsoDuranteDesconecta());
                                newapp.setSeguimientoDeUso(appsDePreferencias.get(i).isSeguimientoDeUso());
                                i = appsDePreferencias.size();
                            }
                        }
                        appInfo.add(newapp);
                        if (newapp.isPermitida())
                            appsPermitidasPaquete.add(newapp.getPackageName());

                    }


                }
            } else {

                for (ApplicationInfo packageInfo : packages) {
                    try {
                        if (isUserApp(packageInfo) && !packageInfo.packageName.equals("com.atendus.app")) {

                            AplicacionPermitida newapp = new AplicacionPermitida(packageInfo
                                    .loadLabel(pm).toString(), false, false, true);
                            newapp.setDrawable(packageInfo.loadIcon(pm));
                            newapp.setPackageName(packageInfo.packageName);
                            appInfo.add(newapp);

                        }

                        Log.d(TAG, "Source dir : " + currentActivity.getPackageManager().getApplicationIcon(packageInfo.packageName));

                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return appInfo;
    }

    public boolean isAppInfoListLoaded() {
        return appsPermitidas != null;
    }

    public List<AplicacionPermitida> getAppsPermitidas() {
        if (appsPermitidas == null)
            return new ArrayList<>();
        return appsPermitidas;
    }

    public List<String> getAppsPermitidasPaquete() {
        return appsPermitidasPaquete;
    }

    public void acalizarAppsPermitidasPaquete() {
        appsPermitidasPaquete.clear();
        for (int i = 0; i < appsPermitidas.size(); ++i) {
            if (appsPermitidas.get(i).isUsoDuranteDesconecta()) {
                appsPermitidasPaquete.add(appsPermitidas.get(i).getPackageName());
            }
        }
    }

    public void cargarListinTelefonico(ContentResolver cr, Context c) {
        if (contactPermission) {
            tfnosPermitidos = new ArrayList<>();
            Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            if (phones != null) {
                while (phones.moveToNext()) {
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("\\s+", "");

                    Telefono tfno = new Telefono(name, phoneNumber, false);
                    tfnosPermitidos.add(tfno);
                }
                phones.close();// close cursor
            }

            ordenarListinTelefonico();
            Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(c);
            tfnosPermitidosNumeros = new ArrayList<>();

            for (int i = 1; i < tfnosPermitidos.size(); ++i) {
                try {
                    tfnosPermitidos.get(i - 1).setId(i - 1);
                    if (tfnosPermitidos.get(i - 1).getNumero().equals(tfnosPermitidos.get(i).getNumero())) {
                        tfnosPermitidos.remove(i);
                        i--;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (app.getTfnosPermitidos().size() > 0) {
                for (int i = 0; i < tfnosPermitidos.size(); ++i) {
                    for (int j = 0; j < app.getTfnosPermitidos().size(); ++j) {
                        if (tfnosPermitidos.get(i).getNumero().equals(app.getTfnosPermitidos().get(j).getNumero())) {
                            if (app.getTfnosPermitidos().get(j).isPermitido()) {
                                tfnosPermitidos.get(i).setPermitido(app.getTfnosPermitidos().get(j).isPermitido());
                                tfnosPermitidosNumeros.add(tfnosPermitidos.get(i).getNumero());
                            }
                            j = app.getTfnosPermitidos().size();
                        }
                    }
                    tfnosPermitidos.get(i).setId(i);
                }

            }
        }
    }

    private void ordenarListinTelefonico() {
        Collections.sort(tfnosPermitidos, new Comparator<Telefono>() {
            public int compare(Telefono v1, Telefono v2) {
                return v1.getNombre().compareTo(v2.getNombre());
            }
        });

    }

    private void ordenarAplicacionesPermitidas() {
        try {
            Collections.sort(appsPermitidas, new Comparator<AplicacionPermitida>() {
                public int compare(AplicacionPermitida v1, AplicacionPermitida v2) {
                    if (v1 == null) {
                        if (v2 == null)
                            return 0;
                        return -1;
                    } else {
                        return v1.getAppLabel().compareTo(v2.getAppLabel());
                    }
                }
            });
        } catch (Exception e) {

        }
    }

    public List<Telefono> getTfnosPermitidos() {
        return tfnosPermitidos;
    }

    public List<String> getTfnosPermitidosNumeros() {
        return tfnosPermitidosNumeros;
    }

    public void updateTelefonosPermitidos() {
        tfnosPermitidosNumeros = new ArrayList<>();
        if (PhoneStateManager.getInstance().getTfnosPermitidos().size() > 0) {
            for (int i = 0; i < tfnosPermitidos.size(); ++i) {
                for (int j = 0; j < PhoneStateManager.getInstance().getTfnosPermitidos().size(); ++j) {
                    if (tfnosPermitidos.get(i).getNumero().equals(PhoneStateManager.getInstance().getTfnosPermitidos().get(j).getNumero())) {
                        if (PhoneStateManager.getInstance().getTfnosPermitidos().get(j).isPermitido()) {
                            tfnosPermitidos.get(i).setPermitido(PhoneStateManager.getInstance().getTfnosPermitidos().get(j).isPermitido());
                            tfnosPermitidosNumeros.add(tfnosPermitidos.get(i).getNumero());
                        }
                        j = PhoneStateManager.getInstance().getTfnosPermitidos().size();
                    }
                }
                tfnosPermitidos.get(i).setId(i);
            }

        }
    }

    public NotificacionInfo.NotificacionInfoList getNotificaciones() {
        return notificacioines;
    }


    public void guardarDatosDesconexion(Context c) {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(c);

        Desconexion desconexion = new Desconexion(
                getDesconexion().getlDate(),
                Calendar.getInstance().getTime().getTime(),
                notificacioines,
                getDesconexion().isBloquearNotificaciones(),
                getDesconexion().isBloquearLlamadas()
        );

        app.getDesconexiones().add(desconexion);
        app.guardarEnPreferencias(c);
    }

    public void refrescarEstadisticasDeUso(Context context) {
        TresAndroides.getDailyStatistics(context);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
