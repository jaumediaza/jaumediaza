package com.atendus.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.atendus.app.utilidades.DatosUsoManager;

import java.util.concurrent.TimeUnit;



public class ActivityAvisoEstadisticas extends Activity {

    public static int isDesbloqueos = 1;
    public static boolean fromService = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
      //finish();
        super.onCreate(savedInstanceState);
        if (!fromService){
            Intent intn = new Intent(ActivityAvisoEstadisticas.this, ActivityPrincipal.class);
            startActivity(intn);
        }else{
            fromService = false;
        }
        setContentView(R.layout.activity_uso_estadisticas);

        TextView txtTitulo = findViewById(R.id.txtTitulo);
        TextView txtContenido = findViewById(R.id.txtContenido);
        if (isDesbloqueos == 1) {
            txtTitulo.setText(getString(R.string.titulo_alerta_desbloqueos) );
            String uso = getString(R.string.mensaje_alerta_desbloqueos1) + " " +
                    DatosUsoManager.getInstance().getAjustes().getDesbloqueos()
                    +" "+ getString(R.string.mensaje_alerta_desbloqueos2);
            txtContenido.setText(uso);
        }else if (isDesbloqueos == 2){
            txtTitulo.setText(getString(R.string.titulo_alerta_uso));
            long millis = DatosUsoManager.getInstance().getAjustes().getUsageTime();
            String uso = getString(R.string.mensaje_alerta_uso1) + " "+ TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                    (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.MILLISECONDS.toHours(millis)*60) + "m"
                    +" " + getString(R.string.mensaje_alerta_uso2);
            txtContenido.setText(uso);
        }else{
            long millis = DatosUsoManager.getInstance().getAjustes().getUsageTime();
            String str = "Uso del terminal: " + TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                    (TimeUnit.MILLISECONDS.toMinutes(millis) -
                            TimeUnit.MILLISECONDS.toHours(millis)*60) + "m" + "\nDesbloqueos: "
                    + DatosUsoManager.getInstance().getAjustes().getDesbloqueos() + "\n" +
                    getString(R.string.mensaje_alerta_uso2);
            txtTitulo.setText(getString(R.string.titulo_alerta_rsumen));
            txtContenido.setText(str);

        }

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intn = new Intent(ActivityAvisoEstadisticas.this, ActivityPrincipal.class);
                startActivity(intn);
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        finish();
        //finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //finish();
        finish();
    }


}
