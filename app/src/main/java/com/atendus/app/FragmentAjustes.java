package com.atendus.app;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.atendus.app.clases.Ajustes;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.utilidades.CustomViewPager;
import com.atendus.app.utilidades.DatosUsoManager;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.ViewPagerManager;

import java.util.Calendar;

import spencerstudios.com.bungeelib.Bungee;


public class FragmentAjustes extends Fragment{

    private View view;
    private Context context;

    public Switch sw_notificaciones;
    public Switch sw_llamadas;
    private Switch sw_uso_movil;
    private Switch sw_desbloqueo_pantalla;
    private Switch sw_seguimiento_uso;
    private Switch sw_resumen_diario;

    private boolean aux = true;
    private boolean aux2 = true;

    private static CustomViewPager mViewPager;
    private Aplicacion app;

    public FragmentAjustes() {}


    public static FragmentAjustes newInstance(CustomViewPager pager) {
        FragmentAjustes fragmentFirst = new FragmentAjustes();
        Bundle args = new Bundle();
       // args.putInt("someInt", ActivityPrincipal.kAjustes);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        mViewPager = pager;
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ajustes, container, false);

        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        sw_notificaciones.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones());
        sw_llamadas.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas());
    }



    public void init(){

        sw_notificaciones = view.findViewById(R.id.sw_notificaciones);
        sw_llamadas = view.findViewById(R.id.sw_llamadas);
        sw_uso_movil = view.findViewById(R.id.sw_uso_movil);
        sw_desbloqueo_pantalla = view.findViewById(R.id.sw_desbloqueo_pantalla);
        sw_seguimiento_uso = view.findViewById(R.id.sw_seguimiento_uso);
        sw_resumen_diario = view.findViewById(R.id.sw_resumen_diario);

        final TextView txt_uso_movil = view.findViewById(R.id.txt_uso_movil );
        final TextView txt_desbloqueo_pantalla = view.findViewById(R.id.txt_desbloqueo_pantalla);
        final TextView txt_resumen_diario = view.findViewById(R.id.txt_resumen_diario);


        app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);


        sw_notificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones()){
                    PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(true);
                    sw_notificaciones.setChecked(true);
                    ViewPagerManager.getInstance().fragmentDesconecta.notificaciones.
                            setImageDrawable(context.getDrawable(R.drawable.notificaciones_on));
                }else{
                    PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(false);
                    sw_notificaciones.setChecked(false);
                    ViewPagerManager.getInstance().fragmentDesconecta.notificaciones.
                            setImageDrawable(context.getDrawable(R.drawable.notificaciones_off));
                }
                PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);
            }
        });
        sw_llamadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas()){
                    PhoneStateManager.getInstance().getDesconexion().setBloquearLlamadas(true);
                    sw_llamadas.setChecked(true);
                    ViewPagerManager.getInstance().fragmentDesconecta.llamadas.
                            setImageDrawable(context.getDrawable(R.drawable.llamadas_on));
                }else{
                    PhoneStateManager.getInstance().getDesconexion().setBloquearLlamadas(false);
                    sw_llamadas.setChecked(false);
                    ViewPagerManager.getInstance().fragmentDesconecta.llamadas.
                            setImageDrawable(context.getDrawable(R.drawable.llamadas_off));
                }
                PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);
            }
        });
        sw_uso_movil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sw_uso_movil.isChecked()){
                    DatosUsoManager.getInstance().getAjustes().setAlertaUsoMovil(true);
                }else{
                    DatosUsoManager.getInstance().getAjustes().setAlertaUsoMovil(false);
                }
                DatosUsoManager.getInstance().saveInPrefs(context);
            }
        });
        sw_desbloqueo_pantalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sw_desbloqueo_pantalla.isChecked()){
                    DatosUsoManager.getInstance().getAjustes().setAlertaDesbloqueoPantalla(true);
                }else{
                    DatosUsoManager.getInstance().getAjustes().setAlertaDesbloqueoPantalla(false);
                }
                DatosUsoManager.getInstance().saveInPrefs(context);
            }
        });
        sw_seguimiento_uso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sw_seguimiento_uso.isChecked()){
                    app.getAjustes().setSeguimientoUso(true);
                }else{
                    app.getAjustes().setSeguimientoUso(false);
                }
            }
        });
        sw_resumen_diario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sw_resumen_diario.isChecked()){
                    DatosUsoManager.getInstance().getAjustes().setNotificarResumenDiario(true);
                }else{
                    DatosUsoManager.getInstance().getAjustes().setNotificarResumenDiario(false);
                }
                DatosUsoManager.getInstance().saveInPrefs(context);
            }
        });

        LinearLayout ly_ajuste_notif = view.findViewById(R.id.ly_ajuste_notif);
        LinearLayout ly_ajuste_llamada = view.findViewById(R.id.ly_ajuste_llamada);

        ly_ajuste_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  mViewPager.setCurrentItem(ActivityPrincipal.kNotificaciones);
            }
        });

        ly_ajuste_llamada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityTelefonosDesconecta.class);
                context.startActivity(intent);
                Bungee.fade(context);
            }
        });

        LinearLayout ly_ajuste_uso_movil = view.findViewById(R.id.ly_ajuste_uso_movil);
        LinearLayout ly_ajuste_resumen_diario = view.findViewById(R.id.ly_ajuste_resumen_diario);

        ly_ajuste_uso_movil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalender = Calendar.getInstance();
                int hour = DatosUsoManager.getInstance().getAjustes().getUsoMovil().hh;
                int minute = DatosUsoManager.getInstance().getAjustes().getUsoMovil().mm;

                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);

                            DatosUsoManager.getInstance().getAjustes().getUsoMovil().hh = hourOfDay;
                            DatosUsoManager.getInstance().getAjustes().getUsoMovil().mm = minute;
                            DatosUsoManager.getInstance().saveInPrefs(context);
                            txt_uso_movil.setText(intTiempoString(hourOfDay, minute));
                            //sw_uso_movil.setChecked(true);

                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
                timePickerDialog.setTitle(context.getString(R.string.alerta_uso_movil));
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });

        ly_ajuste_resumen_diario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalender = Calendar.getInstance();
                int hour = DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().hh;
                int minute = DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().mm;

                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);

                            DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().hh = hourOfDay;
                            DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().mm = minute;
                            txt_resumen_diario.setText(intTiempoString(hourOfDay, minute));
                            DatosUsoManager.getInstance().saveInPrefs(context);
                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
                timePickerDialog.setTitle(context.getString(R.string.resumen_diario));
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });

        LinearLayout ly_ajuste_desbloqueo_pantalla = view.findViewById(R.id.ly_ajuste_desbloqueo_pantalla);

        ly_ajuste_desbloqueo_pantalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final NumberPicker numberPicker = new NumberPicker(context);
                numberPicker.setMaxValue(999);
                numberPicker.setMinValue(1);
                numberPicker.setValue(app.getAjustes().getAvisarDesbloqueos());


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(context.getString(R.string.alerta_desbloqueo));
                builder.setPositiveButton(context.getString(R.string.aceptar), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String str = String.valueOf(numberPicker.getValue()) + " " +context.getString(R.string.veces);
                        txt_desbloqueo_pantalla.setText(str);
                        //sw_desbloqueo_pantalla.setChecked(true);
                       DatosUsoManager.getInstance().getAjustes().
                                setAvisarDesbloqueos(numberPicker.getValue());
                        DatosUsoManager.getInstance().saveInPrefs(context);
                    }
                });
                builder.setNegativeButton(context.getString(R.string.cancelar), new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.setView(numberPicker);
                builder.show();
            }
        });

        LinearLayout ly_ajuste_seguimiento_uso = view.findViewById(R.id.ly_ajuste_seguimiento_uso);
        ly_ajuste_seguimiento_uso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityAppsSeguimientoUso.class);
                context.startActivity(intent);
                Bungee.fade(context);
                //sw_seguimiento_uso.setChecked(true);
            }
        });


        PhoneStateManager.getInstance().cargarDatosDeAplicacion(context);
        Ajustes ajustes = app.getAjustes();
        sw_notificaciones.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones());
        sw_llamadas.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas());
        sw_uso_movil.setChecked(ajustes.isAlertaUsoMovil());
        sw_desbloqueo_pantalla.setChecked(ajustes.isAlertaDesbloqueoPantalla());
        sw_seguimiento_uso.setChecked(ajustes.isSeguimientoUso());
        sw_resumen_diario.setChecked(ajustes.isNotificarResumenDiario());


        txt_uso_movil.setText(intTiempoString(ajustes.getUsoMovil().hh, ajustes.getUsoMovil().mm));
        txt_resumen_diario.setText(intTiempoString(ajustes.getHoraResumenDiario().hh,
                ajustes.getHoraResumenDiario().mm));
        String desbloqueo_pantalla = ajustes.getAvisarDesbloqueos() + " "+ context.getString(R.string.veces);
        txt_desbloqueo_pantalla.setText(desbloqueo_pantalla);



        sw_notificaciones.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (aux){
                    aux = false;
                    sw_notificaciones.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones());
                }

            }
        });

        sw_llamadas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (aux2){
                    aux2 = false;
                    sw_llamadas.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas());
                }

            }
        });

    }

    private String intTiempoString(int hh, int mm) {
        String str;

        if (hh <= 9) {
            str = "0" + hh;
        } else {
            str = String.valueOf(hh);

        }

        if (mm <= 9) {
           str =  str.concat(":0" + mm);
        } else {
            str =  str.concat(":" + mm);
        }

        return str;

    }




}
