package com.atendus.app;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atendus.app.adapters.AdapterAppsFragmentRemoto;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.TresAndroides;

import spencerstudios.com.bungeelib.Bungee;

public class ActivityAppsDesconecta extends AppCompatActivity {

    private ProgressBar progressBar;
    //private IndexFastScrollRecyclerView fast_recycler_apps;
    private GridView listApps;
    //private AdapterAppsPermitidasRecycler adapter;
    private AdapterAppsFragmentRemoto adapter;

    private boolean flgProgramarDesconexion = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_desconecta);

        if (getIntent().getBooleanExtra("fromProgramarDesconexion", false))
            flgProgramarDesconexion = true;

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        progressBar.setVisibility(View.VISIBLE);
        final Handler handler = new Handler();
        final Handler handler2 = new Handler();

        listApps = findViewById(R.id.listaApps);
        listApps.setVisibility(View.VISIBLE);

        if (!flgProgramarDesconexion)
            new Thread() {
                @Override
                public void run() {
                    while (!PhoneStateManager.getInstance().isAppInfoListLoaded()) {
                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            new Thread() {
                                @Override
                                public void run() {
                                    //adapter = new AdapterAppsPermitidasRecycler(flgProgramarDesconexion);
                                    adapter = new AdapterAppsFragmentRemoto(ActivityAppsDesconecta.this);
                                    handler2.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            listApps.setAdapter(adapter);
                                            progressBar.setVisibility(View.GONE);
                                        }
                                    });
                                }
                            }.start();
                        }
                    });
                }
            }.start();
        else {
            adapter = new AdapterAppsFragmentRemoto(ActivityAppsDesconecta.this);
            listApps.setAdapter(adapter);
            progressBar.setVisibility(View.GONE);
        }

        findViewById(R.id.arr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final TextView txt_titulo = findViewById(R.id.txt_titulo);
        final EditText edtxt_search = findViewById(R.id.edtxt_search);
        ImageView search = findViewById(R.id.search);

        txt_titulo.setVisibility(View.VISIBLE);
        edtxt_search.setVisibility(View.GONE);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtxt_search.getVisibility() == View.GONE) {
                    txt_titulo.setVisibility(View.INVISIBLE);
                    edtxt_search.setVisibility(View.VISIBLE);
                    edtxt_search.requestFocus();
                    TresAndroides.showKeyboard(ActivityAppsDesconecta.this);
                } else {
                    edtxt_search.requestFocus();
                    TresAndroides.showKeyboard(ActivityAppsDesconecta.this);
                }
            }
        });

        edtxt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = edtxt_search.getText().toString();
                    //adapter.search(search);
                    return true;
                }
                return false;
            }
        });

        edtxt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String search = edtxt_search.getText().toString();
                //adapter.search(search);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

       /* if (flgProgramarDesconexion)
            if (ActivityProgramarDesconexion.aplicacionPermitidas != null) {
                ActivityProgramarDesconexion.aplicacionPermitidas.clear();
                ActivityProgramarDesconexion.aplicacionPermitidas.addAll(adapter.getAppsSeleccionadas());
            }*/
        Bungee.fade(this);
        finish();
    }

    @Override
    protected void onPause() {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        app.setAppsPermitidas(PhoneStateManager.getInstance().getAppsPermitidas());
        app.guardarEnPreferencias(this);
        PhoneStateManager.getInstance().acalizarAppsPermitidasPaquete();
        super.onPause();
    }


}
