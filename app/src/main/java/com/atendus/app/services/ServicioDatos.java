package com.atendus.app.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.atendus.app.ActivityAvisoEstadisticas;
import com.atendus.app.ActivityPrincipal;
import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.clases.AppUsage;
import com.atendus.app.clases.Telefono;
import com.atendus.app.json.AESCrypt;
import com.atendus.app.json.JSONAddApp;
import com.atendus.app.json.JSONEnviarEstadisticas;
import com.atendus.app.json.JSONEnviarEstadisticasUso;
import com.atendus.app.json.JSONEnviarEstadoDispositivo;
import com.atendus.app.json.JSONEnviarListinTelefonico;
import com.atendus.app.json.JSONLocation;
import com.atendus.app.utilidades.DatosUsoManager;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

public class ServicioDatos extends Service {

    private ScreenReceiver screenReceiver;
    private ServicioDatosRestartBroadcastReceiver receiver;

    private boolean entrar = true;
    private boolean entrar2 = true;

    private boolean looperSet = true;

    private int val = 59;
    private int idusu = 0;
    private int contadorEstadisticas = 30;
    private int contadorColaEnvios = 0;
    private int contadorEstado = 0;
    private static int mm = 0;

    LocationManager mLocationManager;
    LocationListener mLocationListener;
    Location currentBestLocation;
    static final int LOCATION_INTERVAL = 30000;

    public void startCheckingForLocation(Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationListener = new LocationListener() {
                @Override
                public void onLocationChanged(final Location location) {
                    makeUseOfNewLocation(location);
                    setLocation(currentBestLocation);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, 20, mLocationListener);
            Location lastKnownLocation = getLastBestLocation();
            makeUseOfNewLocation(lastKnownLocation);
            if (currentBestLocation != null) {
                setLocation(currentBestLocation);
            } else {
                if (lastKnownLocation != null) {
                    setLocation(lastKnownLocation);
                }
            }
        }
    }

    /**
     * This method modify the last know good location according to the arguments.
     *
     * @param location The possible new location.
     */
    void makeUseOfNewLocation(Location location) {
        if (isBetterLocation(location, currentBestLocation)) {
            currentBestLocation = location;
        }
    }

    /**
     * Determines whether one location reading is better than the current location fix
     *
     * @param location            The new location that you want to evaluate
     * @param currentBestLocation The current location fix, to which you want to compare the new one.
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > LOCATION_INTERVAL;
        boolean isSignificantlyOlder = timeDelta < -LOCATION_INTERVAL;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location,
        // because the user has likely moved.
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse.
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else return isNewer && !isSignificantlyLessAccurate && isFromSameProvider;
    }

    // Checks whether two providers are the same
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    /**
     * @return the last know best location
     */
    private Location getLastBestLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        long NetLocationTime = 0;

        if (null != locationGPS)
            GPSLocationTime = locationGPS.getTime();

        if (null != locationNet)
            NetLocationTime = locationNet.getTime();

        if (0 < GPSLocationTime - NetLocationTime) {
            return locationGPS;
        } else {
            return locationNet;
        }
    }

    void setLocation(final Location location) {
        new Thread() {
            @Override
            public void run() {
                super.run();
                JSONLocation jsonChild = new JSONLocation(ServicioDatos.this);
                try {


                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(getApplication());
                    final String iddisp = app.getUsuario().getNombrehijo();
                    jsonChild.run(app.getUsuario().getId(), iddisp, location.getLatitude(), location.getLongitude());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        String channel = "CHANNEL_DESCONEXIÓN";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channel, "Atendus sigue funcionando para ofrecerle unas estadísticas precisas", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setSound(null, null);
            notificationChannel.setVibrationPattern(new long[]{0L});
            notificationChannel.setLockscreenVisibility(MODE_PRIVATE);
            notificationChannel.setShowBadge(false);
            notificationChannel.setDescription("Atendus sigue funcionando para ofrecerle unas estadísticas precisas");
            notificationManager.createNotificationChannel(notificationChannel);
        } else {
            channel = "";
        }

        Intent showTaskIntent = new Intent(getApplicationContext(), ActivityPrincipal.class);
        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                showTaskIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(getApplicationContext(), channel)
                .build();


        startForeground(501, notification);

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getBaseContext());
        idusu = app.getUsuario().getId();

        startCheckingForLocation(getBaseContext());

        //TODO 3A Esto puede consumir mucha batería
        PowerManager mgr = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "bfree:MyWakeLock");
        wakeLock.acquire();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);


        DatosUsoManager.getInstance().loadFromPrefs(getBaseContext());

        //cont = Calendar.getInstance().get(Calendar.SECOND);


        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.SCREEN_OFF");
        filter.addAction("android.intent.action.SCREEN_ON");
        filter.addAction("android.intent.action.USER_PRESENT");
        screenReceiver = new ScreenReceiver();
        registerReceiver(screenReceiver, filter);

        IntentFilter filter2 = new IntentFilter();
        filter2.addAction("com.tresandroides.com.bfree.services.RestartServicioDatos");
        filter2.addAction("android.intent.action.BOOT_COMPLETED");
        filter2.addAction("android.intent.action.LOCKED_BOOT_COMPLETED");
        receiver = new ServicioDatosRestartBroadcastReceiver();
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        registerReceiver(receiver, filter2);
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("Hola_paso_por_aqui", "ServicioDatosRestartBroadcastReceiver");
        super.onDestroy();
        DatosUsoManager.getInstance().saveInPrefs(getBaseContext());
        try {
            unregisterReceiver(screenReceiver);
            Intent broadcastIntent = new Intent("com.tresandroides.com.bfree.services.RestartServicioDatos");
            sendBroadcast(broadcastIntent);
            stoptimertask();
        } catch (Exception e) {

        }

        //unregisterReceiver(receiver);
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime = 0;

    public void startTimer() {
        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {

            public void run() {

                if (looperSet) {
                    Looper.prepare();
                    looperSet = false;
                }
                if (DatosUsoManager.getInstance().getAjustes().isUserPresent()) {
                    DatosUsoManager.getInstance().getAjustes().addOneSecondOfUsage();
                    guardarDatosDeUso();
                }

                if (Calendar.getInstance().get(Calendar.SECOND) > 55) {
                    entrar = true;
                }

                if (Calendar.getInstance().get(Calendar.SECOND) < 10 && entrar) {
                    entrar = false;
                    datosDeUso();
                }

                val++;

                if (val >= 60) {
                    val = 0;

                    //Enviar datos de apps al servidor

                    Aplicacion app = new Aplicacion();

                    app.cargarAplicacionDePreferencias(getBaseContext());
                    final int idusu = app.getUsuario().getId();
                    final String iddisp = app.getUsuario().getNombrehijo();
                    Log.d("BFreePruebas", "enviando datos...");

                    new Thread() {
                        @Override
                        public void run() {
                            super.run();

                            JSONAddApp jsonAddApp = new JSONAddApp(getBaseContext());
                            String strLbl = "";
                            String strPckg = "";
                            for (AplicacionPermitida nApp : PhoneStateManager.getInstance().getAppsPermitidas()) {
                                try {
                                    final String pckName = nApp.getPackageName();
                                    Drawable icon = getPackageManager().getApplicationIcon(pckName);
                                    Bitmap bmp = null;
                                    if (icon instanceof BitmapDrawable) {
                                        BitmapDrawable bitmapDrawable = (BitmapDrawable) icon;
                                        if (bitmapDrawable.getBitmap() != null) {
                                            bmp = bitmapDrawable.getBitmap();
                                        }
                                    }

                                    if (bmp != null) {
                                        ByteArrayOutputStream byteArrayOutputStreamObject;

                                        byteArrayOutputStreamObject = new ByteArrayOutputStream();

                                        bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStreamObject);

                                        byte[] byteArrayVar = byteArrayOutputStreamObject.toByteArray();

                                        final String imageData = Base64.encodeToString(byteArrayVar, Base64.DEFAULT);

                                        //ImageProcessClass imageProcessClass = new ImageProcessClass();

                                        HashMap<String, String> HashMapParams = new HashMap<String, String>();

                                        HashMapParams.put("package", pckName);

                                        HashMapParams.put("image_data", imageData);

                                        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                                        String url = getResources().getString(R.string.url_webservices) + "userdata/add_listaapp.php";

// Request a string response from the provided URL.
                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        Log.d("wat", response);
                                                    }
                                                }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Log.e("wat", "That didn't work!");
                                            }
                                        }) {

                                            @Override
                                            protected Map<String, String> getParams() {
                                                Map<String, String> params = new HashMap<String, String>();
                                                params.put("package", pckName);
                                                params.put("image_data", imageData);


                                                return params;
                                            }

                                        };
// Add the request to the RequestQueue.
                                        queue.add(stringRequest);


                                        //String FinalData = imageProcessClass.ImageHttpRequest(getResources().getString(R.string.url_webservices)+"userdata/add_listapp.php", HashMapParams);


                                    }
                                } catch (Exception e) {

                                }
                                try {
                                    if (nApp.getAppLabel().contains("/")) {
                                        String nstr = nApp.getAppLabel().replace("/", " ");
                                        strLbl = strLbl.concat(URLEncoder.encode(AESCrypt.encrypt(nstr), "UTF-8") + "/");
                                    } else {
                                        strLbl = strLbl.concat(URLEncoder.encode(AESCrypt.encrypt(nApp.getAppLabel()), "UTF-8") + "/");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    strPckg = strPckg.concat(AESCrypt.encrypt(nApp.getPackageName()) + "/");
                                } catch (Exception e) {

                                }
                                try {
                                    String nstr = nApp.getAppLabel().replace("/", " ");
                                    nstr = URLEncoder.encode(AESCrypt.encrypt(nstr));
                                    jsonAddApp.run(idusu, iddisp, nstr, AESCrypt.encrypt(nApp.getPackageName()));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }.start();

                    PhoneStateManager.getInstance().cargarAplicacionesInstaladas();

                    enviarListinTelefonico(iddisp);
                }

                actualizarEstadisticasDiarias();

                //Enviar estadisiticas
                contadorEstadisticas++;
                if (contadorEstadisticas >= 30) {
                    contadorEstadisticas = 0;

                    Aplicacion app = new Aplicacion();

                    app.cargarAplicacionDePreferencias(getBaseContext());
                    final int idusu = app.getUsuario().getId();
                    final String iddisp = app.getUsuario().getNombrehijo();

                    if (idusu != -1) {
                        //Diarias
                        enviarEstadisticasDiarias(idusu, iddisp);
                        //Semanales
                        enviarEstadisticasSemanales(idusu, iddisp);
                        //mensuales
                        enviarEstadisticasMensuales(idusu, iddisp);
                    }
                }

                //Cola envios
                contadorColaEnvios++;
                if (contadorColaEnvios >= 30) {
                    contadorColaEnvios = 0;
                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(getBaseContext());
                    if (app.getColaEnvios().pendientes && TresAndroides.isOnline(getBaseContext())) {
                        boolean done = app.getColaEnvios().enviarTodos(getBaseContext(),
                                app.getUsuario().getId());
                        app.getColaEnvios().pendientes = !done;
                        app.guardarEnPreferencias(getBaseContext());
                    }
                }

                //Envío del estado del terminal (desconectado o no)
                //Envío de estadísticas de uso
                contadorEstado++;
                if (contadorEstado >= 30) {
                    contadorEstado = 0;

                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }

                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(getApplication());
                    final String iddisp = app.getUsuario().getNombrehijo();

                    JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo =
                            new JSONEnviarEstadoDispositivo(getBaseContext());

                    JSONEnviarEstadisticasUso jsonEnviarEstadisticasUso =
                            new JSONEnviarEstadisticasUso(getBaseContext());

                    int estdao = 0;
                    if (PhoneStateManager.getInstance().isDesconectado())
                        estdao = 1;
                    try {
                        jsonEnviarEstadoDispositivo.run(idusu, iddisp, estdao);
                        jsonEnviarEstadisticasUso.run(idusu, iddisp, app.getEstadisticaUsoDia(),
                                app.getEstadisticaUsoSemana(), app.getEstadisticaUsoMes());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        };


    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void datosDeUso() {
        //Alerta de tiempo de uso del terminal
        if (DatosUsoManager.getInstance().getAjustes().isAlertaUsoMovil()) {
            int hh1 = DatosUsoManager.getInstance().getAjustes().getUsoMovil().hh;
            int mm1 = DatosUsoManager.getInstance().getAjustes().getUsoMovil().mm;

            long milliseconds = DatosUsoManager.getInstance().getAjustes().getUsageTime();
            int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
            int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

            if (hours == hh1 && mm1 == minutes) {
                Intent intrtrn = new Intent(getBaseContext(), ActivityAvisoEstadisticas.class);
                intrtrn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityAvisoEstadisticas.isDesbloqueos = 2;
                ActivityAvisoEstadisticas.fromService = true;
                getBaseContext().startActivity(intrtrn);

            }
        }

        //Alerta de uso diario del terminal
        if (DatosUsoManager.getInstance().getAjustes().isNotificarResumenDiario()) {
            int hours = DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().hh;
            int minutes = DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().mm;

            int mm = Calendar.getInstance().get(Calendar.MINUTE);
            int hh = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

            if (hours == hh && minutes == mm) {

                Intent intrtrn = new Intent(getBaseContext(), ActivityAvisoEstadisticas.class);
                intrtrn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityAvisoEstadisticas.isDesbloqueos = 3;
                ActivityAvisoEstadisticas.fromService = true;
                getBaseContext().startActivity(intrtrn);

            }
        }
        DatosUsoManager.getInstance().saveInPrefs(getBaseContext());
    }

    private void actualizarEstadisticasDiarias() {
        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == 23
                && Calendar.getInstance().get(Calendar.MINUTE) == 59) {

            if (Calendar.getInstance().get(Calendar.SECOND) > 5) {
                entrar2 = true;
            }
            if (Calendar.getInstance().get(Calendar.SECOND) > 55 && entrar2) {
                entrar2 = false;


            }
        }
    }

    private void enviarEstadisticasDiarias(final int idusu, final String iddisp) {
        List<AppUsage> daily = TresAndroides.getDailyStatistics(getBaseContext());
        String pckg = "";
        String timeInForeground = "";
        String name = "";
        String clr = "";
        long total = 0;
        String pctg = "";
        String lastUsage = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double) apusu.getTotalTimeInForeground()) * 100 / ((double) total));
                pctg = pctg.concat(val + "/");
                lastUsage = lastUsage.concat(apusu.getLastTimeUsed() + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(getBaseContext());

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg, lastUsage, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstadisticasSemanales(final int idusu, final String iddisp) {
        List<AppUsage> daily = TresAndroides.getWeeklyStatistics(getBaseContext());
        String pckg = "";
        String timeInForeground = "";
        String name = "";
        String clr = "";
        long total = 0;
        String lastUsage = "";
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                lastUsage = lastUsage.concat(apusu.getLastTimeUsed() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double) apusu.getTotalTimeInForeground()) * 100 / ((double) total));
                pctg = pctg.concat(val + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(getBaseContext());

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg, lastUsage, 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstadisticasMensuales(final int idusu, final String iddisp) {
        List<AppUsage> daily = TresAndroides.getMonthlyStatistics(getBaseContext());
        String pckg = "";
        String timeInForeground = "";
        String name = "";
        String clr = "";
        String lastUsage = "";
        long total = 0;
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                lastUsage = lastUsage.concat(apusu.getLastTimeUsed() + "/");
                double val = (((double) apusu.getTotalTimeInForeground()) * 100 / ((double) total));
                pctg = pctg.concat(val + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(getBaseContext());

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg, lastUsage, 3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstado() {

    }

    private void enviarListinTelefonico(final String iddisp) {

        if (PhoneStateManager.getInstance().getTfnosPermitidos().size() == 0) {
            try {
                if (getSharedPreferences("Preferencias", Context.MODE_PRIVATE).getInt("padre", 0) == 0) {
                    PhoneStateManager.getInstance().cargarListinTelefonicoThread(getContentResolver(), this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        new Thread() {
            @Override
            public void run() {
                super.run();

                JSONEnviarListinTelefonico jsonTfnos = new JSONEnviarListinTelefonico(getBaseContext());
                String strNombre = "";
                String strNumero = "";
                //Aplicacion app = new Aplicacion();
                //app.cargarAplicacionDePreferencias(getApplication());
                int i = 0;
                for (Telefono tfno : PhoneStateManager.getInstance().getTfnosPermitidos()) {

                    try {
                        if (tfno.getNombre().contains("/")) {
                            String nstr = AESCrypt.encrypt(tfno.getNombre()).replace("/", " ");
                            //strNombre = strNombre.concat(URLEncoder.encode(nstr, "UTF-8") + "/");
                            strNombre = strNombre.concat(nstr + "/");
                        } else {
                            strNombre = strNombre.concat(AESCrypt.encrypt(tfno.getNombre()) + "/");
                        }
                        strNumero = strNumero.concat(AESCrypt.encrypt(tfno.getNumero()) + "/");
                    } catch (Exception e) {

                    }

                    ++i;
                    /*if (i >= 100){
                        try {
                            jsonTfnos.run(idusu, iddisp, strNombre, strNumero);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        strNombre = "";
                        strNumero = "";
                    }*/
                }
                try {
                    jsonTfnos.run(idusu, iddisp, strNombre, strNumero);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("BFreePruebas", "envio terminado");
            }
        }.start();
    }

    private void guardarDatosDeUso() {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getBaseContext());

        boolean cambioFecha = false;

        //Uso diarios
        if (app.getEstadisticaUsoDia().getDatoFecha() == -1) {
            app.getEstadisticaUsoDia().setDatoFecha(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
            app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoDia().setTiempoUso(0);
        } else if (app.getEstadisticaUsoDia().getDatoFecha() ==
                Calendar.getInstance().get(Calendar.DAY_OF_YEAR)) {
            app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoDia().setTiempoUso(app.getEstadisticaUsoDia().getTiempoUso() + 1000);
        } else {
            app.getEstadisticaUsoDia().setDatoFecha(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
            app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoDia().setTiempoUso(0);
            app.getEstadisticaUsoDia().setDesbloqueos(0);
            cambioFecha = true;
        }


        //Uso semanales
        if (app.getEstadisticaUsoSemana().getDatoFecha() == -1) {
            app.getEstadisticaUsoSemana().setDatoFecha(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
            app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoSemana().setTiempoUso(0);
        } else if (app.getEstadisticaUsoSemana().getDatoFecha() ==
                Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)) {
            app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoSemana().setTiempoUso(app.getEstadisticaUsoSemana().getTiempoUso() + 1000);
        } else {
            app.getEstadisticaUsoSemana().setDatoFecha(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
            app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoSemana().setTiempoUso(0);
            app.getEstadisticaUsoSemana().setDesbloqueos(0);
            cambioFecha = true;
        }


        //Uso mensuales
        if (app.getEstadisticaUsoMes().getDatoFecha() == -1) {
            app.getEstadisticaUsoMes().setDatoFecha(Calendar.getInstance().get(Calendar.MONTH));
            app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoMes().setTiempoUso(0);
        } else if (app.getEstadisticaUsoMes().getDatoFecha() ==
                Calendar.getInstance().get(Calendar.MONTH)) {
            app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoMes().setTiempoUso(app.getEstadisticaUsoMes().getTiempoUso() + 1000);
        } else {
            app.getEstadisticaUsoMes().setDatoFecha(Calendar.getInstance().get(Calendar.MONTH));
            app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoMes().setTiempoUso(0);
            app.getEstadisticaUsoMes().setDesbloqueos(0);
            cambioFecha = true;
        }

        app.guardarEnPreferencias(getBaseContext());

        if (cambioFecha) {
            JSONEnviarEstadisticasUso jsonEnviarEstadisticasUso =
                    new JSONEnviarEstadisticasUso(getBaseContext());

            final String iddisp = app.getUsuario().getNombrehijo();
            try {
                jsonEnviarEstadisticasUso.run(idusu, iddisp, app.getEstadisticaUsoDia(),
                        app.getEstadisticaUsoSemana(), app.getEstadisticaUsoMes());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    public class ImageProcessClass {

        public String ImageHttpRequest(String requestURL, HashMap<String, String> PData) {


            StringBuilder stringBuilder = new StringBuilder();

            try {

                URL url;
                HttpURLConnection httpURLConnectionObject;
                OutputStream OutPutStream;
                BufferedWriter bufferedWriterObject;
                BufferedReader bufferedReaderObject;
                int RC;

                url = new URL(requestURL);

                httpURLConnectionObject = (HttpURLConnection) url.openConnection();

                httpURLConnectionObject.setReadTimeout(19000);

                httpURLConnectionObject.setConnectTimeout(19000);

                httpURLConnectionObject.setRequestMethod("POST");

                httpURLConnectionObject.setDoInput(true);

                httpURLConnectionObject.setDoOutput(true);

                OutPutStream = httpURLConnectionObject.getOutputStream();

                bufferedWriterObject = new BufferedWriter(

                        new OutputStreamWriter(OutPutStream, StandardCharsets.UTF_8));

                bufferedWriterObject.write(bufferedWriterDataFN(PData));

                bufferedWriterObject.flush();

                bufferedWriterObject.close();

                OutPutStream.close();

                RC = httpURLConnectionObject.getResponseCode();

                if (RC == HttpsURLConnection.HTTP_OK) {

                    bufferedReaderObject = new BufferedReader(new InputStreamReader(httpURLConnectionObject.getInputStream()));

                    stringBuilder = new StringBuilder();

                    String RC2;

                    while ((RC2 = bufferedReaderObject.readLine()) != null) {

                        stringBuilder.append(RC2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        private String bufferedWriterDataFN(HashMap<String, String> HashMapParams) throws UnsupportedEncodingException {

            StringBuilder stringBuilderObject;

            stringBuilderObject = new StringBuilder();

            for (Map.Entry<String, String> KEY : HashMapParams.entrySet()) {


                stringBuilderObject.append(URLEncoder.encode(KEY.getKey(), "UTF-8"));

                stringBuilderObject.append("=");

                stringBuilderObject.append(URLEncoder.encode(KEY.getValue(), "UTF-8"));
            }

            return stringBuilderObject.toString();
        }

    }


}
