package com.atendus.app.services;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import com.atendus.app.ActivityAvisoEstadisticas;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.json.JSONEnviarEstadisticasUso;
import com.atendus.app.utilidades.DatosUsoManager;
import com.atendus.app.utilidades.PhoneStateManager;

import org.json.JSONException;

import java.util.Calendar;


public class ScreenReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(final Context context, Intent intent) {

        if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
            PhoneStateManager.getInstance().setLocked(true);
            DatosUsoManager.getInstance().getAjustes().locked();
        }else if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
            PhoneStateManager.getInstance().setLocked(false);
        }else if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
            DatosUsoManager.getInstance().getAjustes().unlocked();
            int desb = DatosUsoManager.getInstance().getAjustes().getDesbloqueos();
            desb++;
            DatosUsoManager.getInstance().getAjustes().setDesbloqueos(desb);
            DatosUsoManager.getInstance().saveInPrefs(context);
            final Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(context);

            //Desbloqueos diarios
            if (app.getEstadisticaUsoDia().getDatoFecha() == -1){
                app.getEstadisticaUsoDia().setDatoFecha(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
                app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoDia().setDesbloqueos(1);
            }else if(app.getEstadisticaUsoDia().getDatoFecha() ==
                    Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
                app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoDia().setDesbloqueos(app.getEstadisticaUsoDia().getDesbloqueos() + 1);
            }/*else{
                app.getEstadisticaUsoDia().setDatoFecha(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
                app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoDia().setDesbloqueos(1);
            }*/


            //Desbloqueos semanales
            if (app.getEstadisticaUsoSemana().getDatoFecha() == -1){
                app.getEstadisticaUsoSemana().setDatoFecha(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
                app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoSemana().setDesbloqueos(1);
            }else if(app.getEstadisticaUsoSemana().getDatoFecha() ==
                    Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)){
                app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoSemana().setDesbloqueos(app.getEstadisticaUsoSemana().getDesbloqueos() + 1);
            }/*else{
                app.getEstadisticaUsoSemana().setDatoFecha(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
                app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoSemana().setDesbloqueos(1);
            }*/


            //Desbloqueos mensuales
            if (app.getEstadisticaUsoMes().getDatoFecha() == -1){
                app.getEstadisticaUsoMes().setDatoFecha(Calendar.getInstance().get(Calendar.MONTH));
                app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoMes().setDesbloqueos(1);
            }else if(app.getEstadisticaUsoMes().getDatoFecha() ==
                    Calendar.getInstance().get(Calendar.MONTH)){
                app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoMes().setDesbloqueos(app.getEstadisticaUsoMes().getDesbloqueos() + 1);
            }/*else{
                app.getEstadisticaUsoMes().setDatoFecha(Calendar.getInstance().get(Calendar.MONTH));
                app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
                app.getEstadisticaUsoMes().setDesbloqueos(1);
            }*/

            app.guardarEnPreferencias(context);
            final int idusu = app.getUsuario().getId();

            final String iddisp = app.getUsuario().getNombrehijo();

            new Thread(){
                @Override
                public void run() {
                    super.run();
                    JSONEnviarEstadisticasUso jsonEnviarEstadisticasUso =
                            new JSONEnviarEstadisticasUso(context);
                    try {
                        jsonEnviarEstadisticasUso.run(idusu, iddisp, app.getEstadisticaUsoDia(),
                                app.getEstadisticaUsoSemana(), app.getEstadisticaUsoMes());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            if (DatosUsoManager.getInstance().getAjustes().isAvisarDesbloqueos()){

                Intent intrtrn = new Intent(context, ActivityAvisoEstadisticas.class);
                intrtrn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityAvisoEstadisticas.isDesbloqueos = 1;
                ActivityAvisoEstadisticas.fromService = true;
                context.startActivity(intrtrn);


                /*String channel = "CHANNEL_DATOSUSO";
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(channel,
                            context.getString(R.string.channel_desbloqueos),
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setDescription(context.getString(R.string.channel_desbloqueos));
                    notificationManager.createNotificationChannel(notificationChannel);
                }else{
                    channel = "";
                }

                Intent intnt = new Intent(context, ActivityPrincipal.class);
                PendingIntent pIntent = PendingIntent.getActivity(context,
                        0, intnt,0);
                String mensaje = context.getString(R.string.notificacion_desbloqueos_1) +
                        " " + DatosUsoManager.getInstance().getAjustes().getDesbloqueos() + " " +
                        context.getString(R.string.notificacion_desbloqueos_2);
                Notification n  = new NotificationCompat.Builder(context, channel)
                        .setContentTitle(context.getString(R.string.notificacion_desbloqueos_3))
                        .setContentText(mensaje)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(mensaje))
                        .setSmallIcon(R.drawable.ic_bfree)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setFullScreenIntent(pIntent,true)
                        .setAutoCancel(true).build();




                notificationManager.notify(505, n);

                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(context, notification);
                r.play();
                Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
                }else{
                    //deprecated in API 26
                    v.vibrate(500);
                }*/
            }
        }


    }
}
