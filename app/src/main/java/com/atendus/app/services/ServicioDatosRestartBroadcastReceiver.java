package com.atendus.app.services;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.atendus.app.utilidades.ViewPagerManager;


public class ServicioDatosRestartBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context, "SERVICIO REINICIADO", Toast.LENGTH_LONG).show();
        Log.d("Hola_paso_por_aqui", "ServicioDatosRestartBroadcastReceiver");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && intent.getAction().equals("android.intent.action.LOCKED_BOOT_COMPLETED")
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.N && intent.getAction().equals("android.intent.action.BOOT_COMPLETED")
                || intent.getAction().equals("com.tresandroides.com.bfree.services.RestartServicioDatos")) {
            if (!isMyServiceRunning(ServicioDatos.class, context)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(new Intent(context, ServicioDatos.class));
                } else {
                    context.startService(new Intent(context, ServicioDatos.class));
                }
            }
            ViewPagerManager.getInstance().setAppRunning(false);
            Log.d("BFree", "Reiniciado " + intent.getAction());
        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
