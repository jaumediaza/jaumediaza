package com.atendus.app.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.atendus.app.ActivityPrincipal;
import com.atendus.app.R;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.clases.NotificacionInfo;
import com.atendus.app.utilidades.PhoneStateManager;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by Alberto on 23/06/2018.
 */

public class
NotificationMonitor extends NotificationListenerService {
    private static final String TAG = "SevenNLS";
    private static final String TAG_PRE = "[" + NotificationMonitor.class.getSimpleName() + "] ";
    public static final String ACTION_NLS_CONTROL = "com.seven.notificationlistenerdemo.NLSCONTROL";
    public static List<StatusBarNotification[]> mCurrentNotifications = new ArrayList<StatusBarNotification[]>();
    public static int mCurrentNotificationsCounts = 0;
    public static StatusBarNotification mRemovedNotification;
    private NotificationMonitorBroadcastReceiver receiver;

    private static String lastNotif;
    public static String getLastNotif(){
        String aux = lastNotif;
        lastNotif = "NULL";
        return aux;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        receiver = new NotificationMonitorBroadcastReceiver();
        IntentFilter filter2 = new IntentFilter();
        filter2.addAction("com.tresandroides.com.bfree.services.RestartServicioDesconexion");
        registerReceiver(receiver, filter2);


        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        logNLS("onCreate...");
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_NLS_CONTROL);

        super.onCreate();
        String channel = "CHANNEL_NOTIF";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channel, "Control de notificaciones en modo desconecta", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setSound(null, null);
            notificationChannel.setVibrationPattern(new long[]{0L});
            notificationChannel.setLockscreenVisibility(MODE_PRIVATE);
            notificationChannel.setShowBadge(false);
            notificationChannel.setDescription("Control de notificaciones en modo desconecta");
            notificationManager.createNotificationChannel(notificationChannel);
        }else{
            channel = "Control de notificaciones en modo desconecta";
        }

        Intent showTaskIntent = new Intent(getApplicationContext(), ActivityPrincipal.class);
        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        Notification notification = new NotificationCompat.Builder(getApplicationContext(), channel)
                .build();


        startForeground(501, notification);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(PhoneStateManager.getInstance().isDesconectado()){
            Intent broadcastIntent = new Intent("com.tresandroides.com.bfree.services.RestartServicioNotificationMonitor");
            sendBroadcast(broadcastIntent);
        }
        if (receiver != null)
            unregisterReceiver(receiver);

    }

    @Override
    public IBinder onBind(Intent intent) {
        // a.equals("b");
        logNLS("onBind...");
        return super.onBind(intent);
    }

    @Override
    public void onInterruptionFilterChanged(int interruptionFilter) {
        super.onInterruptionFilterChanged(interruptionFilter);
        Log.d("a", "a");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        lastNotif = sbn.getPackageName();
        if (sbn.getId() == 12321) {
            cancelNotification(sbn.getKey());
            return;
        }else if(sbn.getPackageName().equals("com.atendus.app") || sbn.getPackageName().contains("incallui"))
            return;


        if(!PhoneStateManager.getInstance().isDesconectado())
            return;

        try {
            boolean permitida = false;
            for (AplicacionPermitida app: PhoneStateManager.getInstance().getAppsPermitidas()){
                if (app.getPackageName().equals(sbn.getPackageName()) && app.isUsoDuranteDesconecta()){
                    permitida = true;
                }
            }
            if (permitida) {
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                if (am.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {

                    //Ñapa para que suene la notificación aunque esté en silencio
                    am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                }
                return;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }


        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        if(!PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones()){
            if (am.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {

                //Ñapa para que suene la notificación aunque esté en silencio
                am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
            }
            return;
        }
        am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        cancelNotification(sbn.getKey());



        Intent target = new Intent();

        PendingIntent pending = PendingIntent.getBroadcast(this, 0, target, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setContentTitle("").setContentText("")
                .setSmallIcon(R.drawable.transparent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setFullScreenIntent(pending, false)
                .setAutoCancel(true)
                .build();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notification.headsUpContentView = new RemoteViews(getPackageName(), R.layout.empty_heads_up);
        }

        notification.sound = null;
        notification.ledARGB = 0;
        notification.vibrate = new long[0];

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(12321, notification);

        NotificacionInfo.NotificacionInfoList notifs =  new NotificacionInfo.NotificacionInfoList(
                PhoneStateManager.getInstance().getNotificaciones().getList());
        boolean encontrado = false;
        //Se añade la notificación a la lista de esta desconexión
        for (int i = 0; i < notifs.getList().size() && !encontrado; ++i){
            if (notifs.getList().get(i).getPackageName().equals(sbn.getPackageName())){
                encontrado = true;
                PhoneStateManager.getInstance().getNotificaciones().getList()
                        .get(i).addNewNotificaction();
            }
        }

        boolean encontrado2 = false;

        for (int i = 0; i < PhoneStateManager.getInstance().getAppsPermitidas().size(); ++i){
            if (PhoneStateManager.getInstance().getAppsPermitidas().get(i).getPackageName().equals(sbn.getPackageName())) {
                encontrado2 = true;
            }
        }

        if (!encontrado && encontrado2){
            PhoneStateManager.getInstance().getNotificaciones().getList()
                    .add(new NotificacionInfo(sbn.getPackageName()));
        }

    }


    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        updateCurrentNotifications();
        logNLS("removed...");
        logNLS("have " + mCurrentNotificationsCounts + " active notifications");
        mRemovedNotification = sbn;
    }

    private void updateCurrentNotifications() {
        try {
            StatusBarNotification[] activeNos = getActiveNotifications();
            if (mCurrentNotifications.size() == 0) {
                mCurrentNotifications.add(null);
            }
            mCurrentNotifications.set(0, activeNos);
            mCurrentNotificationsCounts = activeNos.length;
        } catch (Exception e) {
            logNLS("Should not be here!!");
            e.printStackTrace();
        }
    }

    public static StatusBarNotification[] getCurrentNotifications() {
        if (mCurrentNotifications.size() == 0) {
            logNLS("mCurrentNotifications size is ZERO!!");
            return null;
        }
        return mCurrentNotifications.get(0);
    }

    private static void logNLS(Object object) {
        Log.i(TAG, TAG_PRE + object);
    }

}
