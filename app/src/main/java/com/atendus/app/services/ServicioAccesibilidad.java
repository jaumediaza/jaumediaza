package com.atendus.app.services;

import android.accessibilityservice.AccessibilityService;
import android.os.Build;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.ArrayList;
import java.util.List;

public class ServicioAccesibilidad extends AccessibilityService {
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            if (event.getPackageName() != null){
                String packageOfApp = String.valueOf(event.getPackageName());
                    //check if app is on list of blocked apps
                boolean isOnList = false;
                if(isOnList) {
                    AccessibilityNodeInfo nodeInfo = event.getSource();
                    Log.i("Bfree", "ACC::onAccessibilityEvent: nodeInfo=" + nodeInfo);
                    if (nodeInfo == null) {
                        return;
                    }

                    List<AccessibilityNodeInfo> list = new ArrayList<>();
                    if ("com.android.settings.applications.InstalledAppDetailsTop".equals(event.getClassName())) {
                        if (Build.VERSION.SDK_INT >= 18) {
                            list = nodeInfo.findAccessibilityNodeInfosByViewId("com.android.settings:id/right_button");

                        } else if (Build.VERSION.SDK_INT >= 14) {
                            list = nodeInfo.findAccessibilityNodeInfosByText("com.android.settings:id/right_button");
                        }
                        for (AccessibilityNodeInfo node : list) {
                            Log.i("Bfree", "ACC::onAccessibilityEvent: left_button " + node);
                            node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        }
                    } else if ("android.app.AlertDialog".equals(event.getClassName())) {
                        list = new ArrayList<>();
                        if (Build.VERSION.SDK_INT >= 18) {
                            list = nodeInfo.findAccessibilityNodeInfosByViewId("android:id/button1");

                        } else if (Build.VERSION.SDK_INT >= 14) {
                            list = nodeInfo.findAccessibilityNodeInfosByText("android:id/button1");
                        }

                        for (final AccessibilityNodeInfo node : list) {
                            Log.i("Bfree", "ACC::onAccessibilityEvent: button1 " + node);
                            node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            //node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        }
                    }
                }

                }
            }
        }


    @Override
    public void onInterrupt() {
    }


}
