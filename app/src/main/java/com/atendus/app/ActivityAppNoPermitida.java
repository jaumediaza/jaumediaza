package com.atendus.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;


public class ActivityAppNoPermitida extends Activity {

    public static boolean fromService = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      //finish();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_no_permitida);

    }


    @Override
    protected void onPause() {
        super.onPause();
        //finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fromService){
            new CountDownTimer(2000, 2000){

                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    fromService = false;
                    finish();
                }
            }.start();
        }else{
            Intent intenti = new Intent(this, ActivityPrincipal.class);
            intenti.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intenti);
        }
    }
}
