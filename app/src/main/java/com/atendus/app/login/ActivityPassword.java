package com.atendus.app.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.atendus.app.ActivityPrincipal;
import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.Usuario;


public class ActivityPassword extends AppCompatActivity {
    private Aplicacion app=new Aplicacion();
    private Activity activity;
    private Context contexto;
    EditText editPw;
    Button okButton;
    Usuario usuario;

    @Override
    public void onBackPressed() {

        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("timeloaded", System.currentTimeMillis());
        editor.commit();
        activity = this;
        contexto = this;

        editPw = (EditText) findViewById(R.id.edittext);
        okButton = (Button) findViewById(R.id.btn_entrar);
        app.cargarAplicacionDePreferencias(contexto);
        usuario = new Usuario();
        if (app.isUsuarioConectado()){
            usuario = app.getUsuario();
        }

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(usuario!=null) {
                    if (usuario.getPassword().equals(editPw.getText().toString())) {
                        Intent i = new Intent(ActivityPassword.this, ActivityPrincipal.class);
                        startActivity(i);
                        finish();
                    }
                    else{
                        SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putLong("timeloaded", 0);
                        editor.commit();
                        AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                        dlg.setMessage("Contraseña no válida");
                        dlg.setPositiveButton("Aceptar", null);
                        dlg.show();
                    }
                }
            }
        });




    }
}
