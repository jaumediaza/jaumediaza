package com.atendus.app.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.atendus.app.ActivityPadre;
import com.atendus.app.ActivityPermisos;
import com.atendus.app.ActivityPrincipal;
import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;

import spencerstudios.com.bungeelib.Bungee;

import static com.atendus.app.ActivityPermisos.accessibilityPermission;
import static com.atendus.app.ActivityPermisos.adminPermission;
import static com.atendus.app.ActivityPermisos.contactPermission;
import static com.atendus.app.ActivityPermisos.usageStatPermission;
import static com.atendus.app.ActivityPermisos.defaultPermission;
import static com.atendus.app.ActivityPermisos.locationPermission;
import static com.atendus.app.ActivityPermisos.callPermission;
import static com.atendus.app.ActivityPermisos.notificationPermission;


public class ActivityBienvenida extends AppCompatActivity {
    private Aplicacion app=new Aplicacion();
    private TextView txtSaludo,textView6,textView7;

    private static final int DURACION_PANTALLA = 2000;
    Typeface Roboto_Medium=null;
    Typeface Roboto_Light=null;
    private Typeface Roboto_Light_Italic;
    private Activity activity;
    private Context contexto;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ActivityBienvenida.this, ActivityPrincipal.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvenida);

        activity=this;
        contexto=this;

        txtSaludo= this.findViewById(R.id.txtSaludo);
        textView6= this.findViewById(R.id.textView6);
        textView7= this.findViewById(R.id.textView7);

        Roboto_Medium=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Medium.ttf");
        Roboto_Light=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Light.ttf");
        Roboto_Light_Italic=Typeface.createFromAsset(getAssets(),"fonts/Roboto-LightItalic.ttf");

        txtSaludo.setTypeface(Roboto_Medium);
        textView6.setTypeface(Roboto_Light_Italic);
        textView7.setTypeface(Roboto_Light_Italic);


        app.cargarAplicacionDePreferencias(this);

        String saludo = getString(R.string.mensaje_bienvenida_1) + " ";
        if (getIntent().getExtras().getInt("Registro") != 0){
            saludo = saludo.concat(app.getUsuario().getNombre().toUpperCase());
        }else{
            app.setUsuarioSinRegistro(true);
            app.guardarEnPreferencias(this);
        }
        saludo = saludo.concat(" !");
        txtSaludo.setText(saludo);

        new CountDownTimer(DURACION_PANTALLA, DURACION_PANTALLA) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {

                SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                int isPadre = -1;
                if(app.getUsuario()!=null){
                    isPadre = app.getUsuario().getIsPadre();//preferences.getInt("padre",-1);
                }

                if ((preferences.getBoolean("defaultPermission",false) && preferences.getBoolean("accessibilityPermission",false) && preferences.getBoolean("locationPermission",false) && preferences.getBoolean("usageStatPermission",false) && preferences.getBoolean("adminPermission",false) && preferences.getBoolean("contactPermission",false) ==true && preferences.getBoolean("callPermission",false)) || isPadre==1){
                    Intent intent = new Intent(ActivityBienvenida.this, ActivityPrincipal.class);
                    startActivity(intent);
                    Bungee.fade(activity);
                    finish();
                }
                else if((preferences.getBoolean("defaultPermission",false) && preferences.getBoolean("accessibilityPermission",false) && preferences.getBoolean("locationPermission",false) && preferences.getBoolean("usageStatPermission",false) && preferences.getBoolean("adminPermission",false) && preferences.getBoolean("contactPermission",false) ==true && preferences.getBoolean("callPermission",false)) && isPadre==0 && !app.isUsuarioSinRegistro()){
                    Intent intent = new Intent(ActivityBienvenida.this, ActivityPassword.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else if(isPadre==0){
                    Intent i;
                    if(preferences.getInt("selectedSex",-1)!=-1) {
                        i = new Intent(ActivityBienvenida.this, ActivityPermisos.class);
                    }
                    else{
                        i = new Intent(ActivityBienvenida.this, ActivityPadre.class);
                    }
                    startActivity(i);

                    Bungee.fade(activity);
                    finish();
                }
                else{
                    Intent i = new Intent(ActivityBienvenida.this, ActivityPadre.class);
                    startActivity(i);
                    Bungee.fade(activity);
                    finish();
                }

            }
        }.start();


    }
}
