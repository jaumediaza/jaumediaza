package com.atendus.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;

import com.atendus.app.adapters.AdapterExpandableDesconexionesAnteriores;



public class ActivityDesconexionesAnteriores extends AppCompatActivity {

    private int lastExpandedPosition = -1;
    private ExpandableListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desconexiones_anteriores);

        findViewById(R.id.arr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        AdapterExpandableDesconexionesAnteriores adapter =
                new AdapterExpandableDesconexionesAnteriores(this);

        lv = findViewById(R.id.explv_desconexiones);

        lv.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    lv.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if (adapter.getGroupCount() > 0){
            findViewById(R.id.txt_no_desc).setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
