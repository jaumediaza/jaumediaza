package com.atendus.app;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.json.JSONChild;
import com.atendus.app.json.JSONVincularDispositivo;
import com.atendus.app.login.ActivityPassword;
import com.atendus.app.services.AdminReceiver;
import com.atendus.app.services.ServicioAccesibilidad;
import com.atendus.app.utilidades.TresAndroides;
import com.atendus.permissions.AtendusOnboardingEngine;
import com.atendus.permissions.AtendusOnboardingPage;
import com.atendus.permissions.listeners.AtendusOnboardingOnChangeListener;
import com.atendus.permissions.listeners.AtendusOnboardingOnRightOutListener;
import com.atendus.permissions.listeners.ButtonListener;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class ActivityPermisos extends AppCompatActivity {

    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = 666;
    private static final int PERMISSION_ACCESS_FINE_LOCATION = 777;
    private static final int PERMISSION_ACCESSIBILITY_REQUEST_CODE = 1000;
    private static final int PERMISSION_USAGE_REQUEST_CODE = 1001;
    private static final int PERMISSION_NOTIFICATION_REQUEST_CODE = 1003;
    private static final int PERMISSION_DEFAULT_CALLER_REQUEST_CODE = 1004;
    private static final int PERMISSION_ADMIN_REQUEST_CODE = 1005;

    public static boolean callPermission = false;
    public static boolean defaultPermission = false;
    public static boolean contactPermission = false;
    public static boolean adminPermission = false;
    public static boolean usageStatPermission = false;
    public static boolean notificationPermission = false;
    public static boolean accessibilityPermission = false;
    public static boolean locationPermission = false;
    int resChild;
    int resvinc;
    Handler handlerChild;
    Handler vincuChild;
    String nombrehijo;
    int sexohijo;
    int anohijo;

    private Aplicacion app;

    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    private AtendusOnboardingEngine engine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = preferences.edit();
        prefEditor.putBoolean("defaultPermission", false); //**syntax error on tokens**
        prefEditor.putBoolean("accessibilityPermission", false); //**syntax error on tokens**
        prefEditor.putBoolean("locationPermission", false); //**syntax error on tokens**
        prefEditor.putBoolean("usageStatPermission", false);
        prefEditor.putBoolean("adminPermission", false); //**syntax error on tokens**
        prefEditor.putBoolean("contactPermission", false);
        prefEditor.putBoolean("callPermission", false);
        prefEditor.apply();
        //Initialize main view for permission library
        setContentView(R.layout.onboarding_main_layout);

        app = new Aplicacion();

        //Inicializamos el motor de permisos
        View rootView = findViewById(R.id.onboardingRootView);
        engine = new AtendusOnboardingEngine(
                rootView,
                getViewPermissionData(),
                this
        );

        engine.setOnChangeListener(new AtendusOnboardingOnChangeListener() {
            @Override
            public void onPageChanged(int oldElementIndex, int newElementIndex) {
            }
        });
        engine.setOnRightOutListener(new AtendusOnboardingOnRightOutListener() {
            @Override
            public void onRightOut() {
                if (checkPermissions()) {
                    SharedPreferences preferences = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
                    SharedPreferences.Editor prefEditor = preferences.edit();
                    prefEditor.putBoolean("defaultPermission", true); //**syntax error on tokens**
                    prefEditor.putBoolean("accessibilityPermission", true); //**syntax error on tokens**
                    prefEditor.putBoolean("locationPermission", true); //**syntax error on tokens**
                    prefEditor.putBoolean("usageStatPermission", true);
                    prefEditor.putBoolean("adminPermission", true); //**syntax error on tokens**
                    prefEditor.putBoolean("contactPermission", true);
                    prefEditor.putBoolean("callPermission", true);
                    prefEditor.commit();
                    Intent intent = new Intent(ActivityPermisos.this, ActivityPassword.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(ActivityPermisos.this, "Necesitamos todos los permisos para proceder.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case PERMISSION_ACCESSIBILITY_REQUEST_CODE:
                accessibilityPermission = true;
                engine.toggleContent(false);
                break;
            case PERMISSION_USAGE_REQUEST_CODE:
                usageStatPermission = true;
                engine.toggleContent(false);
                break;
            case PERMISSION_NOTIFICATION_REQUEST_CODE:
                notificationPermission = true;
                engine.toggleContent(false);
                break;
            case PERMISSION_DEFAULT_CALLER_REQUEST_CODE:
                defaultPermission = true;
                engine.toggleContent(false);
            case PERMISSION_ADMIN_REQUEST_CODE:
                adminPermission = true;
                engine.toggleContent(false);
                break;
        }
    }

    //TODO: Valida aca que los permisos solicitados se concedan o no y aplica la logica para ello.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        callPermission = true;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_READ_CONTACTS:
                if (grantResults[0] >= 0) {
                    contactPermission = true;
                    engine.toggleContent(false);
                } else {
                    contactPermission = false;
                }
                break;

            case PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults[0] >= 0) {
                    locationPermission = true;
                    engine.toggleContent(false);
                } else {
                    locationPermission = false;
                }
                break;
        }
    }

    public ArrayList<AtendusOnboardingPage> getViewPermissionData() {
        // prepare data


        AtendusOnboardingPage location = new AtendusOnboardingPage(
                getString(R.string.location_title),
                getString(R.string.location_disclaimer),
                ContextCompat.getColor(this, R.color.colorAccent),
                R.drawable.onboarding_pager_circle_icon,
                "Conceder",
                new ButtonListener() {
                    @Override
                    public void onClick() {
                        requestPermission(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                PERMISSION_ACCESS_FINE_LOCATION
                        );
                    }
                }
        );

        AtendusOnboardingPage accessibility = new AtendusOnboardingPage(
                getString(R.string.accessibility_title),
                getString(R.string.accessibility_disclaimer),
                ContextCompat.getColor(this, R.color.colorAccent),
                R.drawable.onboarding_pager_circle_icon,
                "Conceder",
                new ButtonListener() {
                    @Override
                    public void onClick() {
                        startActivityForResult(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS),
                                PERMISSION_ACCESSIBILITY_REQUEST_CODE);
                    }
                }
        );

        AtendusOnboardingPage defaultApp = new AtendusOnboardingPage(
                "Aplicación de teléfono predeterminada",
                "Necesitamos este permiso para poder bloquear o permitir llamadas",
                ContextCompat.getColor(this, R.color.colorAccent),
                R.drawable.onboarding_pager_circle_icon,
                "Conceder",
                new ButtonListener() {
                    @Override
                    public void onClick() {
                        TelecomManager telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);
                        boolean isAlreadyDefaultDialer = getPackageName()
                                .equals(telecomManager.getDefaultDialerPackage());

                        if (!isAlreadyDefaultDialer) {
                            Intent ChangeDialer = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
                            ChangeDialer.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, getPackageName());
                            if (ChangeDialer.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(ChangeDialer, PERMISSION_DEFAULT_CALLER_REQUEST_CODE);
                            } else {
                                Log.w(getLocalClassName(), "No Intent available to handle action");
                            }
                        } else {
                            engine.toggleContent(false);
                        }
                    }
                }
        );

        AtendusOnboardingPage contacts = new AtendusOnboardingPage(
                getString(R.string.contact_title),
                getString(R.string.contact_disclaimer),
                ContextCompat.getColor(this, R.color.colorAccent),
                R.drawable.onboarding_pager_circle_icon,
                "Conceder",
                new ButtonListener() {
                    @Override
                    public void onClick() {
                        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                                PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                }
        );

        AtendusOnboardingPage admin = new AtendusOnboardingPage(
                getString(R.string.admin_title),
                getString(R.string.admin_disclaimer),
                ContextCompat.getColor(this, R.color.colorAccent),
                R.drawable.onboarding_pager_circle_icon,
                "Conceder",
                new ButtonListener() {
                    @Override
                    public void onClick() {
                        if (!app.isDeviceAdmin()) {
                            requestAdminPermission();
                        } else {
                            engine.toggleContent(false);
                        }
                    }
                }
        );

        AtendusOnboardingPage usage = new AtendusOnboardingPage(
                getString(R.string.usage_title),
                getString(R.string.usage_disclaimer),
                ContextCompat.getColor(this, R.color.colorAccent),
                R.drawable.onboarding_pager_circle_icon,
                "Conceder",
                new ButtonListener() {
                    @Override
                    public void onClick() {
                        if (!app.isPermisoAccesoDatosUsoAceptado()) {
                            if (!isUsageStatActive()) {
                                startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS), PERMISSION_USAGE_REQUEST_CODE);
                            } else {
                                engine.toggleContent(false);
                            }
                        } else {
                            engine.toggleContent(false);
                        }
                    }
                }
        );

        AtendusOnboardingPage notification = new AtendusOnboardingPage(
                getString(R.string.notification_title),
                getString(R.string.notification_disclaimer),
                ContextCompat.getColor(this, R.color.colorAccent),
                R.drawable.onboarding_pager_circle_icon,
                "Conceder",
                new ButtonListener() {
                    @Override
                    public void onClick() {
                        if (!isNotificationListenerActive()) {
                            startActivityForResult(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS),
                                    PERMISSION_NOTIFICATION_REQUEST_CODE);
                        } else {
                            engine.toggleContent(false);
                        }
                    }
                }
        );

        ArrayList<AtendusOnboardingPage> permissions = new ArrayList<>();

        permissions.add(accessibility);
        permissions.add(location);
        permissions.add(contacts);
        permissions.add(defaultApp);
        permissions.add(notification);
        permissions.add(usage);
        permissions.add(admin);

        return permissions;
    }

    private void requestPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    private Boolean checkPermissions() {
        app.cargarAplicacionDePreferencias(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            callPermission = checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED;
        } else {
            callPermission = true;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            locationPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED;
        } else {
            locationPermission = true;
        }

        TelecomManager telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);

        defaultPermission = getPackageName().equals(telecomManager.getDefaultDialerPackage());
        accessibilityPermission = isAccessibilityServiceEnabled(this, ServicioAccesibilidad.class);

        if (!(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)) {
            contactPermission = true;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            usageStatPermission = isUsageStatActive();
        } else {
            usageStatPermission = true;
        }

        adminPermission = app.isDeviceAdmin();
        notificationPermission = isNotificationListenerActive();

        return callPermission && locationPermission && defaultPermission && accessibilityPermission &&
                contactPermission && usageStatPermission && adminPermission && notificationPermission;
    }

    @Override
    protected void onResume() {
        super.onResume();


        if(getIntent().getExtras()!=null) {
            nombrehijo = getIntent().getExtras().getString("nombrehijo", "Padre");
            sexohijo = getIntent().getExtras().getInt("sexohijo", 0);
            anohijo = getIntent().getExtras().getInt("anohijo", 2019);
        }
        else{
            nombrehijo = app.getUsuario().getNombrehijo();
            sexohijo = app.getUsuario().getSexo();
            anohijo = app.getUsuario().getEdadhijo();
        }
        checkPermissions();
        if (isNotificationListenerActive() && defaultPermission && accessibilityPermission &&
                locationPermission && usageStatPermission &&
                adminPermission && contactPermission && callPermission) {
            final String device_name = TresAndroides.getDeviceName();
           /* if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }*/

            //TODO: Te recomiendo no seguir utilizando este metodo para obtener el ID del dispositivo, mejor ocupa
            // SECURE_ANDROID_ID, evitemos problemas con los de Google por este tipo de situaciones
            final String deviceId = app.getUsuario().getNombrehijo();

            handlerChild = new Handler();
            vincuChild = new Handler();
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    resvinc = -1;
                    JSONVincularDispositivo jsonVincularDispositivo = new JSONVincularDispositivo(ActivityPermisos.this);
                    try {
                        resvinc = jsonVincularDispositivo.run(app.getUsuario().getId(), nombrehijo, device_name);
                        vincuChild.post(new Runnable() {
                            public void run() {
                                if (resvinc == 1) {
                                    new Thread() {
                                        @Override
                                        public void run() {
                                            super.run();
                                            try {
                                                JSONChild jsonChild = new JSONChild(ActivityPermisos.this);
                                                resChild = -1;
                                                resChild = jsonChild.run(app.getUsuario().getId(), nombrehijo, 0, nombrehijo, sexohijo, anohijo);
                                                handlerChild.post(new Runnable() {
                                                    public void run() {
                                                        if (resChild == 1) {
                                                            app.getUsuario().setIsPadre(0);
                                                            app.getUsuario().setNombrehijo(nombrehijo);
                                                            app.getUsuario().setEdadhijo(anohijo);
                                                            app.getUsuario().setSexo(sexohijo);

                                                            app.guardarEnPreferencias(ActivityPermisos.this);

                                                        }
                                                    }

                                                });
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    }.start();

                                }
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }.start();







            SharedPreferences preferences = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = preferences.edit();
            prefEditor.putBoolean("defaultPermission", true); //**syntax error on tokens**
            prefEditor.putBoolean("accessibilityPermission", true); //**syntax error on tokens**
            prefEditor.putBoolean("locationPermission", true); //**syntax error on tokens**
            prefEditor.putBoolean("usageStatPermission", true);
            prefEditor.putBoolean("adminPermission", true); //**syntax error on tokens**
            prefEditor.putBoolean("contactPermission", true);
            prefEditor.putBoolean("callPermission", true);
            prefEditor.commit();
            Intent intent = new Intent(this, ActivityPassword.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public static boolean isAccessibilityServiceEnabled(Context context, Class<?> accessibilityService) {
        ComponentName expectedComponentName = new ComponentName(context, accessibilityService);

        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;
    }

    private void requestAdminPermission() {
        ComponentName mDeviceAdminSample;
        mDeviceAdminSample = new ComponentName(ActivityPermisos.this, AdminReceiver.class);
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                mDeviceAdminSample);

        startActivityForResult(intent, PERMISSION_ADMIN_REQUEST_CODE);
    }

    private boolean isNotificationListenerActive() {
        String pkgName = getPackageName();
        final String flat = Settings.Secure.getString(getContentResolver(), ENABLED_NOTIFICATION_LISTENERS);

        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (String name : names) {
                final ComponentName cn = ComponentName.unflattenFromString(name);
                if (cn != null) {
                    if (TextUtils.equals(pkgName, cn.getPackageName())) {
                        app.setPermisoNotificacionesAceptado(true);
                        app.guardarEnPreferencias(this);
                        return true;
                    }
                }
            }
        }
        app.setPermisoNotificacionesAceptado(false);
        app.guardarEnPreferencias(this);

        return false;
    }

    private boolean isUsageStatActive() {
        UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();
        List stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);

        return stats != null && !stats.isEmpty();
    }
}