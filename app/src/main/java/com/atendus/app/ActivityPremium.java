package com.atendus.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AppController;
import com.atendus.app.json.JSONPremium;

import org.json.JSONException;
import org.solovyev.android.checkout.ActivityCheckout;
import org.solovyev.android.checkout.Billing;
import org.solovyev.android.checkout.BillingRequests;
import org.solovyev.android.checkout.Checkout;
import org.solovyev.android.checkout.EmptyRequestListener;
import org.solovyev.android.checkout.Inventory;
import org.solovyev.android.checkout.ProductTypes;
import org.solovyev.android.checkout.Purchase;
import org.solovyev.android.checkout.RequestListener;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;


public class ActivityPremium extends AppCompatActivity {

    private Aplicacion app;

    private static Activity activity;
    private Context contexto;

    private TextView txtSaludo;

    private List<String> listaTitles;
    private List<String> listaPrecios;
    private List<String> listaDescris;
    private List<String> listaSkus;
    String actualSku;
    private ActivityCheckout mCheckout;
    int plan;
    Handler handlerPremium;
    Inventory mInventory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);
        activity = this;
        contexto = this;

        final Billing billing = AppController.get(ActivityPremium.this).getBilling();
        mCheckout = Checkout.forActivity(ActivityPremium.this, billing);
        mCheckout.start();

       txtSaludo = (TextView)findViewById(R.id.txtSaludo);



        app = new Aplicacion();

        app.cargarAplicacionDePreferencias(this);

        String saludo = getString(R.string.mensaje_bienvenida_1) + " ";
        if (app.getUsuario()!=null){
            saludo = saludo.concat(app.getUsuario().getNombre().toUpperCase());
        }else{
            app.setUsuarioSinRegistro(true);
            app.guardarEnPreferencias(this);
        }
        saludo = saludo.concat(" !");
        txtSaludo.setText(saludo);

        listaTitles = new ArrayList<String>();
        listaDescris = new ArrayList<String>();
        listaPrecios = new ArrayList<String>();
        listaSkus = new ArrayList<String>();

        /*
        listaTitles.add("Premium 1");
        listaDescris.add("Controla hasta 3 dispositivos");
        listaPrecios.add("10€");

        listaTitles.add("Premium 2");
        listaDescris.add("Controla hasta 10 dispositivos");
        listaPrecios.add("25€");

        listaTitles.add("Premium 3");
        listaDescris.add("Controla hasta 20 dispositivos");
        listaPrecios.add("40€");
*/
        getObjectFromShop();



    }

    private class InventoryCallback implements Inventory.Callback {
        @Override
        public void onLoaded(Inventory.Products products) {
            for(Inventory.Product product : products){
                if(product.isPurchased(actualSku)){
                    Purchase purchase1 = product.getPurchaseInState(actualSku, Purchase.State.PURCHASED);
                    if (purchase1 != null) {
                        consume(purchase1);
                    }
                }
                for(int i=0;i<product.getSkus().size();i++) {
                    listaTitles.add(product.getSkus().get(i).getDisplayTitle());
                    listaDescris.add(product.getSkus().get(i).description);
                    listaPrecios.add(product.getSkus().get(i).price);
                    listaSkus.add(product.getSkus().get(i).title);
                }
           // product.getSkus().get(0).
            }

            populateListing();

           // mCheckout.destroyPurchaseFlow();
           // mCheckout.startPurchaseFlow(ProductTypes.IN_APP, actualSku, null, new PurchaseListener());
        }
    }

    public void getObjectFromShop(){
        mInventory = mCheckout.makeInventory();
        mInventory.load(Inventory.Request.create()
                .loadAllPurchases(),new InventoryCallback());



    }

    void populateListing(){
        LinearLayout ll = (LinearLayout) findViewById(R.id.listaChild);
        ll.removeAllViews();

        if(listaTitles.size()>0) {

            LayoutInflater inflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (int i = 0; i<listaTitles.size();i++) {
                LinearLayout kl = (LinearLayout) inflater.inflate(R.layout.row_premium, ll, false);
                kl.setTag(i);
                TextView tv = (TextView) kl.findViewById(R.id.titleProduct);
                tv.setText(listaTitles.get(i));

                tv = (TextView) kl.findViewById(R.id.titleDispositivos);
                tv.setText(listaDescris.get(i));

                tv = (TextView) kl.findViewById(R.id.titlePrecio);
                tv.setText(listaPrecios.get(i)+" / año");

                kl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(app.getUsuario().getPlan()<2) {
                            int numero = (int)view.getTag();
                            actualSku = listaSkus.get(numero);
                            if(actualSku.equals("premiuma")){
                                plan = 2;
                            }
                            else if(actualSku.equals("premiumb")){
                                plan =3;
                            }
                            else if(actualSku.equals("premiumc")){
                                plan = 4;
                            }

                            mCheckout.destroyPurchaseFlow();
                            mCheckout.startPurchaseFlow(ProductTypes.IN_APP, actualSku, null, new PurchaseListener());
                            //getObjectFromShop();
                        }
                        else{
                            //check if purchased
                            Toast toast =
                                    Toast.makeText(getApplicationContext(),
                                            "¡Ya eres Premium!", Toast.LENGTH_SHORT);


                            toast.show();
                        }

                    }
                });
                ll.addView(kl);
            }
        }
        else{
            ll.setVisibility(View.INVISIBLE);
        }
    }


    public void consume(final Purchase purchase) {
        mCheckout.whenReady(new Checkout.EmptyListener() {
            @Override
            public void onReady(@Nonnull BillingRequests requests) {
                requests.consume(purchase.token, makeRequestListener());
            }
        });
    }

    private class PurchaseListener extends EmptyRequestListener<Purchase> {
        @Override
        public void onError(int response, @Nonnull Exception e) {
            super.onError(response, e);
            // Toast.makeText(MenuActivity.this, "Error "+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            //unlock();
        }

        @Override
        public void onSuccess(@Nonnull Purchase result) {
            //   Toast.makeText(MenuActivity.this, "purchased", Toast.LENGTH_SHORT).show();
            consume(result);
        }
    }

    private <T> RequestListener<T> makeRequestListener() {
        return new RequestListener<T>() {
            @Override
            public void onSuccess(@Nonnull T result) {

                handlerPremium = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        super.run();


                        JSONPremium jsonChild = new JSONPremium(ActivityPremium.this);
                        try {
                            final int resPrem = jsonChild.run(app.getUsuario().getId(),plan);
                            handlerPremium.post(new Runnable() {
                                public void run() {
                                    if(resPrem==1) {
                                        AlertDialog.Builder alert;
                                        alert = new AlertDialog.Builder(ActivityPremium.this);

                                        alert.setTitle(R.string.app_name);
                                        alert.setMessage("Compra completada. ¡Disfruta ahora de las funcionalidades Premium y administra más dispositivos!");

                                        alert.setPositiveButton(R.string.aceptar,
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog,
                                                                        int whichButton) {
                                                        //call your method here then finish activity;
                                                        app.getUsuario().setPlan(plan);
                                                        app.guardarEnPreferencias(ActivityPremium.this);
                                                        finish();


                                                    }


                                                });


                                        alert.show();
                                    }
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }.start();


            }
            @Override
            public void onError(int response, @Nonnull Exception e) {
                Toast.makeText(ActivityPremium.this, "Ha habido un error al intentar realizar la compra. Por favor, vuelve a intentarlo más tarde", Toast.LENGTH_SHORT).show();
            }
        };
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();


        app.cargarAplicacionDePreferencias(this);

    }


    @Override
    protected void onResume() {
        super.onResume();


    }
}
