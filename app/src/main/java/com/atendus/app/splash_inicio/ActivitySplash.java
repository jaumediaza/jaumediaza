package com.atendus.app.splash_inicio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.atendus.app.ActivityPadre;
import com.atendus.app.ActivityPermisos;
import com.atendus.app.ActivityPrincipal;
import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.login.ActivityPassword;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.ViewPagerManager;

import spencerstudios.com.bungeelib.Bungee;

import static com.atendus.app.ActivityPermisos.accessibilityPermission;
import static com.atendus.app.ActivityPermisos.adminPermission;
import static com.atendus.app.ActivityPermisos.contactPermission;
import static com.atendus.app.ActivityPermisos.usageStatPermission;
import static com.atendus.app.ActivityPermisos.defaultPermission;
import static com.atendus.app.ActivityPermisos.locationPermission;
import static com.atendus.app.ActivityPermisos.callPermission;
import static com.atendus.app.ActivityPermisos.notificationPermission;


public class ActivitySplash extends AppCompatActivity {

    private static final int DURACION_PANTALLA = 2000;
    private Aplicacion app=new Aplicacion();
    private Activity activity;
    private Context contexto;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        activity=this;
        contexto=this;




        if (ViewPagerManager.getInstance().isAppRunning()){

            app.cargarAplicacionDePreferencias(this);
            SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
            int isPadre = -1;
            if(app.getUsuario()!=null){
                isPadre = app.getUsuario().getIsPadre();//preferences.getInt("padre",-1);
            }

            if ((preferences.getBoolean("defaultPermission",false) && preferences.getBoolean("accessibilityPermission",false) && preferences.getBoolean("locationPermission",false) && preferences.getBoolean("usageStatPermission",false) && preferences.getBoolean("adminPermission",false) && preferences.getBoolean("contactPermission",false) ==true && preferences.getBoolean("callPermission",false)) || isPadre==1){
                Intent intent = new Intent(ActivitySplash.this, ActivityPrincipal.class);
                startActivity(intent);
                Bungee.fade(activity);
                finish();
            }
            else if((preferences.getBoolean("defaultPermission",false) && preferences.getBoolean("accessibilityPermission",false) && preferences.getBoolean("locationPermission",false) && preferences.getBoolean("usageStatPermission",false) && preferences.getBoolean("adminPermission",false) && preferences.getBoolean("contactPermission",false) ==true && preferences.getBoolean("callPermission",false)) && isPadre==0 && !app.isUsuarioSinRegistro()){
                Intent intent = new Intent(ActivitySplash.this, ActivityPassword.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }else if(isPadre==0){
                Intent i;
                if(preferences.getInt("selectedSex",-1)!=-1) {
                    i = new Intent(ActivitySplash.this, ActivityPermisos.class);
                }
                else{
                    i = new Intent(ActivitySplash.this, ActivityPadre.class);
                }
                startActivity(i);
                Bungee.fade(activity);
                finish();
            }
            else if (isPadre==-1){
                Intent i = new Intent(ActivitySplash.this, ActivityPadre.class);
                startActivity(i);
                Bungee.fade(activity);
                finish();
            }
            else{
                Intent i;

                i = new Intent(ActivitySplash.this, ActivityPermisos.class);

                startActivity(i);
                Bungee.fade(activity);
                finish();
            }

        }
        else
        {
            setContentView(R.layout.activity_splash);
            PhoneStateManager.getInstance().cargarDatosDeAplicacion(this);
            PhoneStateManager.getInstance().refrescarEstadisticasDeUso(this);
            app.cargarAplicacionDePreferencias(this);


            ViewPagerManager.getInstance().setAppRunning(true);
            new CountDownTimer(DURACION_PANTALLA, DURACION_PANTALLA) {
                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    if(app.isUsuarioConectado() || app.isUsuarioSinRegistro())
                    {



                        SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                        int isPadre = -1;
                        if(app.getUsuario()!=null){
                            isPadre = app.getUsuario().getIsPadre();//preferences.getInt("padre",-1);
                        }

                        if ((preferences.getBoolean("defaultPermission",false) && preferences.getBoolean("accessibilityPermission",false) && preferences.getBoolean("locationPermission",false) && preferences.getBoolean("usageStatPermission",false) && preferences.getBoolean("adminPermission",false) && preferences.getBoolean("contactPermission",false) ==true && preferences.getBoolean("callPermission",false))  || isPadre==1){
                            Intent intent = new Intent(ActivitySplash.this, ActivityPrincipal.class);
                            startActivity(intent);
                            Bungee.fade(activity);
                            finish();
                        }
                        else if((preferences.getBoolean("defaultPermission",false) && preferences.getBoolean("accessibilityPermission",false) && preferences.getBoolean("locationPermission",false) && preferences.getBoolean("usageStatPermission",false) && preferences.getBoolean("adminPermission",false) && preferences.getBoolean("contactPermission",false) ==true && preferences.getBoolean("callPermission",false)) && isPadre==0 && !app.isUsuarioSinRegistro()){
                            Intent intent = new Intent(ActivitySplash.this, ActivityPassword.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if(isPadre==0){
                            Intent i;
                            if(preferences.getInt("selectedSex",-1)!=-1) {
                                i = new Intent(ActivitySplash.this, ActivityPermisos.class);
                            }
                            else{
                                i = new Intent(ActivitySplash.this, ActivityPadre.class);
                            }
                            startActivity(i);
                            Bungee.fade(activity);
                            finish();
                        }
                        else if (isPadre==-1){
                            Intent i = new Intent(ActivitySplash.this, ActivityPadre.class);
                            startActivity(i);
                            Bungee.fade(activity);
                            finish();
                        }
                        else{
                            Intent i;

                            i = new Intent(ActivitySplash.this, ActivityPermisos.class);

                            startActivity(i);
                            Bungee.fade(activity);
                            finish();
                        }




                    }
                    else
                    {
                        Intent i = new Intent(ActivitySplash.this, ActivityInicio.class);
                        startActivity(i);
                        Bungee.fade(activity);




                        finish();
                    }

                }
            }.start();


        }

        }


}
