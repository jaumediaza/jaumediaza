package com.atendus.app.splash_inicio;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.login.ActivityBienvenida;
import com.atendus.app.login.ActivityLogin;
import com.atendus.app.login.ActivityRegistro;

import spencerstudios.com.bungeelib.Bungee;


public class ActivityInicio extends AppCompatActivity {
    Typeface Roboto_Medium=null;
    Typeface Roboto_Light=null;
    Button btn_entrar,btn_registrarse;

    TextView txtNoregistro,textView5;
    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        activity=this;

        Roboto_Medium=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Medium.ttf");
        Roboto_Light=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Light.ttf");

        btn_entrar= findViewById(R.id.btn_entrar);
        btn_registrarse= findViewById(R.id.btn_registrarse);
        btn_entrar.setTypeface(Roboto_Medium);
        btn_registrarse.setTypeface(Roboto_Medium);

        txtNoregistro= findViewById(R.id.txtNoregistro);
        textView5= findViewById(R.id.textView5);


        txtNoregistro.setTypeface(Roboto_Light);
        textView5.setTypeface(Roboto_Medium);



        findViewById(R.id.btn_entrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityInicio.this,ActivityLogin.class);
                startActivity(i);
                Bungee.fade(activity);
                finish();
            }
        });

        findViewById(R.id.btn_registrarse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityInicio.this,ActivityRegistro.class);
                startActivity(i);
                Bungee.fade(activity);
                finish();
            }
        });

        findViewById(R.id.ly_no_registro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ActivityInicio.this, ActivityBienvenida.class);
                i.putExtra("Registro", 0);
                Aplicacion a = new Aplicacion();
                a.cargarAplicacionDePreferencias(ActivityInicio.this);
                a.setUsuarioSinRegistro(true);
                a.guardarEnPreferencias(activity);
                startActivity(i);




                Bungee.fade(activity);
                finish();
            }
        });
        /*Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        if (!app.isDeviceAdmin()) {
            //resultado=false;
            new android.app.AlertDialog.Builder(this).setTitle("BFree necesita permisos de administrador")
                    .setCancelable(false)
                    .setMessage(getResources().getText(R.string.mensaje_permiso_admin)).setPositiveButton("Aceptar",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ComponentName mDeviceAdminSample;
                            mDeviceAdminSample = new ComponentName(ActivityInicio.this, AdminReceiver.class);
                            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                                    mDeviceAdminSample);

                            startActivity(intent);
                        }
                    }).show();
        }*/
    }
}
