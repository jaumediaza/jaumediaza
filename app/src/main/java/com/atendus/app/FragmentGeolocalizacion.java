package com.atendus.app;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.DispositivosControlados;
import com.atendus.app.json.JSONDispositivosControlados;
import com.atendus.app.json.JSONLocationSearch;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class FragmentGeolocalizacion extends Fragment implements OnMapReadyCallback {

    private View view;
    private Context context;
    public Switch sw_bloquear_notif;
    SupportMapFragment map;
    private GoogleMap mapa;
    List<DispositivosControlados> kidsList;
    int selectedKid = 0;
    LayoutInflater inf;
    ViewGroup cont;
    Location loc;
    Handler handlerChild;

    private boolean aux = true;

    //public static

    public FragmentGeolocalizacion() {}

    public static FragmentGeolocalizacion newInstance() {
        FragmentGeolocalizacion fragmentFirst = new FragmentGeolocalizacion();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kGeolocalizacion);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_geolocalizacion, container, false);

        init();

        inf = inflater;
        cont = container;
        /*DispositivosControlados dispon = new DispositivosControlados();

        dispon.setNombrehijo("Manuel");
        dispon.setSexo(1);
        dispon.setLatitud(41.3887901);
        dispon.setLongitud(2.1589899);
        kidsList.add(dispon);
        dispon.setLatitud(40.4167);
        dispon.setLongitud(-3.70325);
        kidsList.add(dispon);
        final Aplicacion app = new Aplicacion();
        List<DispositivosControlados> dispo = new ArrayList<>();
        app.cargarAplicacionDePreferencias(context);
        dispo = app.getDispositivosControlados();
        for(DispositivosControlados disp : dispo){
            if(disp.getSexo()!=-1){
                kidsList.add(disp);
            }
        }*/

        selectedKid = 0;


        map = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapView);
        map.getMapAsync(this);


        return view;
    }

    public void populateListing(){
        final Handler handler = new Handler();
        new Thread(){
            @Override
            public void run() {
                super.run();
                final Aplicacion appi = new Aplicacion();
                List<DispositivosControlados> dispo = new ArrayList<>();
                appi.cargarAplicacionDePreferencias(getActivity());
                JSONDispositivosControlados js = new JSONDispositivosControlados(getActivity());
                try {
                    dispo = js.run(appi.getUsuario().getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                kidsList =  dispo;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        populateListingN();
                    }
                });
            }
        }.start();
    }

    public void populateListingN(){
        LinearLayout ll = (LinearLayout) view.findViewById(R.id.listaChild);
        ll.removeAllViews();
        Button button = (Button) view.findViewById(R.id.search_button);
        TextView tvx = (TextView) view.findViewById(R.id.textNoreg);
        if(kidsList!=null) {
            if (kidsList.size() > 0) {

                for (int i = kidsList.size() - 1; i >= 0; i--) {
                    if (kidsList.get(i).getSexohijo() == -1) {
                        kidsList.get(i).setSexohijo(0);
                    }
                }

                tvx.setVisibility(View.INVISIBLE);
                ll.setVisibility(View.VISIBLE);
                button.setVisibility(View.VISIBLE);

                for (int i = 0; i < kidsList.size(); i++) {
                    DispositivosControlados disp = kidsList.get(i);
                    LinearLayout kl = (LinearLayout) inf.inflate(R.layout.row_kids, cont, false);

                    ImageView iv = (ImageView) kl.findViewById(R.id.imgChild);
                    TextView tv = (TextView) kl.findViewById(R.id.txtNameChild);
                    View v = (View) kl.findViewById(R.id.underline);
                    if (disp.getNombrehijo() != null)
                        tv.setText(disp.getNombrehijo());
                    if (selectedKid == i) {
                        v.setVisibility(View.VISIBLE);
                    } else {
                        v.setVisibility(View.INVISIBLE);
                    }
                    if (disp.getSexohijo() == 0) { //chico
                        iv.setImageResource(R.mipmap.nino);
                    } else if (disp.getSexohijo() == 1) {  //chica
                        iv.setImageResource(R.mipmap.nina);
                    } else {
                        iv.setImageResource(R.mipmap.nino);
                    }
                    kl.setTag(i);
                    kl.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int numero = (int) view.getTag();
                            selectedKid = numero;
                            populateListingN();


                        }
                    });
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            loc = null;
                            handlerChild = new Handler();

                            new Thread() {
                                @Override
                                public void run() {
                                    super.run();
                                    JSONLocationSearch jsonLoc = new JSONLocationSearch(getActivity());
                                    DispositivosControlados disp = kidsList.get(selectedKid);

                                    try {

                                        //final String iddisp = telephonyManager.getDeviceId();
                                        loc = jsonLoc.run(disp.getId_usuario(), disp.getIddisp());
                                        handlerChild.post(new Runnable() {
                                            public void run() {
                                                if (loc != null) {
                                                    moveMapToPosition();
                                                }
                                            }
                                        });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }.start();
                        }

                    });
                    ll.addView(kl);
                }
            } else {
                ll.setVisibility(View.INVISIBLE);
                button.setVisibility(View.INVISIBLE);
                tvx.setVisibility(View.VISIBLE);
            }
        }
    }

    void moveMapToPosition(){
        DispositivosControlados disp = kidsList.get(selectedKid);
        LatLng latLng = new LatLng(loc.getLatitude(),loc.getLongitude());
        mapa.clear();
        mapa.addMarker(new MarkerOptions()
                .position(latLng)
                .title(disp.getNombrehijo()));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18);
        mapa.animateCamera(cameraUpdate);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
     //   mapa.setMinZoomPreference(1);
      //  LatLng ny = new LatLng(40.7143528, -74.0059731);
       // gmap.moveCamera(CameraUpdateFactory.newLatLng(ny));
    }

    private void init() {




    }

    @Override
    public void onResume() {
        super.onResume();
        populateListing();

    }

    public void updateRootView(View v){
        view = v;
    }

}
