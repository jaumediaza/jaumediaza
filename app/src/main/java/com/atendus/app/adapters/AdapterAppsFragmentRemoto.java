package com.atendus.app.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.remoto.ActivityPrincipalRemoto;
import com.squareup.picasso.Picasso;


/**
 * Created by Alberto on 23/06/2018.
 */

public class AdapterAppsFragmentRemoto extends BaseAdapter {

    private Context context;

    public AdapterAppsFragmentRemoto(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return ActivityPrincipalRemoto.appsPermitidas.size();
    }

    @Override
    public AplicacionPermitida getItem(int position) {
        return ActivityPrincipalRemoto.appsPermitidas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.celda_app_fragment, viewGroup, false);
        }

        ImageView imagen = view.findViewById(R.id.imagen);
        TextView label = view.findViewById(R.id.label);
        final ConstraintLayout lyMain = view.findViewById(R.id.ly_main);
        final ImageView imgCircle = view.findViewById(R.id.imageView14);

        label.setText(ActivityPrincipalRemoto.appsPermitidas.get(position).getAppLabel());

        String url = context.getResources().getString(R.string.url_webservices) + "userdata/appimages/" + ActivityPrincipalRemoto.appsPermitidas.get(position).getPackageName() + ".png";
        Picasso.get().load(url).into(imagen);

        Switch sw = view.findViewById(R.id.sw);
        sw.setChecked(ActivityPrincipalRemoto.appsPermitidas.get(position).isUsoDuranteDesconecta());
        if (ActivityPrincipalRemoto.appsPermitidas.get(position).isUsoDuranteDesconecta()) {
            //lyMain.setBackgroundColor(Color.parseColor("#b47eb5d4"));
            imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle3));
        } else {
            // lyMain.setBackgroundColor(Color.parseColor("#00ffffff"));
            imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle));
        }
        lyMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityPrincipalRemoto.appsPermitidas.get(position).isUsoDuranteDesconecta()) {
                    ActivityPrincipalRemoto.appsPermitidas.get(position).setUsoDuranteDesconecta(false);
                    ActivityPrincipalRemoto.appsPermitidas.get(position).setPermitida(false);
                    //lyMain.setBackgroundColor(Color.parseColor("#00ffffff"));
                    imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle));
                } else {
                    ActivityPrincipalRemoto.appsPermitidas.get(position).setUsoDuranteDesconecta(true);
                    ActivityPrincipalRemoto.appsPermitidas.get(position).setPermitida(true);
                    //lyMain.setBackgroundColor(Color.parseColor("#b47eb5d4"));
                    imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle3));
                }
            }
        });

        return view;
    }
}