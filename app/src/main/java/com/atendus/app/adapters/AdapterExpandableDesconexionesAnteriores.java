package com.atendus.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.Desconexion;
import com.atendus.app.clases.NotificacionInfo;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class AdapterExpandableDesconexionesAnteriores extends BaseExpandableListAdapter {

    private List<Desconexion> desconexiones;
    private Context context;
    public AdapterExpandableDesconexionesAnteriores(Context c){
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(c);
        desconexiones = app.getDesconexiones();
        context = c;
        Collections.reverse(desconexiones);

    }

    @Override
    public int getGroupCount() {
        return desconexiones.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return desconexiones.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return desconexiones.get(i);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.celda_cabecera_deconexiones, null);
        }

        TextView tv = view.findViewById(R.id.txt_fecha);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(desconexiones.get(i).getFechaInicio()));

        String text = (cal.get(Calendar.DAY_OF_MONTH)) + " ";
        switch (cal.get(Calendar.MONTH)){
            case 0:{
                text = text.concat(context.getString(R.string.enero));
                break;
            }
            case 1:{
                text = text.concat(context.getString(R.string.febrero));
                break;
            }
            case 2:{
                text = text.concat(context.getString(R.string.marzo));
                break;
            }
            case 3:{
                text = text.concat(context.getString(R.string.abril));
                break;
            }
            case 4:{
                text = text.concat(context.getString(R.string.mayo));
                break;
            }
            case 5:{
                text = text.concat(context.getString(R.string.junio));
                break;
            }
            case 6:{
                text = text.concat(context.getString(R.string.julio));
                break;
            }
            case 7:{
                text = text.concat(context.getString(R.string.agosto));
                break;
            }
            case 8:{
                text = text.concat(context.getString(R.string.septiembre));
                break;
            }
            case 9:{
                text = text.concat(context.getString(R.string.octubre));
                break;
            }
            case 10:{
                text = text.concat(context.getString(R.string.noviembre));
                break;
            }
            case 11:{
                text = text.concat(context.getString(R.string.diciembre));
                break;
            }
        }

        tv.setText(text);

        TextView txt_titulo = view.findViewById(R.id.txt_titulo);
        cal = Calendar.getInstance();
        cal.setTime(new Date(desconexiones.get(i).getFechaInicio()));
        String strInicio;
        if (cal.get(Calendar.HOUR_OF_DAY)<= 9){
            strInicio = "0" +cal.get(Calendar.HOUR_OF_DAY);
        }else{
            strInicio = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        }
        if(cal.get(Calendar.MINUTE) <=9){
            strInicio = strInicio.concat(":0" + cal.get(Calendar.MINUTE));
        }else {
            strInicio = strInicio.concat(":" + cal.get(Calendar.MINUTE));
        }

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date(desconexiones.get(i).getFechaFin()));
        String strFin;
        if (cal2.get(Calendar.HOUR_OF_DAY)<= 9){
            strFin = "0" +cal2.get(Calendar.HOUR_OF_DAY);
        }else{
            strFin = String.valueOf(cal2.get(Calendar.HOUR_OF_DAY));
        }
        if(cal2.get(Calendar.MINUTE) <=9){
            strFin = strFin.concat(":0" + cal2.get(Calendar.MINUTE));
        }else {
            strFin = strFin.concat(":" + cal2.get(Calendar.MINUTE));
        }

        String str = strInicio + " - " +strFin;

        txt_titulo.setText(str);

        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.contenido_cabecera_desconexion, null);
        }

        TextView txtInicio = view.findViewById(R.id.txt_inicio);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(desconexiones.get(i).getFechaInicio()));
        String strInicio;
        if (cal.get(Calendar.HOUR_OF_DAY)<= 9){
            strInicio = "0" +cal.get(Calendar.HOUR_OF_DAY);
        }else{
            strInicio = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
        }
        if(cal.get(Calendar.MINUTE) <=9){
            strInicio = strInicio.concat(":0" + cal.get(Calendar.MINUTE));
        }else {
            strInicio = strInicio.concat(":" + cal.get(Calendar.MINUTE));
        }
        txtInicio.setText(strInicio);

        TextView txtFin = view.findViewById(R.id.txt_fin);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date(desconexiones.get(i).getFechaFin()));
        String strFin;
        if (cal2.get(Calendar.HOUR_OF_DAY)<= 9){
            strFin = "0" +cal2.get(Calendar.HOUR_OF_DAY);
        }else{
            strFin = String.valueOf(cal2.get(Calendar.HOUR_OF_DAY));
        }
        if(cal2.get(Calendar.MINUTE) <=9){
            strFin = strFin.concat(":0" + cal2.get(Calendar.MINUTE));
        }else {
            strFin = strFin.concat(":" + cal2.get(Calendar.MINUTE));
        }
        txtFin.setText(strFin);

        Switch sw_notif = view.findViewById(R.id.sw_notificaciones);
        Switch sw_llamad = view.findViewById(R.id.sw_llamadas);

        sw_notif.setChecked(desconexiones.get(i).isAppsPermitidas());
        sw_llamad.setChecked(desconexiones.get(i).isLlamadasPermitidas());

        TextView txtDuracion = view.findViewById(R.id.txt_duracion);
        String strDuracion = "";
        int horas =cal2.get(Calendar.HOUR_OF_DAY) - cal.get(Calendar.HOUR_OF_DAY);
        if(horas<0){
            horas = 24 + horas;
        }
        int minutos = cal2.get(Calendar.MINUTE) - cal.get(Calendar.MINUTE);
        if (minutos<0){
            minutos = 60 + minutos;
        }
        if (horas > 1){
            if (horas-1>1)
                strDuracion = String.valueOf(horas-1)+ " "+ context.getString(R.string.horas);
            else
                strDuracion = String.valueOf(horas-1)+ " "+ context.getString(R.string.hora);

            if (minutos >1)
                strDuracion = strDuracion.concat(" " + minutos + " " + context.getString(R.string.minutos));
            else
                strDuracion =  strDuracion.concat(" " + minutos + " " + context.getString(R.string.minuto));

        }else {
            if (minutos >1)
                strDuracion = minutos + " " + context.getString(R.string.minutos);
            else
                strDuracion = minutos + " " + context.getString(R.string.minuto);
        }
        if (minutos == 0 && horas == 0){
            strDuracion = ">1 " + context.getString(R.string.minuto);
        }
        txtDuracion.setText(strDuracion);


        NotificacionInfo.NotificacionInfoList notifs = desconexiones.get(i).getNotificaciones();
        notifs.updateItems();
        if (notifs.getList().size() <= 0){
            view.findViewById(R.id.txt_no_notif).setVisibility(View.VISIBLE);
            view.findViewById(R.id.ly_notifs).setVisibility(View.GONE);
        }else{
            view.findViewById(R.id.txt_no_notif).setVisibility(View.GONE);
            view.findViewById(R.id.ly_notifs).setVisibility(View.VISIBLE);
        }
        LayoutInflater inflater = LayoutInflater.from(context);
        LinearLayout ly_notifs = view.findViewById(R.id.ly_notifs);
        ly_notifs.removeAllViews();
        for (int j = 0; j < notifs.getList().size(); ++j){
            View v = inflater.inflate(R.layout.celda_norificacion2, null);

            NotificacionInfo notif = notifs.getList().get(j);
            final String pckgname = notif.getPackageName();


            ImageView img_logo = v.findViewById(R.id.img_logo);
            TextView txt_appname = v.findViewById(R.id.txt_appname);
            TextView txt_veces = v.findViewById(R.id.txt_veces);
            TextView txt_tiempo = v.findViewById(R.id.txt_tiempo);
            ConstraintLayout ly_main = v.findViewById(R.id.ly_main);


            if (notif.getPackageName().equals("Llamada")){
                img_logo.setImageDrawable(context.getDrawable(R.drawable.telephone2));
            }else {
                img_logo.setImageDrawable(notif.getIcon());
            }
            txt_appname.setText(notif.getAppname());
            String reltime = String.valueOf(DateUtils.getRelativeTimeSpanString (notif.getLastdate()
                    .getTime(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
            txt_tiempo.setText(reltime);
            int total = notif.getTotalNotificaciones()/2;
            total /= 2;
            total++;
            String str;
            if (total == 1)
                str = total + " " + context.getString(R.string.vez);
            else
                str = total + " " + context.getString(R.string.veces);
            txt_veces.setText(str);
            ly_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = context.getPackageManager()
                            .getLaunchIntentForPackage(pckgname);
                    if (intent != null) {
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                    if (pckgname.equals("Llamada")){
                        Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
                                "tel", "", null));
                        context.startActivity(phoneIntent);
                    }

                }

            });

            ly_notifs.addView(v);
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
