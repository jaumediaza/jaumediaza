package com.atendus.app.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.atendus.app.ActivityProgramarDesconexion;
import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.DesconexionProgramada;
import com.atendus.app.json.JSONEnviarDesconexionProgramadaModificar;

import org.json.JSONException;

import java.util.Collections;




public class AdapterExpandableDesconexionesProgramadas extends BaseExpandableListAdapter {

    private DesconexionProgramada.DesconexionProgramadaList desconexiones;
    private Context context;
    public AdapterExpandableDesconexionesProgramadas(Context c){
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(c);
        desconexiones = app.getDesconexionesProgramadas();
        context = c;
        Collections.reverse(desconexiones.getList());

    }

    public void recargar(){
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        desconexiones = app.getDesconexionesProgramadas();
        Collections.reverse(desconexiones.getList());
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return desconexiones.getList().size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return desconexiones.getList().get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return desconexiones.getList().get(i);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.celda_cabecera_deconexiones_programadas, null);
        }

        TextView tv = view.findViewById(R.id.txt_titulo);
        tv.setText(desconexiones.getList().get(i).getStrTitulo());





        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {


        if (view == null){
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.contenido_cabecera_desconexion_programada, null);
        }
        final int pos = i;




        final Switch sw_activado = view.findViewById(R.id.sw_activado);
        sw_activado.setChecked(desconexiones.getList().get(i).isEnabled());
        sw_activado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Handler handler = new Handler();
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        final Aplicacion app = new Aplicacion();

                        final String iddisp = app.getUsuario().getNombrehijo();
                        JSONEnviarDesconexionProgramadaModificar jsonEnviarDesconexionProgramadaModificar
                                = new JSONEnviarDesconexionProgramadaModificar(context);
                        if (desconexiones.getList().get(pos).isEnabled())
                            desconexiones.getList().get(pos).setEnabled(false);
                        else desconexiones.getList().get(pos).setEnabled(true);
                        try {
                            jsonEnviarDesconexionProgramadaModificar.run(app.getUsuario().getId(), iddisp,
                                    desconexiones.getList().get(pos));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (desconexiones.getList().get(pos).isEnabled()) {
                                    sw_activado.setChecked(true);
                                }else{
                                    sw_activado.setChecked(false);
                                }


                                app.cargarAplicacionDePreferencias(context);
                                Collections.reverse(app.getDesconexionesProgramadas().getList());
                                app.getDesconexionesProgramadas().getList().get(pos).setEnabled(sw_activado.isChecked());
                                Collections.reverse(app.getDesconexionesProgramadas().getList());
                                app.guardarEnPreferencias(context);
                            }
                        });
                    }
                }.start();


            }
        });

        final DesconexionProgramada desc = desconexiones.getList().get(pos);

        view.findViewById(R.id.edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityProgramarDesconexion.class);
                intent.putExtra("Edit", true);
                intent.putExtra("id", desconexiones.getList().get(pos).getId());
                context.startActivity(intent);
            }
        });

        TextView txtEmpieza = view.findViewById(R.id.txtEmpieza);
        TextView txtTermina = view.findViewById(R.id.txtTermina);
        TextView txtRepetir = view.findViewById(R.id.txtRepetir);
        TextView txtDuracion = view.findViewById(R.id.txtDuracion);

        int horaInicio = desc.getHoraInicio();
        int minutoInicio = desc.getMinutoInicio();
        String str;
        int aux = horaInicio/10;
        str = String.valueOf(aux);

        aux = horaInicio - aux*10;
        str = str.concat(String.valueOf(aux));

        aux = minutoInicio/10;
        str = str.concat(":"+ String.valueOf(aux));
        aux = minutoInicio - aux*10;
        str = str.concat(String.valueOf(aux));

        txtEmpieza.setText(str);


        int horaTermina = desc.getHoraFin();
        int minutoTermina = desc.getMinutoFin();

        aux = horaTermina/10;
        str = String.valueOf(aux);
        aux = horaTermina - aux*10;
        str = str.concat(String.valueOf(aux));

        aux = minutoTermina/10;
        str = str.concat(":" + String.valueOf(aux));
        aux = minutoTermina - aux*10;
        str = str.concat(String.valueOf(aux));


        txtTermina.setText(str);

        String strDuracion = "";

        int horas = horaTermina - horaInicio;
        if(horas<0){
            horas = 24 + horas;
        }
        int minutos = minutoTermina - minutoInicio;

        if (minutos<0){
            horas--;
            minutos = 60 + minutos;
        }
        if (horas >= 1){
            if (horas!=1)
                strDuracion = String.valueOf(horas)+ " h";
            else
                strDuracion = String.valueOf(horas)+ " h";

            if (minutos !=1)
                strDuracion = strDuracion.concat(" " + minutos + " m");
            else
                strDuracion =  strDuracion.concat(" " + minutos + " m");

        }else {
            if (minutos >1)
                strDuracion = minutos + " m";
            else
                strDuracion = minutos + " m";
        }
        if (minutos == 0 && horas == 0){
            strDuracion = ">1 m";
        }

        txtDuracion.setText(strDuracion);

        if (desc.getRepetir() == 1){
            txtRepetir.setText(context.getString(R.string.no));
        }else{
            txtRepetir.setText(context.getString(R.string.si));
        }

        setDia((LinearLayout) view.findViewById(R.id.lyLunes),
                (TextView) view.findViewById(R.id.txtLunes), desc.getDias()[0]);

        setDia((LinearLayout) view.findViewById(R.id.lyMartes),
                (TextView) view.findViewById(R.id.txtMartes), desc.getDias()[1]);

        setDia((LinearLayout) view.findViewById(R.id.lyMiercoles),
                (TextView) view.findViewById(R.id.txtMiercoles), desc.getDias()[2]);

        setDia((LinearLayout) view.findViewById(R.id.lyJueves),
                (TextView) view.findViewById(R.id.txtJueves), desc.getDias()[3]);

        setDia((LinearLayout) view.findViewById(R.id.lyViernes),
                (TextView) view.findViewById(R.id.txtViernes), desc.getDias()[4]);

        setDia((LinearLayout) view.findViewById(R.id.lySabado),
                (TextView) view.findViewById(R.id.txtSabado), desc.getDias()[5]);

        setDia((LinearLayout) view.findViewById(R.id.lyDomingo),
                (TextView) view.findViewById(R.id.txtDomingo), desc.getDias()[6]);

        view.findViewById(R.id.eliminar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Intent intent = new Intent(context, ActivityProgramarDesconexion.class);
                intent.putExtra("Edit", true);
                intent.putExtra("Delete", true);
                intent.putExtra("id", desconexiones.getList().get(pos).getId());
                context.startActivity(intent);


            }
        });

        if (!desconexiones.getList().get(pos).isEditable()){
            sw_activado.setEnabled(false);
            view.findViewById(R.id.edit).setEnabled(false);
            view.findViewById(R.id.eliminar).setEnabled(false);
            view.findViewById(R.id.edit).setVisibility(View.GONE);
            view.findViewById(R.id.eliminar).setVisibility(View.GONE);
        }else{
            sw_activado.setEnabled(true);
            view.findViewById(R.id.edit).setEnabled(true);
            view.findViewById(R.id.eliminar).setEnabled(true);
            view.findViewById(R.id.edit).setVisibility(View.VISIBLE);
            view.findViewById(R.id.eliminar).setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private void setDia(LinearLayout ly, TextView txt, boolean dia){
        if (dia){
            ly.setBackground(context.getDrawable(R.drawable.circle2));
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Regular.ttf");
            txt.setTypeface(type, Typeface.BOLD);
            txt.setTextColor(context.getColor(R.color.colorAccent));
        }else{
            ly.setBackground(null);
            Typeface type = Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Thin.ttf");
            txt.setTypeface(type, Typeface.BOLD);
            txt.setTextColor(context.getColor(R.color.grisClaro));
        }
    }
}
