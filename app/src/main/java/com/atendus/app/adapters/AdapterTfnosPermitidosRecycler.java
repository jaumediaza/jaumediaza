package com.atendus.app.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;
import android.widget.Switch;
import android.widget.TextView;

import com.atendus.app.ActivityProgramarDesconexion;
import com.atendus.app.R;
import com.atendus.app.clases.Telefono;
import com.atendus.app.utilidades.PhoneStateManager;

import java.util.ArrayList;
import java.util.List;


public class AdapterTfnosPermitidosRecycler extends RecyclerView.Adapter<AdapterTfnosPermitidosRecycler.CustomViewHolder> implements SectionIndexer {

    private List<Telefono> tfnos;
    private List<Telefono> tfnosOriginal;
    private ArrayList<Integer> mSectionPositions;
    private boolean fromProgramarDesconexion;

    public AdapterTfnosPermitidosRecycler(boolean fromProgramarDesconexion) {
        tfnos = new ArrayList<>();
        tfnos.addAll(PhoneStateManager.getInstance().getTfnosPermitidos());
        this.fromProgramarDesconexion = fromProgramarDesconexion;
        tfnosOriginal = new ArrayList<>();
        if (fromProgramarDesconexion)
            for (Telefono t : tfnos) {
                t.setPermitido(false);
                for (int i = 0; i < ActivityProgramarDesconexion.telefonosPermitidos.size(); ++i) {
                    if (ActivityProgramarDesconexion.telefonosPermitidos.get(i).getNumero().equals(t.getNumero())) {
                        t.setPermitido(true);
                    }
                }
            }
        tfnosOriginal.addAll(tfnos);
    }

    public void search(String search) {
        tfnos.clear();
        for (int i = 0; i < tfnosOriginal.size(); ++i) {
            try {
                String str1 = tfnosOriginal.get(i).getNombre().toLowerCase().substring(0, search.length());
                String str2 = search.toLowerCase();
                if (str1.equals(str2)) {
                    tfnos.add(tfnosOriginal.get(i));
                }
            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.celda_tfno_permitido, parent, false);

        CustomViewHolder tvh = new CustomViewHolder(itemView);
        return tvh;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        final Telefono tfno = tfnos.get(position);

        holder.sw.setText(tfno.getNombre());
        holder.sw.setChecked(tfno.isPermitido());
        holder.inicial.setText(String.valueOf(tfno.getLetra()));
        final int pos = tfno.getId();
        holder.sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tfno.isPermitido()) {
                    tfno.setPermitido(false);
                    Telefono taux = null;
                    for (Telefono t : ActivityProgramarDesconexion.telefonosPermitidos) {
                        if (t.getNumero().equals(tfno.getNumero())) {
                            taux = t;
                            break;
                        }
                    }
                    if (taux != null) {
                        ActivityProgramarDesconexion.telefonosPermitidos.remove(taux);
                    }
                } else {
                    tfno.setPermitido(true);
                    ActivityProgramarDesconexion.telefonosPermitidos.add(tfno);
                }

                if (!fromProgramarDesconexion)
                    for (Telefono t : PhoneStateManager.getInstance().getTfnosPermitidos()) {
                        if (t.getNumero().equals(tfno.getNumero())) {
                            t.setPermitido(t.isPermitido());
                        }
                    }
            }
        });
    }

    @Override
    public int getItemCount() {
        return tfnos.size();
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = tfnos.size(); i < size; i++) {
            String section = String.valueOf(String.valueOf(tfnos.get(i).getLetra()).toUpperCase());
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return i;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        Switch sw;
        TextView inicial;

        public CustomViewHolder(View itemView) {
            super(itemView);

            sw = itemView.findViewById(R.id.switch_tfno);
            inicial = itemView.findViewById(R.id.inicial_tfno);
        }
    }


}
