package com.atendus.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.NotificacionInfo;
import com.atendus.app.utilidades.PhoneStateManager;

import java.util.Collections;


public class AdapterNotificaciones extends RecyclerView.Adapter<AdapterNotificaciones.CustomViewholder> {

    private NotificacionInfo.NotificacionInfoList notificaciones;
    private Context context;

    public AdapterNotificaciones(Context c){
        context = c;
        notificaciones = new NotificacionInfo.NotificacionInfoList(
                PhoneStateManager.getInstance().getNotificaciones()
                        .getList());

        Collections.sort(notificaciones.getList());
        Collections.reverse(notificaciones.getList());
    }


    @NonNull
    @Override
    public CustomViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.celda_norificacion, parent, false);
        return new AdapterNotificaciones.CustomViewholder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewholder holder, int position) {
        final int pos = position;
        NotificacionInfo notif = notificaciones.getList().get(position);
        final String pckgname = notif.getPackageName();

        if (notif.getPackageName().equals("Llamada")){
            holder.img_logo.setImageDrawable(context.getDrawable(R.drawable.telephone));
        }else {
            holder.img_logo.setImageDrawable(notif.getIcon());
        }
        holder.txt_appname.setText(notif.getAppname());
        String reltime = String.valueOf(DateUtils.getRelativeTimeSpanString (notif.getLastdate()
                .getTime(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));
        holder.txt_tiempo.setText(reltime);
        int total = notif.getTotalNotificaciones()/2;
        total /= 2;
        total++;
        String str;
        if (total == 1)
            str = total + " " + context.getString(R.string.vez);
        else
            str = total + " " + context.getString(R.string.veces);
        holder.txt_veces.setText(str);
        holder.ly_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = context.getPackageManager()
                        .getLaunchIntentForPackage(pckgname);
                if (intent != null) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                if (pckgname.equals("Llamada")){
                    Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
                            "tel", "", null));
                    context.startActivity(phoneIntent);
                }

            }

        });

    }

    @Override
    public int getItemCount() {
        return notificaciones.getList().size();
    }

    class CustomViewholder extends RecyclerView.ViewHolder{

        ImageView img_logo;
        TextView txt_appname;
        TextView txt_veces;
        TextView txt_tiempo;
        ConstraintLayout ly_main;

        CustomViewholder(View itemView) {
            super(itemView);
            img_logo = itemView.findViewById(R.id.img_logo);
            txt_appname = itemView.findViewById(R.id.txt_appname);
            txt_veces = itemView.findViewById(R.id.txt_veces);
            txt_tiempo = itemView.findViewById(R.id.txt_tiempo);
            ly_main = itemView.findViewById(R.id.ly_main);
        }
    }
}
