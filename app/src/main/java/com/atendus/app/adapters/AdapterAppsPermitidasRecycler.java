package com.atendus.app.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.Switch;

import com.atendus.app.ActivityProgramarDesconexion;
import com.atendus.app.R;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.utilidades.PhoneStateManager;

import java.util.ArrayList;
import java.util.List;


public class AdapterAppsPermitidasRecycler extends RecyclerView.Adapter<AdapterAppsPermitidasRecycler.CustomViewHolder> implements SectionIndexer {
    private List<AplicacionPermitida> apps;
    private List<AplicacionPermitida> appsOriginales;
    private List<AplicacionPermitida> appsSeleccionadas;
    private ArrayList<Integer> mSectionPositions;

    public AdapterAppsPermitidasRecycler(boolean flgProgramarDesconexion) {
        apps = new ArrayList<>();
        apps.addAll(PhoneStateManager.getInstance().getAppsPermitidas());
        appsSeleccionadas = new ArrayList<>();
        if (flgProgramarDesconexion)
            for (AplicacionPermitida app : apps) {
                app.setPermitida(false);
                for (AplicacionPermitida app2 : ActivityProgramarDesconexion.aplicacionPermitidas) {
                    if (app2.getPackageName().equals(app.getPackageName())) {
                        app.setPermitida(true);
                        appsSeleccionadas.add(app);
                    }
                }
            }
        appsOriginales = new ArrayList<>();
        appsOriginales.addAll(apps);
    }


    public void search(String search) {
        apps.clear();
        for (int i = 0; i < appsOriginales.size(); ++i) {
            try {
                String str1 = appsOriginales.get(i).getAppLabel().toLowerCase().substring(0, search.length());
                String str2 = search.toLowerCase();
                if (str1.equals(str2)) {
                    apps.add(appsOriginales.get(i));
                }
            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.celda_app_permitida, parent, false);

        AdapterAppsPermitidasRecycler.CustomViewHolder tvh = new AdapterAppsPermitidasRecycler.CustomViewHolder(itemView);
        return tvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomViewHolder holder, int posicion) {
        final AplicacionPermitida appinfo = apps.get(posicion);
        holder.sw.setChecked(appinfo.isPermitida());
        holder.sw.setText(appinfo.getAppLabel());
        holder.sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (appinfo.isPermitida()) {
                    appsSeleccionadas.remove(appinfo);
                    appinfo.setPermitida(false);
                } else {
                    appsSeleccionadas.add(appinfo);
                    appinfo.setPermitida(true);
                }
            }
        });

        holder.img.setImageDrawable(appinfo.getDrawable());
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = apps.size(); i < size; i++) {
            String section = String.valueOf(apps.get(i).getAppLabel().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return i;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        Switch sw;
        ImageView img;

        public CustomViewHolder(View itemView) {
            super(itemView);
            sw = itemView.findViewById(R.id.switch_app);
            img = itemView.findViewById(R.id.logo_app);
        }

    }

    public List<AplicacionPermitida> getAppsSeleccionadas() {

        appsSeleccionadas.clear();
        for (AplicacionPermitida appPer : apps) {
            if (appPer.isPermitida())
                appsSeleccionadas.add(appPer);
        }

        return appsSeleccionadas;
    }
}
