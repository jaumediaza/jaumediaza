package com.atendus.app;

import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AppUsage;
import com.atendus.app.clases.EstadisticaAplicacion;
import com.atendus.app.remoto.ActivityPrincipalRemoto;
import com.atendus.app.utilidades.TresAndroides;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;



public class FragmentDatosDeUso extends Fragment  implements SeekBar.OnSeekBarChangeListener,
        OnChartValueSelectedListener {

    private final int kTOTALDATOS = 3;

    private View view;
    private Context context;

    private PieChart mChart;
    private BarChart mChart2;
    private String[] mParties ;
    int values[];
    private Drawable drawables[];
    int total;
    private int colors[] = new int[kTOTALDATOS+1];

    List<AppUsage> appUsages;


    private long mTotal;

    private LinearLayout lyEstadisticasLista;
    private LinearLayout lyAct;

    private boolean swGrafico = false;

    private Button btnDia;
    private Button btnSemana;
    private Button btnMes;

    private int periodoEstadisticas = 1;

    private float fValues[];

    private boolean firsttime = true;
    //private ImageView imgActualizar;

    private static boolean ususarioRemoto = false;

    public FragmentDatosDeUso(){}

    public static FragmentDatosDeUso newInstance() {
        FragmentDatosDeUso fragmentFirst = new FragmentDatosDeUso();
        Bundle args = new Bundle();
        //args.putInt("someInt", ActivityPrincipal.kDatosUso);
        args.putString("someTitle", "");
        ususarioRemoto = false;
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }
    public static FragmentDatosDeUso newInstance(boolean u) {
        FragmentDatosDeUso fragmentFirst = new FragmentDatosDeUso();
        Bundle args = new Bundle();
       // args.putInt("someInt", ActivityPrincipal.kDatosUso);
        args.putString("someTitle", "");
        ususarioRemoto = u;
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_estadisticas, container, false);

        if (ususarioRemoto){
            initRemoto();
        }
        else {
            init();
        }


        return view;
    }


    public void init(){

        lyAct = view.findViewById(R.id.lyAct);
        final Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        appUsages = app.getAppUsages();


      /*  AppUsage a = new AppUsage();
        a.setAppName("who");
        a.setColor(ContextCompat.getColor(context, R.color.blanco));
        a.setPackage_name("wat");
        a.setTotalTimeInForeground(1289179);

        appUsages.add(a);

        AppUsage av = new AppUsage();
        av.setAppName("wa");
        av.setColor(ContextCompat.getColor(context, R.color.blanco));
        av.setPackage_name("waat");
        av.setTotalTimeInForeground(2343451);
        appUsages.add(av);

        AppUsage avv = new AppUsage();
        avv.setAppName("was");
        avv.setColor(ContextCompat.getColor(context, R.color.blanco));
        avv.setPackage_name("waaat");
        avv.setTotalTimeInForeground(23332);
        appUsages.add(avv);*/

        mChart =  view.findViewById(R.id.chart1);
        mChart2 =  view.findViewById(R.id.chart2);
        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);


       // swGrafico = app.isChartTypeEstadisticas();

        if (appUsages.size() > 0)
            setListaApps();

        if (!swGrafico) {
            actualizarDatosTarta();
            fab.setImageResource(R.drawable.analytics);
        }
        else {
            actualizarDatosLineal();
            fab.setImageResource(R.drawable.pie_chart);
        }



        /*imgActualizar = view.findViewById(R.id.imgActualizar);
        imgActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lyAct.setVisibility(View.VISIBLE);
                imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        if (periodoEstadisticas == 1)
                            appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                        else if (periodoEstadisticas == 2)
                            appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                        else if(periodoEstadisticas == 3)
                            appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);


                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                if (swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();
                                lyAct.setVisibility(View.GONE);
                                imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();
            }
        });

        */


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Aplicacion app = new Aplicacion();
                //app.cargarAplicacionDePreferencias(context);
                swGrafico = !swGrafico;
                if (swGrafico) {
                    actualizarDatosLineal();
                    fab.setImageResource(R.drawable.pie_chart);
                } else {
                    actualizarDatosTarta();
                    fab.setImageResource(R.drawable.analytics);
                }
                app.setChartTypeEstadisticas(swGrafico);
                app.guardarEnPreferencias(context);
            }
        });


        btnDia = view.findViewById(R.id.btnDia);
        btnSemana = view.findViewById(R.id.btnSemana);
        btnMes = view.findViewById(R.id.btnMes);

        btnDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnDia.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 1;

                lyAct.setVisibility(View.VISIBLE);
                //imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                final Handler handler3 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);

                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                if (!swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();

                                new Thread(){
                                    @Override
                                    public void run() {
                                        super.run();
                                        appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                                        handler3.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (appUsages.size() > 0)
                                                    setListaApps();
                                                if (!swGrafico)
                                                    actualizarDatosTarta();
                                                else
                                                    actualizarDatosLineal();

                                                lyAct.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }.start();

                                //imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();

            }
        });
        btnSemana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnSemana.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 2;

                lyAct.setVisibility(View.VISIBLE);
                //imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                final Handler handler3 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);

                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                new Thread(){
                                    @Override
                                    public void run() {
                                        super.run();
                                        appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                                        handler3.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (appUsages.size() > 0)
                                                    setListaApps();
                                                if (!swGrafico)
                                                    actualizarDatosTarta();
                                                else
                                                    actualizarDatosLineal();

                                                lyAct.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }.start();
                                //imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();

            }
        });
        btnMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnMes.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnMes.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 3;

                lyAct.setVisibility(View.VISIBLE);
                //imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                final Handler handler3 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);

                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                new Thread(){
                                    @Override
                                    public void run() {
                                        super.run();
                                        appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);
                                        handler3.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (appUsages.size() > 0)
                                                    setListaApps();
                                                if (!swGrafico)
                                                    actualizarDatosTarta();
                                                else
                                                    actualizarDatosLineal();

                                                lyAct.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }.start();
                                //imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();
            }
        });

        view.findViewById(R.id.imgDatos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long millis = 0L;
                Aplicacion app = new Aplicacion();
                app.cargarAplicacionDePreferencias(context);
                int desbloqueos = 0;


                switch (periodoEstadisticas ){
                    case 1:
                        millis = app.getEstadisticaUsoDia().getTiempoUso();
                        desbloqueos = app.getEstadisticaUsoDia().getDesbloqueos();break;
                    case 2:
                        millis = app.getEstadisticaUsoSemana().getTiempoUso();
                        desbloqueos = app.getEstadisticaUsoSemana().getDesbloqueos();break;
                    case 3:millis = app.getEstadisticaUsoMes().getTiempoUso();
                        desbloqueos = app.getEstadisticaUsoMes().getDesbloqueos();break;
                }

                String str = "Uso del terminal: " + TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                        (TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.MILLISECONDS.toHours(millis)*60) + "m" + "\nDesbloqueos: "
                        + desbloqueos;

                new SimpleTooltip.Builder(context)
                        .anchorView(view.findViewById(R.id.imgDatos))
                        .text(str)
                        .gravity(Gravity.BOTTOM)
                        .animated(true)
                        .transparentOverlay(false)
                        .build()
                        .show();
            }
        });

        btnDia.setBackground(null);
        btnSemana.setBackground(null);
        btnMes.setBackground(null);
        btnDia.setTextColor(context.getColor(R.color.blanco));
        btnSemana.setTextColor(context.getColor(R.color.blanco));
        btnMes.setTextColor(context.getColor(R.color.blanco));
        btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
        btnDia.setTextColor(context.getColor(R.color.backgroundStart));

        periodoEstadisticas = 1;

        lyAct.setVisibility(View.VISIBLE);
        //imgActualizar.setVisibility(View.GONE);
        final Handler handler2 = new Handler();
        final Handler handler3 = new Handler();
        new Thread() {
            @Override
            public void run() {
                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);

                handler2.post(new Runnable() {
                    @Override
                    public void run() {
                        if (appUsages.size() > 0)
                            setListaApps();
                                if (!swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();

                        new Thread(){
                            @Override
                            public void run() {
                                super.run();
                                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                                handler3.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                        if (!swGrafico)
                                            actualizarDatosTarta();
                                        else
                                            actualizarDatosLineal();

                                        lyAct.setVisibility(View.GONE);
                                    }
                                });
                            }
                        }.start();

                        //imgActualizar.setVisibility(View.VISIBLE);
                    }
                });
            }
        }.start();

        final SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swRefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                switch (periodoEstadisticas){
                    case 1:{
                        btnDia.setBackground(null);
                        btnSemana.setBackground(null);
                        btnMes.setBackground(null);
                        btnDia.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setTextColor(context.getColor(R.color.blanco));
                        btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                        btnDia.setTextColor(context.getColor(R.color.backgroundStart));

                        periodoEstadisticas = 1;

                        lyAct.setVisibility(View.VISIBLE);
                        //imgActualizar.setVisibility(View.GONE);
                        final Handler handler2 = new Handler();
                        final Handler handler3 = new Handler();
                        new Thread() {
                            @Override
                            public void run() {
                                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);

                                handler2.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                if (!swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();

                                        new Thread(){
                                            @Override
                                            public void run() {
                                                super.run();
                                                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                                                handler3.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (appUsages.size() > 0)
                                                            setListaApps();
                                                        if (!swGrafico)
                                                            actualizarDatosTarta();
                                                        else
                                                            actualizarDatosLineal();

                                                        lyAct.setVisibility(View.GONE);
                                                        swipeRefreshLayout.setRefreshing(false);
                                                    }
                                                });
                                            }
                                        }.start();

                                        //imgActualizar.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }.start();
                        break;
                    }

                    case 2:{
                        btnDia.setBackground(null);
                        btnSemana.setBackground(null);
                        btnMes.setBackground(null);
                        btnDia.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                        btnSemana.setTextColor(context.getColor(R.color.backgroundStart));

                        periodoEstadisticas = 2;

                        lyAct.setVisibility(View.VISIBLE);
                        //imgActualizar.setVisibility(View.GONE);
                        final Handler handler2 = new Handler();
                        final Handler handler3 = new Handler();
                        new Thread() {
                            @Override
                            public void run() {
                                appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);

                                handler2.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                        new Thread(){
                                            @Override
                                            public void run() {
                                                super.run();
                                                appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                                                handler3.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (appUsages.size() > 0)
                                                            setListaApps();
                                                        if (!swGrafico)
                                                            actualizarDatosTarta();
                                                        else
                                                            actualizarDatosLineal();

                                                        lyAct.setVisibility(View.GONE);
                                                        swipeRefreshLayout.setRefreshing(false);
                                                    }
                                                });
                                            }
                                        }.start();
                                        //imgActualizar.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }.start();
                        break;
                    }

                    case 3:{
                        btnDia.setBackground(null);
                        btnSemana.setBackground(null);
                        btnMes.setBackground(null);
                        btnDia.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                        btnMes.setTextColor(context.getColor(R.color.backgroundStart));

                        periodoEstadisticas = 3;

                        lyAct.setVisibility(View.VISIBLE);
                        //imgActualizar.setVisibility(View.GONE);
                        final Handler handler2 = new Handler();
                        final Handler handler3 = new Handler();
                        new Thread() {
                            @Override
                            public void run() {
                                appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);

                                handler2.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                        new Thread(){
                                            @Override
                                            public void run() {
                                                super.run();
                                                appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);
                                                handler3.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (appUsages.size() > 0)
                                                            setListaApps();
                                                        if (!swGrafico)
                                                            actualizarDatosTarta();
                                                        else
                                                            actualizarDatosLineal();

                                                        lyAct.setVisibility(View.GONE);
                                                        swipeRefreshLayout.setRefreshing(false);
                                                    }
                                                });
                                            }
                                        }.start();
                                        //imgActualizar.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }.start();
                        break;
                    }

                }
            }
        });

    }
    public void initRemoto(){

        mChart =  view.findViewById(R.id.chart1);
        mChart2 =  view.findViewById(R.id.chart2);
        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);

        while (ActivityPrincipalRemoto.estadisticasUsuarioRemoto == null){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        final List<EstadisticaAplicacion> estadisticaAplicacionDia =
                ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getEstadisticasDia();
        final List<EstadisticaAplicacion> estadisticaAplicacionSemana =
                ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getEstadisticasSemana();
        final List<EstadisticaAplicacion> estadisticaAplicacionMes =
                ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getEstadisticasMes();
        actualizarDatosTartaRemoto(estadisticaAplicacionDia);
        setListaAppsRemoto(estadisticaAplicacionDia);

        swGrafico = false;
        fab.setImageResource(R.drawable.analytics);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<EstadisticaAplicacion> estadisticas = estadisticaAplicacionDia;

                if (periodoEstadisticas == 2){
                    estadisticas = estadisticaAplicacionSemana;
                }else if (periodoEstadisticas == 3){
                    estadisticas = estadisticaAplicacionMes;
                }
                swGrafico=!swGrafico;
                if (swGrafico) {
                    actualizarDatosLinealRemoto(estadisticas);
                    fab.setImageResource(R.drawable.pie_chart);
                } else {
                    actualizarDatosTartaRemoto(estadisticas);
                    fab.setImageResource(R.drawable.analytics);
                }
            }
        });

        btnDia = view.findViewById(R.id.btnDia);
        btnSemana = view.findViewById(R.id.btnSemana);
        btnMes = view.findViewById(R.id.btnMes);

        btnDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnDia.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 1;

                if (!swGrafico) {
                    actualizarDatosTartaRemoto(estadisticaAplicacionDia);
                } else {
                    actualizarDatosLinealRemoto(estadisticaAplicacionDia);
                }


            }
        });

        btnSemana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnSemana.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 2;

                if (!swGrafico) {
                    actualizarDatosTartaRemoto(estadisticaAplicacionSemana);
                } else {
                    actualizarDatosLinealRemoto(estadisticaAplicacionSemana);
                }



            }
        });

        btnMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnMes.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnMes.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 3;

                if (!swGrafico) {
                    actualizarDatosTartaRemoto(estadisticaAplicacionMes);
                } else {
                    actualizarDatosLinealRemoto(estadisticaAplicacionMes);
                }


            }
        });

        view.findViewById(R.id.swRefresh).setEnabled(false);

        view.findViewById(R.id.imgDatos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long millis = 0L;
                Aplicacion app = new Aplicacion();
                app.cargarAplicacionDePreferencias(context);
                int desbloqueos = 0;


                switch (periodoEstadisticas){
                    case 1:
                        millis = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getTiempoUsoDia();
                        desbloqueos = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getDesbloqueosDia();break;
                    case 2:
                        millis = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getTiempoUsoSemana();
                        desbloqueos = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getDesbloqueosSemana();break;
                    case 3:millis = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getTiempoUsoMes();
                        desbloqueos = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getDesbloqueosMes();break;
                }

                String str = "Uso del terminal: " + TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                        (TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.MILLISECONDS.toHours(millis)*60) + "m" + "\nDesbloqueos: "
                        + desbloqueos;

                new SimpleTooltip.Builder(context)
                        .anchorView(view.findViewById(R.id.imgDatos))
                        .text(str)
                        .gravity(Gravity.BOTTOM)
                        .animated(true)
                        .transparentOverlay(false)
                        .build()
                        .show();
            }
        });
    }

    private void actualizarDatosTarta(){
        if (appUsages.size() <= 0)
            return;

        mChart.setVisibility(View.VISIBLE);
        mChart2.setVisibility(View.GONE);

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);
        mChart.setCenterTextColor(ContextCompat.getColor(context,R.color.azul_celdas));
        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setCenterText(generateCenterSpannableText("", null, -1));

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(context.getColor(R.color.backgroundEnd));

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(40f);
        mChart.setTransparentCircleRadius(10f);

        mChart.setDrawCenterText(false);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(false);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);

        mChart.getLegend().setEnabled(false);

        setDataPie(mParties.length, 100);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling
        mChart.setEntryLabelColor(ContextCompat.getColor(context,R.color.azul_celdas));
        mChart.setEntryLabelTextSize(16f);
    }
    private void actualizarDatosTartaRemoto(List<EstadisticaAplicacion> estadisticas){
        if (estadisticas.size() <= 0)
            return;


        mChart.setVisibility(View.VISIBLE);
        mChart2.setVisibility(View.GONE);

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);
        mChart.setCenterTextColor(ContextCompat.getColor(context,R.color.azul_celdas));
        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setCenterText(generateCenterSpannableText("", null, -1));

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(context.getColor(R.color.backgroundEnd));

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(40f);
        mChart.setTransparentCircleRadius(10f);

        mChart.setDrawCenterText(false);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(false);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);

        mChart.getLegend().setEnabled(false);

        setDataPieRemoto(estadisticas);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling
        mChart.setEntryLabelColor(ContextCompat.getColor(context,R.color.azul_celdas));
        mChart.setEntryLabelTextSize(16f);




        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);
    }

    private void actualizarDatosLineal(){
        mChart.setVisibility(View.GONE);
        mChart2.setVisibility(View.VISIBLE);

        if (appUsages.size() <= 0)
            return;



        mChart2.getDescription().setEnabled(false);
        mChart2.setExtraOffsets(5, 10, 5, 5);

        mChart2.setDragDecelerationFrictionCoef(0.95f);





        // enable rotation of the chart by touch
        mChart2.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart2.setOnChartValueSelectedListener(this);

        mChart2.getLegend().setEnabled(false);

        setDataLinear(mParties.length, 100);

        mChart2.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling

    }
    private void actualizarDatosLinealRemoto(List<EstadisticaAplicacion> estadisticas){
        mChart.setVisibility(View.GONE);
        mChart2.setVisibility(View.VISIBLE);

        if (estadisticas.size() <= 0)
            return;



        mChart2.getDescription().setEnabled(false);
        mChart2.setExtraOffsets(5, 10, 5, 5);

        mChart2.setDragDecelerationFrictionCoef(0.95f);





        // enable rotation of the chart by touch
        mChart2.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart2.setOnChartValueSelectedListener(this);

        mChart2.getLegend().setEnabled(false);

        setDataLinearRemoto(estadisticas);

        mChart2.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling

    }


    private void setDataPie(int count, float range) {

        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();



        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count ; i++) {
            PieEntry  entry =new PieEntry(fValues[i],
                    mParties[i],
                    drawables[i]);
            entries.add(entry);
        }
        PieDataSet dataSet = new PieDataSet(entries, "Election Results");

        dataSet.setDrawIcons(false);

        // Outside values
        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        dataSet.setValueLinePart1OffsetPercentage(50f); /** When valuePosition is OutsideSlice, indicates offset as percentage out of the slice size */
        dataSet.setValueLineColor(ContextCompat.getColor(context,R.color.azul_celdas));
        dataSet.setValueLineWidth(2.0f);
        dataSet.setValueLinePart1Length(0.5f); /** When valuePosition is OutsideSlice, indicates length of first half of the line */
        dataSet.setValueLinePart2Length(0.5f); /** When valuePosition is OutsideSlice, indicates length of second half of the line */
        mChart.setExtraOffsets(0.f, 5.f, 0.f, 5.f);
        mChart.getLegend().setTextColor(ContextCompat.getColor(context,R.color.azul_celdas));
        dataSet.setSliceSpace(8f);
        dataSet.setIconsOffset(new MPPointF(0, 0));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        clr.add(appUsages.get(0).getColor());
        if(appUsages.size()>1) {
            clr.add(appUsages.get(1).getColor());
        }
        else{
            clr.add(ResourcesCompat.getColor(context.getResources(), R.color.blanco,null));
        }
        if(appUsages.size()>2) {
            clr.add(appUsages.get(2).getColor());
        }
        else{
            clr.add(ContextCompat.getColor(context, R.color.blanco));
        }
    /*    if(appUsages.size()>3) {
            clr.add(appUsages.get(3).getColor());
        }
        else{
            clr.add(ContextCompat.getColor(context, R.color.blancod));
        }*/
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add(colors[colors.length-1]);

        dataSet.setColors(clr);
        //dataSet.setValueTextSize(20f);
        //dataSet.setSelectionShift(0f);


        PieData data = new PieData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16f);
        data.setValueTextColor(ContextCompat.getColor(context,R.color.blanco));
        data.setValueFormatter(new PercentFormatter());


        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }
    private void setDataPieRemoto(List<EstadisticaAplicacion> estadisticas) {


        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();



        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        for (int i = 0; i < estadisticas.size() ; i++) {
            if(i<3) {
                PieEntry entry = new PieEntry((float) estadisticas.get(i).getPorcentajeUso(),
                        estadisticas.get(i).getNombreApp());
                entries.add(entry);
            }
        }
        float total = 0;
        if(estadisticas.size()>3) {
            for (int i = 3; i < estadisticas.size(); ++i) {
                total += estadisticas.get(i).getPorcentajeUso();
            }
            PieEntry entry = new PieEntry(total, "Otras");
            entries.add(entry);
        }
        PieDataSet dataSet = new PieDataSet(entries, "Election Results");

        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        dataSet.setValueLinePart1OffsetPercentage(50f); /** When valuePosition is OutsideSlice, indicates offset as percentage out of the slice size */
        dataSet.setValueLineColor(ContextCompat.getColor(context,R.color.azul_celdas));
        dataSet.setValueLineWidth(2.0f);
        dataSet.setValueLinePart1Length(0.3f); /** When valuePosition is OutsideSlice, indicates length of first half of the line */
        dataSet.setValueLinePart2Length(0.7f); /** When valuePosition is OutsideSlice, indicates length of second half of the line */
        mChart.setExtraOffsets(0.f, 5.f, 0.f, 5.f);
        mChart.getLegend().setTextColor(ContextCompat.getColor(context,R.color.azul_celdas));
        dataSet.setSliceSpace(8f);
        dataSet.setIconsOffset(new MPPointF(0, 0));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        if(estadisticas.size()>0)
        clr.add(ContextCompat.getColor(context, R.color.blanco));
        if(estadisticas.size()>1)
            if(estadisticas.size()>1)
        clr.add(ContextCompat.getColor(context, R.color.blanco));
        if(estadisticas.size()>2)
        clr.add(ContextCompat.getColor(context, R.color.blanco));
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add((ContextCompat.getColor(context, R.color.blanco)));

        dataSet.setColors(clr);
        //dataSet.setValueTextSize(20f);
        //dataSet.setSelectionShift(0f);


        PieData data = new PieData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16f);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new PercentFormatter());

        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }

    private void setDataLinear(int count, float range) {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();


        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.


        for (int i = 0; i < count; ++i){
            char ellipsis = 133;
            if(mParties[i]!=null) {
                String lbl = mParties[i];
                if (lbl != null) {
                    if (lbl.length() > 9) {
                        lbl = lbl.substring(0, 8);
                        lbl = lbl.concat("...");
                    }
                    mParties[i] = lbl;
                }
            }
        }


        for (int i = 0; i < count ; i++) {
            if(mParties[i]!=null) {
                entries.add(new BarEntry(i, fValues[i],
                        mParties[i]));
            }
        }

        BarDataSet dataSet = new BarDataSet(entries, "Hola");

        //dataSet.setDrawIcons(false);

        dataSet.setIconsOffset(new MPPointF(0, 40));



        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        clr.add(appUsages.get(0).getColor());
        if(appUsages.size()>1) {
            clr.add(appUsages.get(1).getColor());
        }
        else{
            clr.add(ResourcesCompat.getColor(context.getResources(), R.color.blanco,null));
        }
        if(appUsages.size()>2) {
            clr.add(appUsages.get(2).getColor());
        }
        else{
            clr.add(ContextCompat.getColor(context, R.color.blanco));
        }
    /*    if(appUsages.size()>3) {
            clr.add(appUsages.get(3).getColor());
        }
        else{
            clr.add(ContextCompat.getColor(context, R.color.blancod));
        }*/
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add(colors[colors.length-1]);

        dataSet.setColors(clr);
        dataSet.setStackLabels(mParties);
        //dataSet.setSelectionShift(0f);


        BarData data = new BarData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new PercentFormatter());

        mChart2.setData(data);
        mChart2.setDrawValueAboveBar(true);





        mChart2.getXAxis().setValueFormatter(new IndexAxisValueFormatter(mParties));
        mChart2.getXAxis().setTextSize(11f);
        mChart2.getXAxis().setLabelCount(entries.size());
        mChart2.setDoubleTapToZoomEnabled(false);
        mChart2.setPinchZoom(false);
        mChart2.setScaleEnabled(false);



        // undo all highlights
        mChart2.highlightValues(null);

        mChart2.invalidate();
    }
    private void setDataLinearRemoto(List<EstadisticaAplicacion> estadisticas) {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();


        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        String nombres[] = new String[4];
        float values[] = new float[4];
        for (int i = 0; i < estadisticas.size(); ++i){
            char ellipsis = 133;
            String lbl = estadisticas.get(i).getNombreApp();
            if(lbl!=null) {
                if (lbl.length() > 9) {
                    lbl = lbl.substring(0, 8);
                    lbl = lbl.concat("...");
                }
                if(i<4) {
                    nombres[i] = lbl;

                    values[i] = (float) estadisticas.get(i).getPorcentajeUso();
                }
            }
        }
        nombres[3] = "Otras";
        values[3] = 0;
        for (int i = 3; i < estadisticas.size(); ++i){
            values[3]+= estadisticas.get(i).getPorcentajeUso();
        }



        for (int i = 0; i < 4 ; i++) {
            if(nombres[i]!=null)
            entries.add(new BarEntry(i, values[i],
                    nombres[i]));
        }

        BarDataSet dataSet = new BarDataSet(entries, "Hola");

        //dataSet.setDrawIcons(false);

        dataSet.setIconsOffset(new MPPointF(0, 40));



        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        if(estadisticas.size()>0)
        clr.add(ContextCompat.getColor(context, R.color.blanco));
        if(estadisticas.size()>1)
        clr.add(ContextCompat.getColor(context, R.color.blanco));
        if(estadisticas.size()>2)
        clr.add(ContextCompat.getColor(context, R.color.blanco));
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add(ContextCompat.getColor(context, R.color.blanco));

        dataSet.setColors(clr);
        dataSet.setStackLabels(nombres);
        //dataSet.setSelectionShift(0f);


        BarData data = new BarData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new PercentFormatter());

        mChart2.setData(data);
        mChart2.setDrawValueAboveBar(true);





        mChart2.getXAxis().setValueFormatter(new IndexAxisValueFormatter(nombres));
        mChart2.getXAxis().setTextSize(11f);
        mChart2.getXAxis().setLabelCount(entries.size());
        mChart2.setDoubleTapToZoomEnabled(false);
        mChart2.setPinchZoom(false);
        mChart2.setScaleEnabled(false);



        // undo all highlights
        mChart2.highlightValues(null);

        mChart2.invalidate();
    }




    private SpannableString generateCenterSpannableText(String app, Drawable d, float p) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);

        String strapp = app;
        float auxp = p;
        if (p == -14) {
            view.findViewById(R.id.scroll).scrollTo(0, (int) lyEstadisticasLista.getY());
        }else if (p != -1)
            strapp = strapp.concat("\n" + df.format(auxp) + "%");
        SpannableString ss = new SpannableString(strapp + "\n\n ");
        ss.setSpan(new RelativeSizeSpan(1.3f), 0, strapp.length(), 0);
        ss.setSpan(new StyleSpan(Typeface.BOLD), 0, strapp.length(), 0);
        ss.setSpan(new ForegroundColorSpan(context.getColor(R.color.azul_celdas)), 0, strapp.length(), 0);
        if (p!= -1 && p!= -14) {
            d.setBounds(0, 0, 200, 200);
            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
            ss.setSpan(span, strapp.length()+2, strapp.length()+3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        return ss;
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

        /*if (e == null)
            return;
        for (int i = 0; i < values.length; ++i){
            if (values[i] == e.getY()){
                if (i == values.length-1){
                    mChart.setCenterText(generateCenterSpannableText("", drawables[i], -14));
                }else{
                    mChart.setCenterText(generateCenterSpannableText(mParties[i], drawables[i], (float)values[i]*100/total));
                }
            }
        }*/
    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!ususarioRemoto)
            init();

        /*if (!firsttime){
            lyAct.setVisibility(View.VISIBLE);
            //imgActualizar.setVisibility(View.GONE);
            final Handler handler2 = new Handler();
            new Thread() {
                @Override
                public void run() {
                    if (periodoEstadisticas == 1)
                        appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                    else if (periodoEstadisticas == 2)
                        appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                    else if(periodoEstadisticas == 3)
                        appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);


                    handler2.post(new Runnable() {
                        @Override
                        public void run() {
                            if (appUsages.size() > 0)
                                setListaApps();
                            if (swGrafico)
                                actualizarDatosTarta();
                            else
                                actualizarDatosLineal();
                            lyAct.setVisibility(View.GONE);
                            //imgActualizar.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }.start();
        }else
            firsttime = false;*/
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void setListaApps(){
        List<AppUsage> appusg = new ArrayList<>(appUsages);
        lyEstadisticasLista = view.findViewById(R.id.lyEstadisticasLista);
        LayoutInflater inflater = LayoutInflater.from(context);

        lyEstadisticasLista.removeAllViews();

        int i = 0;

        int thisSize = 3;
        if(appUsages.size()>3){
            thisSize = 3;
        }
        else{
            thisSize = appUsages.size();
        }
        drawables = new Drawable[thisSize+1];
        mParties = new String[thisSize+1];
        colors = new int[thisSize+1];
        values = new int[thisSize+1];

        float fval[] =  new float[thisSize+1];
        fval[thisSize] = 0f;

        values[thisSize] = 0;
        mParties[thisSize] = "Otras";
        colors[thisSize] = ContextCompat.getColor(context, R.color.blanco);
        mTotal = 0L;
        for (AppUsage app : appusg){
            mTotal += app.getTotalTimeInForeground();
        }
        for (AppUsage app : appusg){
            View v = inflater.inflate(R.layout.row_estadisticas, null);
            ImageView imgLogo = v.findViewById(R.id.imgLogo);
            TextView txtAppName = v.findViewById(R.id.txtAppName);
            TextView txtUsage = v.findViewById(R.id.txtUsage);
            TextView txtTime = v.findViewById(R.id.timeText);

            String applicationName;
            applicationName = app.getAppName();

            txtAppName.setText(applicationName);
            float value = app.getTotalTimeInForeground()*100 / mTotal;
            String usage;

            if (value == 0){
                usage = " <1 %";
            }else{
                usage = String.valueOf(value) + " %";
            }
            SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
            String dateString = formatter.format(new Date(app.getTotalTimeInForeground()));
            txtUsage.setText(dateString + " min");
            SimpleDateFormat formatterb = new SimpleDateFormat("HH:mm");
            dateString = formatterb.format(new Date(app.getLastTimeUsed()));
            txtTime.setText(dateString);
            try {
                Drawable icon = context.getPackageManager().getApplicationIcon(app.getPackage_name());
                imgLogo.setImageDrawable(icon);
                if (i < thisSize){
                    drawables[i] = icon;
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (i > thisSize-1) {

                values[thisSize] += value;
                fval[thisSize] += app.getTotalTimeInForeground();
            }else{
                values[i] = (int) value;
                mParties[i] = applicationName;
                fval[i] = (float)app.getTotalTimeInForeground()*100f / mTotal;
            }

            lyEstadisticasLista.addView(v);


            for(int j = 0; j<values.length; j++){
                total = total + values[j];
            }
            //total = values[0] + values[1] + values[2] + values[3];// + values[4];

            i++;
        }

        fval[thisSize] = fval[thisSize]*100/mTotal;




        fValues = new float[thisSize+1];
        System.arraycopy(fval, 0, fValues, 0, thisSize + 1);
    }
    private void setListaAppsRemoto(List<EstadisticaAplicacion> estadisticas){
        lyEstadisticasLista = view.findViewById(R.id.lyEstadisticasLista);
        LayoutInflater inflater = LayoutInflater.from(context);

        Collections.sort(estadisticas, new Comparator<EstadisticaAplicacion>(){
            public int compare(EstadisticaAplicacion obj1, EstadisticaAplicacion obj2) {
                // ## Ascending order
                return Double.valueOf(obj2.getLastTimeUsage()).compareTo(Double.valueOf(obj1.getLastTimeUsage())); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            }
        });

        lyEstadisticasLista.removeAllViews();

        for (int i = 0; i < estadisticas.size(); ++i){
            View v = inflater.inflate(R.layout.row_estadisticas, null);

            TextView txtAppName = v.findViewById(R.id.txtAppName);
            TextView txtUsage = v.findViewById(R.id.txtUsage);
            TextView txtTime = v.findViewById(R.id.timeText);
            txtAppName.setText(estadisticas.get(i).getNombreApp());

            float value = (float) estadisticas.get(i).getPorcentajeUso();
            String usage;
            if (value < 1){
                usage = " <1 %";
            }else{
                usage = String.valueOf(value) + " %";
            }
            SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
            String dateString = formatter.format(new Date((long)estadisticas.get(i).getTotalUsage()));
            txtUsage.setText(dateString);
            SimpleDateFormat formatterb = new SimpleDateFormat("HH:mm");
            String dateStringa = formatterb.format(new Date((long)estadisticas.get(i).getLastTimeUsage()));
            txtTime.setText(dateStringa);

            if(!dateString.equals("00:00"))
            lyEstadisticasLista.addView(v);
        }

        lyEstadisticasLista.invalidate();
    }



}
