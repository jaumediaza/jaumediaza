package com.atendus.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.clases.DesconexionProgramada;
import com.atendus.app.clases.Telefono;
import com.atendus.app.json.JSONBorrarDesconexionProgramada;
import com.atendus.app.json.JSONBorrarDesconexionProgramadaRemota;
import com.atendus.app.json.JSONEnviarDesconexionProgramada;
import com.atendus.app.json.JSONEnviarDesconexionProgramadaModificar;
import com.atendus.app.json.JSONEnviarDesconexionProgramadaRemota;
import com.atendus.app.json.JSONEnviarDesconexionProgramadaRemotaModificar;
import com.atendus.app.remoto.ActivityPrincipalRemoto;
import com.atendus.app.remoto.ActivityTelefonosDesconectaRemoto;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import spencerstudios.com.bungeelib.Bungee;


public class ActivityProgramarDesconexion extends AppCompatActivity {
    private int horaInicio = 0;
    private int horaTermina = 0;
    private int minutoInicio = 0;
    private int minutoTermina = 0;
    private boolean bloquearNotificaciones = true;
    private boolean bloquearLlamadas = true;
    private String strTitulo;
    private boolean editar = false;
    private int id = 0;
    DesconexionProgramada descd;
    Handler phandler;

    private boolean[] dias = new boolean[7];
    private LinearLayout lyLunes, lyMartes, lyMiercoles, lyJueves, lyViernes, lySabado, lyDomingo;
    private TextView txtLunes, txtMartes, txtMiercoles, txtJueves, txtViernes, txtSabado, txtDomingo;

    private Spinner spn_repetir;

    public static List<AplicacionPermitida> aplicacionPermitidas = new ArrayList<>();
    public static List<Telefono> telefonosPermitidos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programar_desconexion);

        aplicacionPermitidas = new ArrayList<>();
        telefonosPermitidos = new ArrayList<>();


        findViewById(R.id.arr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.btnAppsPermitidas).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityProgramarDesconexion.this, ActivityAppsDesconecta.class);
                intent.putExtra("fromProgramarDesconexion", true);
                startActivity(intent);
            }
        });

        findViewById(R.id.btnTfnosPermitidas).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PhoneStateManager.getInstance().isDesconectado()) {
                    mostrarDialogoSeleccionarContactosPermitidos();
                }
            }
        });

        strTitulo = getString(R.string.titulo);

        findViewById(R.id.ly_empieza).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView h1Empieza = findViewById(R.id.h1_empieza);
                final TextView h2Empieza = findViewById(R.id.h2_empieza);
                final TextView m1Empieza = findViewById(R.id.m1_empieza);
                final TextView m2Empieza = findViewById(R.id.m2_empieza);

                final Calendar myCalender = Calendar.getInstance();

                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);

                            horaInicio = myCalender.get(Calendar.HOUR_OF_DAY);
                            minutoInicio = myCalender.get(Calendar.MINUTE);

                            String str;
                            int aux = horaInicio / 10;
                            str = String.valueOf(aux);
                            h1Empieza.setText(str);
                            aux = horaInicio - aux * 10;
                            str = String.valueOf(aux);
                            h2Empieza.setText(str);


                            aux = minutoInicio / 10;
                            str = String.valueOf(aux);
                            m1Empieza.setText(str);
                            aux = minutoInicio - aux * 10;
                            str = String.valueOf(aux);
                            m2Empieza.setText(str);


                            calcularDuracion();

                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        ActivityProgramarDesconexion.this,
                        android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener,
                        horaInicio, minutoInicio, true);
                timePickerDialog.setTitle(getString(R.string.modificar_tiempo_desconexion));
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });


        findViewById(R.id.ly_termina).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final TextView h1Termina = findViewById(R.id.h1_termina);
                final TextView h2Termina = findViewById(R.id.h2_termina);
                final TextView m1Termina = findViewById(R.id.m1_termina);
                final TextView m2Termina = findViewById(R.id.m2_termina);

                final Calendar myCalender = Calendar.getInstance();

                TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (view.isShown()) {
                            myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            myCalender.set(Calendar.MINUTE, minute);

                            horaTermina = myCalender.get(Calendar.HOUR_OF_DAY);
                            minutoTermina = myCalender.get(Calendar.MINUTE);

                            String str;
                            int aux = horaTermina / 10;
                            str = String.valueOf(aux);
                            h1Termina.setText(str);
                            aux = horaTermina - aux * 10;
                            str = String.valueOf(aux);
                            h2Termina.setText(str);


                            aux = minutoTermina / 10;
                            str = String.valueOf(aux);
                            m1Termina.setText(str);
                            aux = minutoTermina - aux * 10;
                            str = String.valueOf(aux);
                            m2Termina.setText(str);

                            calcularDuracion();

                        }
                    }
                };
                TimePickerDialog timePickerDialog = new TimePickerDialog(
                        ActivityProgramarDesconexion.this,
                        android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener,
                        horaTermina, minutoTermina, true);
                timePickerDialog.setTitle(getString(R.string.modificar_tiempo_desconexion));
                timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                timePickerDialog.show();
            }
        });

        final Switch notif = findViewById(R.id.sw_notificaciones);
        notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bloquearNotificaciones) {
                    notif.setChecked(false);
                    bloquearNotificaciones = false;
                    findViewById(R.id.btnAppsPermitidas).setVisibility(View.GONE);
                } else {
                    notif.setChecked(true);
                    bloquearNotificaciones = true;
                    findViewById(R.id.btnAppsPermitidas).setVisibility(View.VISIBLE);
                }

            }
        });

        final Switch llamad = findViewById(R.id.sw_llamadas);
        llamad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bloquearLlamadas) {
                    llamad.setChecked(false);
                    bloquearLlamadas = false;
                    findViewById(R.id.btnTfnosPermitidas).setVisibility(View.GONE);
                } else {
                    llamad.setChecked(true);
                    bloquearLlamadas = true;
                    findViewById(R.id.btnTfnosPermitidas).setVisibility(View.VISIBLE);
                }

            }
        });

        findViewById(R.id.ly_add_desc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarDesconexion();
            }
        });


        setBarraDias();

        spn_repetir = findViewById(R.id.spn_repetir);

        List<String> list = new ArrayList<>();
        list.add(getString(R.string.repetir_no));
        list.add(getString(R.string.repetir_si));


        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spn_repetir.setAdapter(adapter);


        if (getIntent().getBooleanExtra("Edit", false)) {
            editarProgramarDesconexion();
        }
        if (getIntent().getBooleanExtra("Delete", false)) {
            if (getIntent().getBooleanExtra("remoto", false)) {
                borrarProgramarDesconexionRemota();
            } else {
                new AlertDialog.Builder(this).setTitle(
                        getString(R.string.confirmar_borrar_desconexion)).setPositiveButton(
                        getString(R.string.borrar), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final Aplicacion app = new Aplicacion();
                                app.cargarAplicacionDePreferencias(ActivityProgramarDesconexion.this);
                                int pos = 0;
                                for (int c = 0; c < app.getDesconexionesProgramadas().getList().size(); ++c) {
                                    if (app.getDesconexionesProgramadas().getList().get(c).getId() == id) {
                                        pos = c;
                                        break;
                                    }
                                }
                                final int fpos = pos;
                                final Handler handler = new Handler();
                                if (TresAndroides.isOnline(ActivityProgramarDesconexion.this)) {
                                    findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                                    findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
                                    new Thread() {
                                        @Override
                                        public void run() {
                                            super.run();

                                            Aplicacion app = new Aplicacion();
                                            app.cargarAplicacionDePreferencias(ActivityProgramarDesconexion.this);

                                            //TODO: Te recomiendo no seguir utilizando este metodo para obtener el ID del dispositivo, mejor ocupa
                                            // SECURE_ANDROID_ID, evitemos problemas con los de Google por este tipo de situaciones
                                            final String iddisp = app.getUsuario().getNombrehijo();
                                            int idusu = app.getUsuario().getId();
                                            JSONBorrarDesconexionProgramada jsonBorrarDesconexionProgramada =
                                                    new JSONBorrarDesconexionProgramada(ActivityProgramarDesconexion.this);
                                            try {
                                                jsonBorrarDesconexionProgramada.run(idusu, iddisp, app.getDesconexionesProgramadas().getList().get(fpos).getId());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Aplicacion app = new Aplicacion();
                                                    app.cargarAplicacionDePreferencias(ActivityProgramarDesconexion.this);
                                                    app.getDesconexionesProgramadas().getList().remove(fpos);
                                                    app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                                                    findViewById(R.id.pgBar).setVisibility(View.GONE);
                                                    findViewById(R.id.lyDis).setVisibility(View.GONE);
                                                    finish();
                                                }
                                            });

                                        }
                                    }.start();
                                } else {
                            /*new AlertDialog.Builder(ActivityProgramarDesconexion.this).setTitle(
                                    getString(R.string.error_no_internet)).setPositiveButton(
                                    getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                        }
                                    }).show();*/
                                    app.getColaEnvios().addBorrarDesconexion(app.getDesconexionesProgramadas().getList().get(id));
                                    app.getDesconexionesProgramadas().getList().remove(id);
                                    app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                                    findViewById(R.id.pgBar).setVisibility(View.GONE);
                                    findViewById(R.id.lyDis).setVisibility(View.GONE);
                                    finish();
                                }
                            }
                        }).setNegativeButton(getString(R.string.cancelar), null).show();
            }
        }
    }

    private void mostrarDialogoSeleccionarContactosPermitidos() {
        Intent i = new Intent(this, ActivityTelefonosDesconectaRemoto.class);
        Bungee.fade(this);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void calcularDuracion() {

        TextView txtDuracion = findViewById(R.id.txt_duracion);
        String strDuracion = "";

        int horas = horaTermina - horaInicio;
        if (horas < 0) {
            horas = 24 + horas;
        }
        int minutos = minutoTermina - minutoInicio;

        if (minutos < 0) {
            horas--;
            minutos = 60 + minutos;
        }
        if (horas >= 1) {
            if (horas != 1)
                strDuracion = horas + " " + getString(R.string.horas);
            else
                strDuracion = horas + " " + getString(R.string.hora);

            if (minutos != 1)
                strDuracion = strDuracion.concat(" " + minutos + " " + getString(R.string.minutos));
            else
                strDuracion = strDuracion.concat(" " + minutos + " " + getString(R.string.minuto));

        } else {
            if (minutos > 1)
                strDuracion = minutos + " " + getString(R.string.minutos);
            else
                strDuracion = minutos + " " + getString(R.string.minuto);
        }
        if (minutos == 0 && horas == 0) {
            strDuracion = ">1 " + getString(R.string.minuto);
        }
        txtDuracion.setText(strDuracion);
    }

    private void guardarDesconexion() {
        if (horaInicio == horaTermina && minutoInicio == minutoTermina) {
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.horario_invalido))
                    .setPositiveButton(getString(R.string.aceptar), null)
                    .show();
            return;
        }

        strTitulo = ((EditText) findViewById(R.id.edtxt_titulo)).getText().toString();

        if (strTitulo.equals(getString(R.string.titulo)) || strTitulo.length() <= 0) {
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.titulo_invalido))
                    .setPositiveButton(getString(R.string.aceptar), null)
                    .show();
            return;
        } else if (strTitulo.trim().length() == 0) {
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.titulo_invalido))
                    .setPositiveButton(getString(R.string.aceptar), null)
                    .show();
            return;
        }

        final DesconexionProgramada desc = new DesconexionProgramada();
        desc.setHoraInicio(horaInicio);
        desc.setHoraFin(horaTermina);
        desc.setMinutoInicio(minutoInicio);
        desc.setMinutoFin(minutoTermina);

        String pckgPermitidas = "";

        for (AplicacionPermitida app : ActivityPrincipalRemoto.appsPermitidas) {
            if (app.isUsoDuranteDesconecta() && app.isPermitida()) {
                pckgPermitidas = pckgPermitidas.concat(app.getPackageName() + "/");
            }
        }

        desc.setAplicacionesPermitidas(pckgPermitidas);

        String numPermitidos = "";
        for (Telefono tfno : telefonosPermitidos) {
            numPermitidos = numPermitidos.concat(tfno.getNumero() + "/");
        }

        desc.setTfnosPermitidos(numPermitidos);


        if (!bloquearNotificaciones && !bloquearLlamadas) {
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.error_seleccionar_tipo_desconexion))
                    .setPositiveButton(getString(R.string.aceptar), null)
                    .show();
            return;
        }

        desc.setBloquearLlamadas(bloquearLlamadas);
        desc.setBloquearNotificaciones(bloquearNotificaciones);
        desc.setStrTitulo(strTitulo);
        if (!dias[0] && !dias[1] && !dias[2] && !dias[3] && !dias[4] && !dias[5] && !dias[6]) {
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.error_seleccionar_almenos_dia))
                    .setPositiveButton(getString(R.string.aceptar), null)
                    .show();
            return;
        }
        desc.setDias(dias);
        if (spn_repetir.getSelectedItem().toString().equals(getString(R.string.repetir_no))) {
            desc.setRepetir(1);
        } else
            desc.setRepetir(2);

        final Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);



        //TODO: Te recomiendo no seguir utilizando este metodo para obtener el ID del dispositivo, mejor ocupa
        // SECURE_ANDROID_ID, evitemos problemas con los de Google por este tipo de situaciones
        final String iddisp = app.getUsuario().getEmail();
        desc.setEditable(true);

        if (getIntent().getBooleanExtra("remoto", false)) {
            if (!editar) {
                mandarDesconexionProgramadaRemota(desc);
            } else {
                mandarDesconexionProgramadaRemotaEditar(desc);

            }
        } else if (!editar) {
            phandler = new Handler();
            findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
            findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
            if (TresAndroides.isOnline(this))
                new Thread() {
                    @Override
                    public void run() {
                        super.run();

                        JSONEnviarDesconexionProgramada jsonEnviarDesconexionProgramada =
                                new JSONEnviarDesconexionProgramada(ActivityProgramarDesconexion.this);
                        int res = -1;
                        try {
                            res = jsonEnviarDesconexionProgramada.run(app.getUsuario().getId(), iddisp, desc);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        final int fres = res;

                        phandler.post(new Runnable() {
                            @Override
                            public void run() {
                                desc.setId(fres);
                                app.getDesconexionesProgramadas().getList().add(desc);
                                app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                                findViewById(R.id.pgBar).setVisibility(View.GONE);
                                findViewById(R.id.lyDis).setVisibility(View.GONE);
                                finish();
                            }
                        });
                    }
                }.start();
            else {
                app.getColaEnvios().addNuevaDesconexion(desc);
                app.getDesconexionesProgramadas().getList().add(desc);
                app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                findViewById(R.id.pgBar).setVisibility(View.GONE);
                findViewById(R.id.lyDis).setVisibility(View.GONE);
                finish();
            }

        } else {
            final Handler handler = new Handler();
            findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
            findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
            if (TresAndroides.isOnline(this))
                new Thread() {
                    @Override
                    public void run() {
                        super.run();

                        Collections.reverse(app.getDesconexionesProgramadas().getList());
                        desc.setId(id);

                        JSONEnviarDesconexionProgramadaModificar jsonEnviarDesconexionProgramada =
                                new JSONEnviarDesconexionProgramadaModificar(ActivityProgramarDesconexion.this);
                        int res = -1;
                        try {
                            res = jsonEnviarDesconexionProgramada.run(app.getUsuario().getId(), iddisp, desc);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        final int fres = res;

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                int pos = 0;
                                for (int i = 0; i < app.getDesconexionesProgramadas().getList().size(); ++i) {
                                    if (app.getDesconexionesProgramadas().getList().get(i).getId() == id) {
                                        pos = i;
                                    }
                                }
                                app.getDesconexionesProgramadas().getList().set(pos, desc);
                                Collections.reverse(app.getDesconexionesProgramadas().getList());
                                app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                                findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                                findViewById(R.id.lyDis).setVisibility(View.GONE);
                                finish();
                            }
                        });
                    }
                }.start();
            else {
                desc.setId(app.getDesconexionesProgramadas().getList().get(id).getId());
                app.getColaEnvios().addModificarDesconexion(desc);
                app.getDesconexionesProgramadas().getList().set(id, desc);
                app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                findViewById(R.id.pgBar).setVisibility(View.GONE);
                findViewById(R.id.lyDis).setVisibility(View.GONE);
                finish();
            }
        }
    }

    private void editarProgramarDesconexion() {
        Intent intent = getIntent();
        editar = true;
        id = intent.getIntExtra("id", 0);
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        descd = new DesconexionProgramada();
        if (getIntent().getBooleanExtra("remoto", false)) {
            for (DesconexionProgramada d : ActivityPrincipalRemoto.desconexionesProgramadasRemoto
                    .getList()) {
                if (d.getId() == id) {
                    descd = d;
                    break;
                }
            }
        } else {
            for (DesconexionProgramada d : app.getDesconexionesProgramadas().getList()) {
                if (d.getId() == id) {
                    descd = d;
                    break;
                }
            }
        }


        findViewById(R.id.delete).setVisibility(View.VISIBLE);
        findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(ActivityProgramarDesconexion.this).setTitle(
                        getString(R.string.confirmar_borrar_desconexion)).setPositiveButton(
                        getString(R.string.borrar), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final Aplicacion app = new Aplicacion();
                                app.cargarAplicacionDePreferencias(ActivityProgramarDesconexion.this);
                                int pos = 0;
                                for (int c = 0; c < app.getDesconexionesProgramadas().getList().size(); ++c) {
                                    if (app.getDesconexionesProgramadas().getList().get(c).getId() == id) {
                                        pos = c;
                                        break;
                                    }
                                }
                                final int fpos = pos;
                                final Handler handler = new Handler();
                                if (TresAndroides.isOnline(ActivityProgramarDesconexion.this)) {
                                    findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                                    findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
                                    new Thread() {
                                        @Override
                                        public void run() {
                                            super.run();


                                            //TODO: Te recomiendo no seguir utilizando este metodo para obtener el ID del dispositivo, mejor ocupa
                                            // SECURE_ANDROID_ID, evitemos problemas con los de Google por este tipo de situaciones
                                            final String iddisp = app.getUsuario().getNombrehijo();
                                            int idusu = app.getUsuario().getId();
                                            JSONBorrarDesconexionProgramada jsonBorrarDesconexionProgramada =
                                                    new JSONBorrarDesconexionProgramada(ActivityProgramarDesconexion.this);
                                            try {
                                                jsonBorrarDesconexionProgramada.run(idusu, iddisp, app.getDesconexionesProgramadas().getList().get(fpos).getId());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    app.getDesconexionesProgramadas().getList().remove(fpos);
                                                    app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                                                    findViewById(R.id.pgBar).setVisibility(View.GONE);
                                                    findViewById(R.id.lyDis).setVisibility(View.GONE);
                                                    finish();
                                                }
                                            });

                                        }
                                    }.start();
                                } else {
                                    app.getColaEnvios().addBorrarDesconexion(app.getDesconexionesProgramadas().getList().get(id));
                                    app.getDesconexionesProgramadas().getList().remove(id);
                                    app.guardarEnPreferencias(ActivityProgramarDesconexion.this);
                                    findViewById(R.id.pgBar).setVisibility(View.GONE);
                                    findViewById(R.id.lyDis).setVisibility(View.GONE);
                                    finish();
                                }
                            }
                        }).setNegativeButton(getString(R.string.cancelar), null).show();
            }
        });

        strTitulo = descd.getStrTitulo();
        ((EditText) findViewById(R.id.edtxt_titulo)).setText(strTitulo);

        horaInicio = descd.getHoraInicio();
        horaTermina = descd.getHoraFin();
        minutoInicio = descd.getMinutoInicio();
        minutoTermina = descd.getMinutoFin();
        minutoTermina = descd.getMinutoFin();

        final TextView h1Empieza = findViewById(R.id.h1_empieza);
        final TextView h2Empieza = findViewById(R.id.h2_empieza);
        final TextView m1Empieza = findViewById(R.id.m1_empieza);
        final TextView m2Empieza = findViewById(R.id.m2_empieza);

        final TextView h1Termina = findViewById(R.id.h1_termina);
        final TextView h2Termina = findViewById(R.id.h2_termina);
        final TextView m1Termina = findViewById(R.id.m1_termina);
        final TextView m2Termina = findViewById(R.id.m2_termina);

        //aplicacionPermitidas.clear();
        if (descd.getAplicacionesPermitidas() != null) {
            String[] pckg = descd.getAplicacionesPermitidas().split("/");

            for (String s : pckg) {
                for (AplicacionPermitida apps : PhoneStateManager.getInstance().getAppsPermitidas()) {
                    if (apps.getPackageName().equals(s)) {
                        apps.setPermitida(true);
                        aplicacionPermitidas.add(apps);
                    }
                }
            }
        }

        telefonosPermitidos.clear();
        if (descd.getTfnosPermitidos() != null) {
            if (descd.getTfnosPermitidos().length() > 0) {
                String[] numbers = descd.getTfnosPermitidos().split("/");
                for (String num : numbers) {
                    for (Telefono phone : PhoneStateManager.getInstance().getTfnosPermitidos()) {
                        if (phone.getNumero().contains(num.trim())) {
                            phone.setPermitido(true);
                            telefonosPermitidos.add(phone);
                        }
                    }
                }
            }
        }


        String str;
        int aux = horaInicio / 10;
        str = String.valueOf(aux);
        h1Empieza.setText(str);
        aux = horaInicio - aux * 10;
        str = String.valueOf(aux);
        h2Empieza.setText(str);


        aux = minutoInicio / 10;
        str = String.valueOf(aux);
        m1Empieza.setText(str);
        aux = minutoInicio - aux * 10;
        str = String.valueOf(aux);
        m2Empieza.setText(str);


        aux = horaTermina / 10;
        str = String.valueOf(aux);
        h1Termina.setText(str);
        aux = horaTermina - aux * 10;
        str = String.valueOf(aux);
        h2Termina.setText(str);


        aux = minutoTermina / 10;
        str = String.valueOf(aux);
        m1Termina.setText(str);
        aux = minutoTermina - aux * 10;
        str = String.valueOf(aux);
        m2Termina.setText(str);

        calcularDuracion();

        bloquearLlamadas = descd.isBloquearLlamadas();
        bloquearNotificaciones = descd.isBloquearNotificaciones();

        ((Switch) findViewById(R.id.sw_llamadas)).setChecked(bloquearLlamadas);
        ((Switch) findViewById(R.id.sw_notificaciones)).setChecked(bloquearNotificaciones);

        dias = descd.getDias();

        if (!dias[0]) {
            lyLunes.setBackground(null);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtLunes.setTypeface(type, Typeface.NORMAL);
            txtLunes.setTextColor(getColor(R.color.blanco));
        } else {
            lyLunes.setBackground(getDrawable(R.drawable.circle));
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtLunes.setTypeface(type, Typeface.BOLD);
            txtLunes.setTextColor(getColor(R.color.blanco));

        }
        if (!dias[1]) {
            lyMartes.setBackground(null);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtMartes.setTypeface(type, Typeface.NORMAL);
            txtMartes.setTextColor(getColor(R.color.blanco));
        } else {
            lyMartes.setBackground(getDrawable(R.drawable.circle));
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtMartes.setTypeface(type, Typeface.BOLD);
            txtMartes.setTextColor(getColor(R.color.blanco));

        }
        if (!dias[2]) {
            lyMiercoles.setBackground(null);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtMiercoles.setTypeface(type, Typeface.NORMAL);
            txtMiercoles.setTextColor(getColor(R.color.blanco));
        } else {
            lyMiercoles.setBackground(getDrawable(R.drawable.circle));
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtMiercoles.setTypeface(type, Typeface.BOLD);
            txtMiercoles.setTextColor(getColor(R.color.blanco));

        }
        if (!dias[3]) {
            lyJueves.setBackground(null);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtJueves.setTypeface(type, Typeface.NORMAL);
            txtJueves.setTextColor(getColor(R.color.blanco));
        } else {
            lyJueves.setBackground(getDrawable(R.drawable.circle));
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtJueves.setTypeface(type, Typeface.BOLD);
            txtJueves.setTextColor(getColor(R.color.blanco));

        }
        if (!dias[4]) {
            lyViernes.setBackground(null);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtViernes.setTypeface(type, Typeface.NORMAL);
            txtViernes.setTextColor(getColor(R.color.blanco));
        } else {
            lyViernes.setBackground(getDrawable(R.drawable.circle));
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtViernes.setTypeface(type, Typeface.BOLD);
            txtViernes.setTextColor(getColor(R.color.blanco));

        }
        if (!dias[5]) {
            lySabado.setBackground(null);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtSabado.setTypeface(type, Typeface.NORMAL);
            txtSabado.setTextColor(getColor(R.color.blanco));
        } else {
            lySabado.setBackground(getDrawable(R.drawable.circle));
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtSabado.setTypeface(type, Typeface.BOLD);
            txtSabado.setTextColor(getColor(R.color.blanco));

        }
        if (!dias[6]) {
            lyDomingo.setBackground(null);
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtDomingo.setTypeface(type, Typeface.NORMAL);
            txtDomingo.setTextColor(getColor(R.color.blanco));
        } else {
            lyDomingo.setBackground(getDrawable(R.drawable.circle));
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
            txtDomingo.setTypeface(type, Typeface.BOLD);
            txtDomingo.setTextColor(getColor(R.color.blanco));

        }

        if (descd.getRepetir() == 1) {
            spn_repetir.setSelection(0);
        } else {
            spn_repetir.setSelection(1);
        }


    }

    private void setBarraDias() {
        dias[0] = false;
        dias[1] = false;
        dias[2] = false;
        dias[3] = false;
        dias[4] = false;
        dias[5] = false;
        dias[6] = false;

        lyLunes = findViewById(R.id.lyLunes);
        lyMartes = findViewById(R.id.lyMartes);
        lyMiercoles = findViewById(R.id.lyMiercoles);
        lyJueves = findViewById(R.id.lyJueves);
        lyViernes = findViewById(R.id.lyViernes);
        lySabado = findViewById(R.id.lySabado);
        lyDomingo = findViewById(R.id.lyDomingo);

        txtLunes = findViewById(R.id.txtLunes);
        txtMartes = findViewById(R.id.txtMartes);
        txtMiercoles = findViewById(R.id.txtMiercoles);
        txtJueves = findViewById(R.id.txtJueves);
        txtViernes = findViewById(R.id.txtViernes);
        txtSabado = findViewById(R.id.txtSabado);
        txtDomingo = findViewById(R.id.txtDomingo);

        lyLunes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dias[0]) {
                    lyLunes.setBackground(null);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtLunes.setTypeface(type, Typeface.NORMAL);
                    txtLunes.setTextColor(getColor(R.color.blanco));
                    dias[0] = false;
                } else {
                    lyLunes.setBackground(getDrawable(R.drawable.circle));
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtLunes.setTypeface(type, Typeface.BOLD);
                    txtLunes.setTextColor(getColor(R.color.blanco));
                    dias[0] = true;

                }
            }
        });


        lyMartes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dias[1]) {
                    lyMartes.setBackground(null);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtMartes.setTypeface(type, Typeface.NORMAL);
                    txtMartes.setTextColor(getColor(R.color.blanco));
                    dias[1] = false;
                } else {
                    lyMartes.setBackground(getDrawable(R.drawable.circle));
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtMartes.setTypeface(type, Typeface.BOLD);
                    txtMartes.setTextColor(getColor(R.color.blanco));
                    dias[1] = true;

                }
            }
        });

        lyMiercoles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dias[2]) {
                    lyMiercoles.setBackground(null);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtMiercoles.setTypeface(type, Typeface.NORMAL);
                    txtMiercoles.setTextColor(getColor(R.color.blanco));
                    dias[2] = false;
                } else {
                    lyMiercoles.setBackground(getDrawable(R.drawable.circle));
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtMiercoles.setTypeface(type, Typeface.BOLD);
                    txtMiercoles.setTextColor(getColor(R.color.blanco));
                    dias[2] = true;

                }
            }
        });

        lyJueves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dias[3]) {
                    lyJueves.setBackground(null);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtJueves.setTypeface(type, Typeface.NORMAL);
                    txtJueves.setTextColor(getColor(R.color.blanco));
                    dias[3] = false;
                } else {
                    lyJueves.setBackground(getDrawable(R.drawable.circle));
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtJueves.setTypeface(type, Typeface.BOLD);
                    txtJueves.setTextColor(getColor(R.color.blanco));
                    dias[3] = true;

                }
            }
        });


        lyViernes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dias[4]) {
                    lyViernes.setBackground(null);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtViernes.setTypeface(type, Typeface.NORMAL);
                    txtViernes.setTextColor(getColor(R.color.blanco));
                    dias[4] = false;
                } else {
                    lyViernes.setBackground(getDrawable(R.drawable.circle));
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtViernes.setTypeface(type, Typeface.BOLD);
                    txtViernes.setTextColor(getColor(R.color.blanco));
                    dias[4] = true;

                }
            }
        });

        lySabado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dias[5]) {
                    lySabado.setBackground(null);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtSabado.setTypeface(type, Typeface.NORMAL);
                    txtSabado.setTextColor(getColor(R.color.blanco));
                    dias[5] = false;
                } else {
                    lySabado.setBackground(getDrawable(R.drawable.circle));
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtSabado.setTypeface(type, Typeface.BOLD);
                    txtSabado.setTextColor(getColor(R.color.blanco));
                    dias[5] = true;

                }
            }
        });

        lyDomingo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dias[6]) {
                    lyDomingo.setBackground(null);
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtDomingo.setTypeface(type, Typeface.NORMAL);
                    txtDomingo.setTextColor(getColor(R.color.blanco));
                    dias[6] = false;
                } else {
                    lyDomingo.setBackground(getDrawable(R.drawable.circle));
                    Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                    txtDomingo.setTypeface(type, Typeface.BOLD);
                    txtDomingo.setTextColor(getColor(R.color.blanco));
                    dias[6] = true;

                }
            }
        });


    }

    private void borrarProgramarDesconexionRemota() {
        new AlertDialog.Builder(this).setTitle(
                getString(R.string.confirmar_borrar_desconexion)).setPositiveButton(
                getString(R.string.borrar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int pos = 0;
                        for (int c = 0; c < ActivityPrincipalRemoto.desconexionesProgramadasRemoto.
                                getList().size(); ++c) {
                            if (ActivityPrincipalRemoto.desconexionesProgramadasRemoto
                                    .getList().get(c).getId() == id) {
                                pos = c;
                                break;
                            }
                        }
                        final int fpos = pos;
                        final Handler handler = new Handler();
                        findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                        findViewById(R.id.lyDis).setVisibility(View.VISIBLE);

                        JSONBorrarDesconexionProgramadaRemota jsonBorrarDesconexionProgramadaRemota =
                                new JSONBorrarDesconexionProgramadaRemota(ActivityProgramarDesconexion.this);
                        try {
                            jsonBorrarDesconexionProgramadaRemota.run(
                                    ActivityPrincipalRemoto.dispositivoControlado.getId_usuario(),
                                    ActivityPrincipalRemoto.dispositivoControlado.getIddisp(),
                                    ActivityPrincipalRemoto.desconexionesProgramadasRemoto.getList().get(fpos).getId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ActivityPrincipalRemoto.desconexionesProgramadasRemoto.getList().remove(fpos);
                                findViewById(R.id.pgBar).setVisibility(View.GONE);
                                findViewById(R.id.lyDis).setVisibility(View.GONE);
                                finish();
                            }
                        });
                    }
                }).setNegativeButton(getString(R.string.cancelar), null).show();
    }

    private void mandarDesconexionProgramadaRemota(final DesconexionProgramada desc) {
        final Handler handler = new Handler();
        findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
        findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
        if (TresAndroides.isOnline(this))
            new Thread() {
                @Override
                public void run() {
                    super.run();

                    JSONEnviarDesconexionProgramadaRemota jsonEnviarDesconexionProgramadaRemota =
                            new JSONEnviarDesconexionProgramadaRemota(ActivityProgramarDesconexion.this);
                    int res = -1;
                    try {
                        res = jsonEnviarDesconexionProgramadaRemota.run(
                                ActivityPrincipalRemoto.dispositivoControlado.getId_usuario(),
                                ActivityPrincipalRemoto.dispositivoControlado.getIddisp(), desc);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    final int fres = res;

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            desc.setId(fres);
                            Collections.reverse(ActivityPrincipalRemoto.desconexionesProgramadasRemoto.getList());
                            ActivityPrincipalRemoto.desconexionesProgramadasRemoto.getList().add(desc);
                            Collections.reverse(ActivityPrincipalRemoto.desconexionesProgramadasRemoto.getList());
                            findViewById(R.id.pgBar).setVisibility(View.GONE);
                            findViewById(R.id.lyDis).setVisibility(View.GONE);
                            finish();
                        }
                    });
                }
            }.start();
    }

    private void mandarDesconexionProgramadaRemotaEditar(final DesconexionProgramada desc) {
        final Handler handler = new Handler();
        findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
        findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
        desc.setId(id);
        if (TresAndroides.isOnline(this))
            new Thread() {
                @Override
                public void run() {
                    JSONEnviarDesconexionProgramadaRemotaModificar jsonEnviarDesconexionProgramadaRemotaModificar =
                            new JSONEnviarDesconexionProgramadaRemotaModificar(ActivityProgramarDesconexion.this);

                    try {
                        jsonEnviarDesconexionProgramadaRemotaModificar.run(
                                ActivityPrincipalRemoto.dispositivoControlado.getId_usuario(),
                                ActivityPrincipalRemoto.dispositivoControlado.getIddisp(), desc);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            int pos = 0;
                            for (int i = 0; i < ActivityPrincipalRemoto.desconexionesProgramadasRemoto.getList().size(); ++i) {
                                if (ActivityPrincipalRemoto.desconexionesProgramadasRemoto
                                        .getList().get(i).getId() == id) {
                                    pos = i;
                                }
                            }
                            ActivityPrincipalRemoto.desconexionesProgramadasRemoto.getList().set(pos, desc);
                            findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                            findViewById(R.id.lyDis).setVisibility(View.GONE);
                            finish();
                        }
                    });
                }
            }.start();
    }
}
