package com.atendus.app;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.atendus.app.adapters.AdapterExpandableDesconexionesProgramadas;
import com.atendus.app.adapters.AdapterExpandableDesconexionesProgramadasRemoto;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.DesconexionProgramada;
import com.atendus.app.json.JSONDesconexionesProgramadas;
import com.atendus.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;


// https://android-arsenal.com/details/1/6765#!description
public class FragmentProgramarDesconexion extends Fragment {

    private Context context;
    private View view;
    public AdapterExpandableDesconexionesProgramadas adapter = null;
    public AdapterExpandableDesconexionesProgramadasRemoto adapterRemoto = null;

    private static boolean usuarioRemoto = false;

    public FragmentProgramarDesconexion() {
    }

    public static FragmentProgramarDesconexion newInstance() {
        FragmentProgramarDesconexion fragmentFirst = new FragmentProgramarDesconexion();
        Bundle args = new Bundle();
        // args.putInt("someInt", ActivityPrincipal.kProgramarDesconexion);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        usuarioRemoto = false;
        return fragmentFirst;
    }

    public static FragmentProgramarDesconexion newInstanceRemoto() {
        FragmentProgramarDesconexion fragmentFirst = new FragmentProgramarDesconexion();
        Bundle args = new Bundle();
        // args.putInt("someInt", ActivityPrincipal.kProgramarDesconexion);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        usuarioRemoto = true;
        return fragmentFirst;
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_programar_desconexion, container, false);

        if (usuarioRemoto) {
            initRemoto();
        } else {
            init();
        }
        return view;
    }

    private void init() {
        view.findViewById(R.id.ly_add_desc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityProgramarDesconexion.class);
                context.startActivity(intent);
            }
        });
        adapter = new AdapterExpandableDesconexionesProgramadas(context);
        ExpandableListView expandableListView = view.findViewById(R.id.explv_desc_program);
        expandableListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        ((SwipeRefreshLayout) view.findViewById(R.id.swiperefresh)).setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                final Handler handler = new Handler();
                if (TresAndroides.isOnline(context))
                    new Thread() {
                        @Override
                        public void run() {
                            super.run();
                            final Aplicacion app = new Aplicacion();
                            app.cargarAplicacionDePreferencias(Objects.requireNonNull(getActivity()));
                            if (app.getColaEnvios().pendientes)
                                app.getColaEnvios().enviarTodos(context, app.getUsuario().getId());

                            JSONDesconexionesProgramadas jsonDesconexionesProgramadas =
                                    new JSONDesconexionesProgramadas(context);


                            final String iddisp = app.getUsuario().getNombrehijo();
                            int idusu = app.getUsuario().getId();
                            DesconexionProgramada.DesconexionProgramadaList list = null;
                            try {
                                list = jsonDesconexionesProgramadas.run(idusu, iddisp);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            final DesconexionProgramada.DesconexionProgramadaList flist = list;

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (flist != null) {
                                        app.getDesconexionesProgramadas().getList().clear();
                                        app.getDesconexionesProgramadas().getList().addAll(flist.getList());
                                        Collections.sort(app.getDesconexionesProgramadas().getList());
                                        app.guardarEnPreferencias(context);
                                        ExpandableListView expandableListView = view.findViewById(R.id.explv_desc_program);
                                        AdapterExpandableDesconexionesProgramadas adapter =
                                                new AdapterExpandableDesconexionesProgramadas(context);
                                        expandableListView.setAdapter(adapter);
                                    } else {
                                        app.getDesconexionesProgramadas().getList().clear();
                                        app.getDesconexionesProgramadas().setList(new ArrayList<DesconexionProgramada>());
                                        app.guardarEnPreferencias(context);
                                        ExpandableListView expandableListView = view.findViewById(R.id.explv_desc_program);
                                        AdapterExpandableDesconexionesProgramadas adapter =
                                                new AdapterExpandableDesconexionesProgramadas(context);
                                        expandableListView.setAdapter(adapter);
                                        ((SwipeRefreshLayout) view.findViewById(R.id.swiperefresh)).setRefreshing(false);
                                    }
                                    ((SwipeRefreshLayout) view.findViewById(R.id.swiperefresh)).setRefreshing(false);
                                }
                            });
                        }
                    }.start();
                else
                    ((SwipeRefreshLayout) view.findViewById(R.id.swiperefresh)).setRefreshing(false);
            }
        });

    }

    private void initRemoto() {
        view.findViewById(R.id.ly_add_desc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityProgramarDesconexion.class);
                intent.putExtra("remoto", true);
                context.startActivity(intent);
            }
        });
        adapterRemoto = new AdapterExpandableDesconexionesProgramadasRemoto(context);
        ExpandableListView expandableListView = view.findViewById(R.id.explv_desc_program);
        expandableListView.setAdapter(adapterRemoto);
        adapterRemoto.notifyDataSetChanged();

        view.findViewById(R.id.swiperefresh).setEnabled(false);

    }


    @Override
    public void onResume() {
        super.onResume();
        if (usuarioRemoto) {
            if (adapterRemoto != null)
                adapterRemoto.notifyDataSetChanged();
            return;
        }
        ExpandableListView expandableListView = view.findViewById(R.id.explv_desc_program);
        AdapterExpandableDesconexionesProgramadas adapter =
                new AdapterExpandableDesconexionesProgramadas(context);
        expandableListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        final Handler handler = new Handler();
        if (TresAndroides.isOnline(context))
            new Thread() {
                @Override
                public void run() {
                    super.run();

                    final Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(getActivity());
                    if (app.getColaEnvios().pendientes)
                        app.getColaEnvios().enviarTodos(context, app.getUsuario().getId());

                    JSONDesconexionesProgramadas jsonDesconexionesProgramadas =
                            new JSONDesconexionesProgramadas(context);


                    final String iddisp = app.getUsuario().getNombrehijo();
                    int idusu = app.getUsuario().getId();
                    DesconexionProgramada.DesconexionProgramadaList list = null;
                    try {
                        list = jsonDesconexionesProgramadas.run(idusu, iddisp);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final DesconexionProgramada.DesconexionProgramadaList flist = list;

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (flist != null) {
                                app.getDesconexionesProgramadas().getList().clear();
                                app.getDesconexionesProgramadas().getList().addAll(flist.getList());
                                Collections.sort(app.getDesconexionesProgramadas().getList());
                                app.guardarEnPreferencias(context);
                                ExpandableListView expandableListView = view.findViewById(R.id.explv_desc_program);
                                AdapterExpandableDesconexionesProgramadas adapter =
                                        new AdapterExpandableDesconexionesProgramadas(context);
                                expandableListView.setAdapter(adapter);
                                ((SwipeRefreshLayout) view.findViewById(R.id.swiperefresh)).setRefreshing(false);
                            } else {
                                app.getDesconexionesProgramadas().getList().clear();
                                app.getDesconexionesProgramadas().setList(new ArrayList<DesconexionProgramada>());
                                app.guardarEnPreferencias(context);
                                ExpandableListView expandableListView = view.findViewById(R.id.explv_desc_program);
                                AdapterExpandableDesconexionesProgramadas adapter =
                                        new AdapterExpandableDesconexionesProgramadas(context);
                                expandableListView.setAdapter(adapter);
                                ((SwipeRefreshLayout) view.findViewById(R.id.swiperefresh)).setRefreshing(false);
                            }
                        }
                    });
                }
            }.start();
    }
}
