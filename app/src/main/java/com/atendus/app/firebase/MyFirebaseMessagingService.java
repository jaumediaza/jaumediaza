package com.atendus.app.firebase;


import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.atendus.app.ActivityPrincipal;
import com.atendus.app.ActivityProgramarDesconexion;
import com.atendus.app.ActivityVinculacionesPendientes;
import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.ConfiguracionDesconexion;
import com.atendus.app.clases.DesconexionProgramada;
import com.atendus.app.json.JSONEnviarEstadoDispositivo;
import com.atendus.app.json.JSONEnviarTokenPush;
import com.atendus.app.services.NotificationMonitor;
import com.atendus.app.services.ServicioDesconexion;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.TresAndroides;
import com.atendus.app.utilidades.ViewPagerManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;

import java.util.Calendar;





/**
 * Created by Pau on 26/10/17.
 */


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final int kFLAG_INTENT= Intent.FLAG_ACTIVITY_NEW_TASK;
    private static final int kFLAG_PENDINGINTENT = PendingIntent.FLAG_CANCEL_CURRENT;

    private final String kCanalUsuariosConectados = "USUARIOS_CONECTADOS";




    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getBaseContext());
        boolean usuarioConectado = app.isUsuarioConectado();
        Log.d("BFreePruebas", "Notificación recibida"+remoteMessage.getData().get("action"));

        if (usuarioConectado)try {
            switch (remoteMessage.getData().get("action")){
                case "ACTION_DISCONNECT_NOW":{desconectarAhora(remoteMessage);break;}
                case "ACTION_VINCULACION":{notificarNuevaVinculacion();break;}
                case "ACTION_NEW_DISCONNECTION":{programarDesconexion(remoteMessage);break;}
                case "ACTION_DELETE_DISCONNECTION":{borrarDesconexionProgramada(remoteMessage);break;}
                case "ACTION_UPDATE_DISCONNECTION":{modificarDesconexionProgramada(remoteMessage);break;}
                case "ACTION_CHECK_STATUS_NOW":{actualizarEstado();break;}
                case "ACTION_STOP_DISCONNECTION":{cancelarDesconexion();break;}
                default:{notificacion(remoteMessage);break;}
            }


        }catch (NullPointerException e){
            e.printStackTrace();
        }



    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        final Aplicacion app = new Aplicacion();
        final Context c = this;
        app.cargarAplicacionDePreferencias(this);
        String token = FirebaseInstanceId.getInstance().getToken();
        if (!app.getUsuario().getToken().equals(token)){
            final String oldToken = app.getUsuario().getToken();
            app.getUsuario().setToken(token);
            app.guardarEnPreferencias(this);

            if (TresAndroides.isOnline(this)){
                Thread tToken = new Thread() {
                    @Override
                    public void run()
                    {
                        JSONEnviarTokenPush jsonEnviarToken=new JSONEnviarTokenPush(c);
                        try {


                            final String iddisp = app.getUsuario().getNombrehijo();
                            jsonEnviarToken.run(app.getUsuario().getId(),app.getUsuario().getToken(), oldToken, iddisp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                };

                tToken.start();
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void desconectarAhora(RemoteMessage msg){
        int hh = Integer.valueOf(msg.getData().get("hh"));
        int mm = Integer.valueOf(msg.getData().get("mm"));
        int notifPermitidas = Integer.valueOf(msg.getData().get("notifPermitidas"));
        int llamadasPermitidas = Integer.valueOf( msg.getData().get("llamadasPermitidas"));

        int editable = Integer.valueOf(msg.getData().get("editable"));

        String[] appsPermitidas;

        String auxApps = msg.getData().get("appsPermitidas");

        if (PhoneStateManager.getInstance().isDesconectado()){
            PhoneStateManager.getInstance().setDesconectado(false);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (auxApps !=null) {
            appsPermitidas = auxApps.split("/");
            if (appsPermitidas.length > 0) {
                for (int i = 0; i < PhoneStateManager.getInstance().getAppsPermitidas().size(); ++i) {
                    PhoneStateManager.getInstance().getAppsPermitidas().get(i).setPermitida(false);
                    PhoneStateManager.getInstance().getAppsPermitidas().get(i).setUsoDuranteDesconecta(false);
                    for (int j = 0; j < appsPermitidas.length; ++j) {
                        if (PhoneStateManager.getInstance().getAppsPermitidas().get(i).getPackageName().equals(appsPermitidas[j])) {
                            PhoneStateManager.getInstance().getAppsPermitidas().get(i).setPermitida(true);
                            PhoneStateManager.getInstance().getAppsPermitidas().get(i).setUsoDuranteDesconecta(true);
                        }
                    }
                }
            }else{
                for (int i = 0; i < PhoneStateManager.getInstance().getAppsPermitidas().size(); ++i) {
                    PhoneStateManager.getInstance().getAppsPermitidas().get(i).setPermitida(false);
                    PhoneStateManager.getInstance().getAppsPermitidas().get(i).setUsoDuranteDesconecta(false);
                }
            }
        }

        String auxTfnos = msg.getData().get("numerosPermitidos");
        if (auxTfnos !=null){
            String[] tfnosPermintidos = auxTfnos.split("/");
            if(tfnosPermintidos.length > 0){
                for (int i = 0; i < PhoneStateManager.getInstance().getTfnosPermitidos().size(); ++i) {
                    PhoneStateManager.getInstance().getTfnosPermitidos().get(i).setPermitido(false);
                    for (int j = 0; j < tfnosPermintidos.length; ++j) {
                        if (PhoneStateManager.getInstance().getTfnosPermitidos().get(i).getNumero().contains(tfnosPermintidos[j].trim())) {
                            PhoneStateManager.getInstance().getTfnosPermitidos().get(i).setPermitido(true);
                        }
                    }
                }
            }else{
                for (int i = 0; i < PhoneStateManager.getInstance().getTfnosPermitidos().size(); ++i) {
                    PhoneStateManager.getInstance().getTfnosPermitidos().get(i).setPermitido(false);
                }
            }
        }

        PhoneStateManager.getInstance().updateTelefonosPermitidos();

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);

        PhoneStateManager.getInstance().setFromProgramarDesconexion(true);

        ConfiguracionDesconexion desconexion = new ConfiguracionDesconexion();

        if (notifPermitidas == 0){
            desconexion.setBloquearNotificaciones(true);
        }else{
            desconexion.setBloquearNotificaciones(false);
        }

        if (llamadasPermitidas == 0){
            desconexion.setBloquearLlamadas(true);
        }else{
            desconexion.setBloquearLlamadas(false);
        }

        PhoneStateManager.getInstance().setDesconectado(true);
        int tiempo  = hh * 60 * 60 *1000 + mm * 60 *1000;
        desconexion.setTiempoDesconexion(tiempo, hh, mm);
        PhoneStateManager.getInstance().getDesconexion().setTiempoDesconexion(tiempo, hh, mm);

        PhoneStateManager.getInstance().getDesconexion().setBloquearLlamadas(desconexion.isBloquearLlamadas());
        PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(desconexion.isBloquearNotificaciones());
        PhoneStateManager.getInstance().getNotificaciones().getList().clear();

        PhoneStateManager.getInstance().getDesconexion().setlDateStart();
        PhoneStateManager.getInstance().getDesconexion().setAudioStatus(getBaseContext());

        PhoneStateManager.getInstance().setMillisUntilFinished(tiempo);
        PhoneStateManager.getInstance().guardarDatosEnAplicacion(getBaseContext());

        if (editable == 0)
            PhoneStateManager.getInstance().setEditable(false);

        Intent servicioDesconexion = new Intent(getBaseContext(), ServicioDesconexion.class);
        Intent notificationMonitor = new Intent(getBaseContext(), NotificationMonitor.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getBaseContext().startForegroundService(servicioDesconexion);
            getBaseContext().startForegroundService(notificationMonitor);
            Log.d("BFreePruebas", "Desconexion programada ServicioIniciado");
        } else {
            getBaseContext().startService(servicioDesconexion);
            getBaseContext().startService(notificationMonitor);
            Log.d("BFreePruebas", "Desconexion programada ServicioIniciado");
        }
        Intent intnt = new Intent(getBaseContext(), ActivityPrincipal.class);
        intnt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intnt);

        Log.d("BFreePruebas", "Desconexion programada Activity Principal");

        new Thread(){
            @Override
            public void run() {
                super.run();

                Aplicacion app = new Aplicacion();
                app.cargarAplicacionDePreferencias(getApplicationContext());
                final String iddisp = app.getUsuario().getNombrehijo();

                JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo =
                        new JSONEnviarEstadoDispositivo(getApplication());
                int estdao = 0;
                if (PhoneStateManager.getInstance().isDesconectado())
                    estdao = 1;
                try {
                    jsonEnviarEstadoDispositivo.run(app.getUsuario().getId(), iddisp, estdao);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    private void notificarNuevaVinculacion(){
        Context context = getApplicationContext();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(kCanalUsuariosConectados,
                    context.getString(R.string.descripcion_canal_usuarios_conectados),
                    NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(context.getString(R.string.descripcion_canal_usuarios_conectados));
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Intent intent=null;
        PendingIntent pendingIntent=null;

        intent = new Intent(this, ActivityVinculacionesPendientes.class);
        intent.addFlags(kFLAG_INTENT);

        int notifID = (int) Calendar.getInstance().getTime().getTime();


        pendingIntent = PendingIntent.getActivity(this, (int)Calendar.getInstance().getTime().getTime(), intent,
                kFLAG_PENDINGINTENT);




        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, kCanalUsuariosConectados);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.N){
            notificationBuilder.setSmallIcon(R.drawable.ic_bfree)
                    .setContentTitle(context.getString(R.string.notificacionNuevaVinculacionTitulo))
                    .setContentText(context.getString(R.string.notificacionNuevaVinculacionMensaje))
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(context.getString(R.string.notificacionNuevaVinculacionMensaje)))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);
        }else{
            notificationBuilder.setSmallIcon(R.drawable.ic_bfree)
                    .setContentTitle(context.getString(R.string.notificacionNuevaVinculacionTitulo)
                            + " " + context.getString(R.string.notificacionNuevaVinculacionMensaje))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);
        }




        notificationManager.notify(notifID, notificationBuilder.build());
        AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
    }
    private void programarDesconexion(RemoteMessage msg){
        DesconexionProgramada desc = new DesconexionProgramada();
        desc.setStrTitulo(msg.getData().get("titulo"));
        desc.setHoraInicio(Integer.parseInt(msg.getData().get("hh_inicio")));
        desc.setMinutoInicio(Integer.parseInt(msg.getData().get("mm_inicio")));
        desc.setHoraFin(Integer.parseInt(msg.getData().get("hh_fin")));
        desc.setMinutoFin(Integer.parseInt(msg.getData().get("mm_fin")));

        int notifPermitidas = Integer.valueOf(msg.getData().get("notifPermitidas"));
        int llamadasPermitidas = Integer.valueOf( msg.getData().get("llamadasPermitidas"));
        int activa = Integer.valueOf( msg.getData().get("activa"));
        int repetir = Integer.valueOf( msg.getData().get("repetir"));

        int lunes = Integer.valueOf( msg.getData().get("lunes"));
        int martes = Integer.valueOf( msg.getData().get("martes"));
        int miercoles = Integer.valueOf( msg.getData().get("miercoles"));
        int jueves = Integer.valueOf( msg.getData().get("jueves"));
        int viernes = Integer.valueOf( msg.getData().get("viernes"));
        int sabado = Integer.valueOf( msg.getData().get("sabado"));
        int domingo = Integer.valueOf( msg.getData().get("domingo"));

        int id = Integer.valueOf( msg.getData().get("id"));

        boolean editable = false;
            if(Integer.valueOf( msg.getData().get("editable")) == 1)
                editable = true;

        desc.setBloquearNotificaciones(notifPermitidas!=0);
        desc.setBloquearLlamadas(llamadasPermitidas!=0);
        desc.setEnabled( activa!=0);

        if (repetir == 0) {
            desc.setRepetir(1);
        }else{
            desc.setRepetir(2);
        }

        desc.getDias()[0] = lunes != 0;
        desc.getDias()[1] = martes != 0;
        desc.getDias()[2] = miercoles != 0;
        desc.getDias()[3] = jueves != 0;
        desc.getDias()[4] = viernes != 0;
        desc.getDias()[5] = sabado != 0;
        desc.getDias()[6] = domingo != 0;

        desc.setId(id);
        desc.setEditable(editable);

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getApplication());
        app.getDesconexionesProgramadas().getList().add(desc);
        app.guardarEnPreferencias(getApplication());

        if (ViewPagerManager.getInstance().fragmentProgramarDesconexion != null){
            if (ViewPagerManager.getInstance().fragmentProgramarDesconexion.adapter != null){
                ViewPagerManager.getInstance().fragmentProgramarDesconexion.adapter.notifyDataSetChanged();
            }
        }

    }

    private void borrarDesconexionProgramada(RemoteMessage msg){
        int id = Integer.parseInt(msg.getData().get("id"));

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getApplication());

        for (int i = 0; i < app.getDesconexionesProgramadas().getList().size(); ++i){
            if (app.getDesconexionesProgramadas().getList().get(i).getId() == id){
                app.getDesconexionesProgramadas().getList().remove(i);
            }
        }

        app.guardarEnPreferencias(getApplication());

    }
    private void modificarDesconexionProgramada(RemoteMessage msg){
        DesconexionProgramada desc = new DesconexionProgramada();
        desc.setStrTitulo(msg.getData().get("titulo"));
        desc.setHoraInicio(Integer.parseInt(msg.getData().get("hh_inicio")));
        desc.setMinutoInicio(Integer.parseInt(msg.getData().get("mm_inicio")));
        desc.setHoraFin(Integer.parseInt(msg.getData().get("hh_fin")));
        desc.setMinutoFin(Integer.parseInt(msg.getData().get("mm_fin")));

        int notifPermitidas = Integer.valueOf(msg.getData().get("notifPermitidas"));
        int llamadasPermitidas = Integer.valueOf( msg.getData().get("llamadasPermitidas"));
        int activa = Integer.valueOf( msg.getData().get("activa"));
        int repetir = Integer.valueOf( msg.getData().get("repetir"));

        int lunes = Integer.valueOf( msg.getData().get("lunes"));
        int martes = Integer.valueOf( msg.getData().get("martes"));
        int miercoles = Integer.valueOf( msg.getData().get("miercoles"));
        int jueves = Integer.valueOf( msg.getData().get("jueves"));
        int viernes = Integer.valueOf( msg.getData().get("viernes"));
        int sabado = Integer.valueOf( msg.getData().get("sabado"));
        int domingo = Integer.valueOf( msg.getData().get("domingo"));

        int id = Integer.valueOf( msg.getData().get("id"));

        boolean editable = false;
        if(Integer.valueOf( msg.getData().get("editable")) == 1)
            editable = true;


        desc.setBloquearNotificaciones(notifPermitidas!=0);
        desc.setBloquearLlamadas(llamadasPermitidas!=0);
        desc.setEnabled( activa!=0);

        if (repetir == 0) {
            desc.setRepetir(1);
        }else{
            desc.setRepetir(2);
        }

        desc.getDias()[0] = lunes != 0;
        desc.getDias()[1] = martes != 0;
        desc.getDias()[2] = miercoles != 0;
        desc.getDias()[3] = jueves != 0;
        desc.getDias()[4] = viernes != 0;
        desc.getDias()[5] = sabado != 0;
        desc.getDias()[6] = domingo != 0;

        desc.setId(id);
        desc.setEditable(editable);


        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getApplication());

        int i = 0;
        for (DesconexionProgramada d : app.getDesconexionesProgramadas().getList()){
            if (d.getId() == id){
                app.getDesconexionesProgramadas().getList().set(i, desc);
            }
            i++;
        }

        app.guardarEnPreferencias(getApplication());

        if (ViewPagerManager.getInstance().fragmentProgramarDesconexion != null){
            if (ViewPagerManager.getInstance().fragmentProgramarDesconexion.adapter != null){
                ViewPagerManager.getInstance().fragmentProgramarDesconexion.adapter.notifyDataSetChanged();
            }
        }
    }

    private void notificacion(RemoteMessage msg){
        Context context = getApplicationContext();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(kCanalUsuariosConectados,
                    context.getString(R.string.descripcion_canal_usuarios_conectados),
                    NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(context.getString(R.string.descripcion_canal_usuarios_conectados));
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Intent intent=null;
        PendingIntent pendingIntent=null;

        intent = new Intent(this, ActivityPrincipal.class);
        intent.addFlags(kFLAG_INTENT);

        int notifID = (int) Calendar.getInstance().getTime().getTime();


        pendingIntent = PendingIntent.getActivity(this, (int)Calendar.getInstance().getTime().getTime(), intent,
                kFLAG_PENDINGINTENT);




        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, kCanalUsuariosConectados);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.N){
            notificationBuilder.setSmallIcon(R.drawable.ic_bfree)
                    .setContentTitle(msg.getData().get("titulo"))
                    .setContentText(msg.getData().get("mensaje"))
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(msg.getData().get("mensaje")))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);
        }else{
            notificationBuilder.setSmallIcon(R.drawable.ic_bfree)
                    .setContentTitle(msg.getData().get("titulo")
                            + " " + msg.getData().get("mensaje"))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH);
        }




        notificationManager.notify(notifID, notificationBuilder.build());
        AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
    }
    private void actualizarEstado(){
        new Thread(){
            @Override
            public void run() {
                super.run();


                Aplicacion app = new Aplicacion();
                app.cargarAplicacionDePreferencias(getApplication());
                final String iddisp = app.getUsuario().getNombrehijo();
                final int idusus = app.getUsuario().getId();

                JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo = new
                        JSONEnviarEstadoDispositivo(getApplication());

                int estdao = 0;
                if (PhoneStateManager.getInstance().isDesconectado())
                    estdao = 1;
                try {
                    jsonEnviarEstadoDispositivo.run(idusus, iddisp, estdao);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    private void cancelarDesconexion(){
        if (PhoneStateManager.getInstance().isDesconectado()){
            if (ViewPagerManager.getInstance().fragmentDesconecta != null){
                ViewPagerManager.getInstance().fragmentDesconecta.desconectarOnUiThread();
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                PhoneStateManager.getInstance().setDesconectado(false);
                PhoneStateManager.getInstance().guardarDatosEnAplicacion(this);
            }else{
                PhoneStateManager.getInstance().setDesconectado(false);
            }


        }

        new Thread(){
            @Override
            public void run() {
                super.run();
                if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

                Aplicacion app = new Aplicacion();
                app.cargarAplicacionDePreferencias(getApplication());
                final String iddisp = app.getUsuario().getNombrehijo();

                JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo =
                        new JSONEnviarEstadoDispositivo(getApplication());
                int estdao = 0;
                if (PhoneStateManager.getInstance().isDesconectado())
                    estdao = 1;
                try {
                    jsonEnviarEstadoDispositivo.run(app.getUsuario().getId(), iddisp, estdao);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }
}

