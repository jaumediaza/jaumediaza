package com.atendus.app.remoto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.atendus.app.ActivityPrincipal;
import com.atendus.app.FragmentAplicaciones;
import com.atendus.app.FragmentDatosDeUso;
import com.atendus.app.FragmentDesconecta;
import com.atendus.app.FragmentNotificaciones;
import com.atendus.app.FragmentProgramarDesconexion;
import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.clases.DesconexionProgramada;
import com.atendus.app.clases.DispositivosControlados;
import com.atendus.app.clases.EstadisticasUsuarioRemoto;
import com.atendus.app.clases.Telefono;
import com.atendus.app.json.JSONDesconexionesProgramadasRemoto;
import com.atendus.app.json.JSONDispositivosControladosApps;
import com.atendus.app.json.JSONDispositivosControladosEstadisticas;
import com.atendus.app.json.JSONDispositivosControladosTelefonos;
import com.atendus.app.utilidades.CustomViewPager;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.ViewPagerManager;
import com.lukedeighton.wheelview.WheelView;
import com.lukedeighton.wheelview.adapter.WheelAdapter;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import spencerstudios.com.bungeelib.Bungee;

public class ActivityPrincipalRemoto extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static DispositivosControlados dispositivoControlado;
    private static DrawerLayout drawer;
    private static CustomViewPager mViewPager;
    private FragmentPagerAdapter adapterViewPager;


    public static final int kTotal = 5; // Total de pantallas

    public static final int kAplicaciones = 0;
    public static final int kDesconecta = 1;
    public static final int kEstadisticas = 2;
    public static final int kDesconexionesProgramadas = 3;
    public static final int kNotificaciones = 4;


    List<Fragment> fragmentsList;

    private static Intent intent;

    public static List<AplicacionPermitida> appsPermitidas;
    public static List<Telefono> tfnosPermitidos;
    public static EstadisticasUsuarioRemoto estadisticasUsuarioRemoto;
    public static DesconexionProgramada.DesconexionProgramadaList desconexionesProgramadasRemoto;
    public static ConstraintLayout lyMain;

    private static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        intent = getIntent();

        activity = this;

        findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
        findViewById(R.id.lyDis).setVisibility(View.VISIBLE);


        dispositivoControlado = new DispositivosControlados();
        dispositivoControlado.setId_usuario(intent.getIntExtra("idUsuario", 0));
        dispositivoControlado.setId_dispositivo(intent.getIntExtra("idDispositivo", 0));
        dispositivoControlado.setIddisp(intent.getStringExtra("iddisp"));
        dispositivoControlado.setUltima_desconexion_hh(intent.getIntExtra("horas", 0));
        dispositivoControlado.setUltima_desconexion_mm(intent.getIntExtra("minutos", 0));
        dispositivoControlado.setNombrehijo(intent.getStringExtra("nombrehijo"));
        dispositivoControlado.setUltima_desconexion_bloquear_apps(
                intent.getBooleanExtra("notificaciones", false));
        dispositivoControlado.setUltima_desconexion_bloquear_llamadas(
                intent.getBooleanExtra("llamadas", false));
        dispositivoControlado.setUltima_desconexion_lista_contactos(
                intent.getStringExtra("listaTfnos"));
        dispositivoControlado.setUltima_desconexion_lista_apps(
                intent.getStringExtra("listaApps"));
        dispositivoControlado.setEtiqueta(intent.getStringExtra("etiqueta"));
        dispositivoControlado.setEstadoDesconexion(intent.getIntExtra("estadoDesconexion", 0));

        ((TextView) findViewById(R.id.Nombre)).setText(dispositivoControlado.getNombrehijo());
        ((TextView) findViewById(R.id.dispositivo)).setText(dispositivoControlado.getEtiqueta());
        findViewById(R.id.Nombre).setVisibility(View.VISIBLE);
        findViewById(R.id.dispositivo).setVisibility(View.VISIBLE);

        final Handler handler = new Handler();
        new Thread() {
            @Override
            public void run() {
                super.run();
                appsPermitidas = new ArrayList<>();
                tfnosPermitidos = new ArrayList<>();
                desconexionesProgramadasRemoto = new DesconexionProgramada.DesconexionProgramadaList();
                JSONDispositivosControladosApps jsonDispositivosControladosApps =
                        new JSONDispositivosControladosApps(ActivityPrincipalRemoto.this);
                JSONDispositivosControladosTelefonos jsonDispositivosControladosTelefonos =
                        new JSONDispositivosControladosTelefonos(ActivityPrincipalRemoto.this);
                JSONDispositivosControladosEstadisticas jsonDispositivosControladosEstadisticas =
                        new JSONDispositivosControladosEstadisticas(ActivityPrincipalRemoto.this);
                JSONDesconexionesProgramadasRemoto jsonDesconexionesProgramadasRemoto =
                        new JSONDesconexionesProgramadasRemoto(ActivityPrincipalRemoto.this);
                try {
                    List<AplicacionPermitida> apps = jsonDispositivosControladosApps.run(dispositivoControlado.getId_usuario(),
                            dispositivoControlado.getIddisp());
                    List<Telefono> telefonos = jsonDispositivosControladosTelefonos.run(
                            dispositivoControlado.getId_usuario(), dispositivoControlado.getIddisp());
                    if (apps != null && apps.size() >= 1) {
                        appsPermitidas.addAll(apps);
                    }
                    if (telefonos != null && telefonos.size() >= 1) {
                        tfnosPermitidos.addAll(telefonos);
                    }
                    estadisticasUsuarioRemoto = jsonDispositivosControladosEstadisticas.run(
                            dispositivoControlado.getId_usuario(), dispositivoControlado.getIddisp());
                    desconexionesProgramadasRemoto = jsonDesconexionesProgramadasRemoto.run(
                            dispositivoControlado.getId_usuario(), dispositivoControlado.getIddisp());

                    String tfs[] = null;
                    if (dispositivoControlado.getUltima_desconexion_lista_contactos() != null) {
                        tfs = dispositivoControlado.getUltima_desconexion_lista_contactos().trim().split("/");
                    }
                    for (Telefono t : tfnosPermitidos) {
                        for (String tf : tfs) {
                            if (tf.equals(t.getNumero())) {
                                t.setPermitido(true);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("wat", appsPermitidas.size() + " ");
                        // ((FragmentAplicaciones)fragmentsList.get(kAplicaciones)).initRemoto();
                        findViewById(R.id.pgBar).setVisibility(View.GONE);
                        findViewById(R.id.lyDis).setVisibility(View.GONE);
                    }
                });

            }
        }.start();

        findViewById(R.id.pgBar).setVisibility(View.GONE);
        findViewById(R.id.lyDis).setVisibility(View.GONE);
        fragmentsList = new ArrayList<>();


       fragmentsList.add(FragmentAplicaciones.newInstance(dispositivoControlado));
       fragmentsList.add(FragmentDesconecta.newInstance(intent, dispositivoControlado));
        fragmentsList.add(FragmentDatosDeUso.newInstance(true));
        fragmentsList.add(FragmentProgramarDesconexion.newInstanceRemoto());
        fragmentsList.add(FragmentNotificaciones.newInstance());

        adapterViewPager = new ActivityPrincipalRemoto.MyPagerAdapter(getSupportFragmentManager(), this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);

        mViewPager.setAdapter(adapterViewPager);

        mViewPager.setCurrentItem(kDesconecta);

        findViewById(R.id.arrBack).setVisibility(View.VISIBLE);
        findViewById(R.id.arrBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        findViewById(R.id.hamburguer).setVisibility(View.GONE);

        findViewById(R.id.imgAPPs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(kAplicaciones);
            }
        });

        findViewById(R.id.imgDesconecta).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(kDesconecta);
            }
        });

        findViewById(R.id.imgEstadisticas).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(kEstadisticas);
            }
        });

        findViewById(R.id.imgProgDesc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewPager.setCurrentItem(kDesconexionesProgramadas);
            }
        });

        lyMain = findViewById(R.id.ly_main);

        if (dispositivoControlado.getEstadoDesconexion() == 1) {
            lyMain.setBackground(getDrawable(R.drawable.gradient_background_off));
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.backgroundStart_off));
        } else {
            lyMain.setBackground(getDrawable(R.drawable.gradient_background));
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getColor(R.color.backgroundStart));
        }


        final com.lukedeighton.wheelview.WheelView wheelView = (com.lukedeighton.wheelview.WheelView) findViewById(R.id.wheelview);
        wheelView.setAdapter(new WheelAdapter() {
            @Override
            public Drawable getDrawable(int position) {
                switch (position) {
                    case 0:
                        return getResources().getDrawable(R.drawable.iconos_02);
                    case 1:
                        return getResources().getDrawable(R.drawable.iconos_01);
                    case 2:
                        return getResources().getDrawable(R.drawable.iconos_07);
                    case 3:
                        return getResources().getDrawable(R.drawable.iconos_06);
                    case 4:
                        return getResources().getDrawable(R.drawable.iconos_03);
                    default:
                        return getResources().getDrawable(R.drawable.iconos_03);
                }

                //return drawable here - the position can be seen in the gifs above
            }

            @Override
            public int getCount() {
                return kTotal;
            }
        });

        ViewPagerManager.getInstance().pager = mViewPager;

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int mCurrentPosition;
            private int mScrollState;
            private int mPreviousPosition;

            private void setNextItemIfNeeded() {
                if (!isScrollStateSettling()) {
                    handleSetNextItem();
                }
            }

            private boolean isScrollStateSettling() {
                return mScrollState == ViewPager.SCROLL_STATE_SETTLING; //indicated page is settling to it's final position
            }

            private void handleSetNextItem() {
                final int lastPosition = mViewPager.getAdapter().getCount() - 1;
                if (mCurrentPosition == 0) {
                    mViewPager.setCurrentItem(lastPosition, false);
                    wheelView.setSelected(lastPosition);
                } else if (mCurrentPosition == lastPosition) {
                    mViewPager.setCurrentItem(0, false);
                    wheelView.setSelected(0);
                }


            }

            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    setNextItemIfNeeded();
                }
                mScrollState = state;
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                mCurrentPosition = position;
                mPreviousPosition = position - 1;


                wheelView.setSelected(position);

                /*if (position == kDesconecta) {
                    mViewPager.setCurrentItem(kAplicaciones, true);
                }

                // skip fake page (last), go to first page
                if (position == kAplicaciones) {
                    mViewPager.setCurrentItem(kDesconecta, false);
                }*/
                //wheelView.setSelected(position);
            }
        });

        wheelView.setSelected(1);
        wheelView.setOnWheelItemClickListener(new WheelView.OnWheelItemClickListener() {
            @Override
            public void onWheelItemClick(WheelView parent, int position, boolean isSelected) {
                parent.setSelected(position);
                mViewPager.setCurrentItem(position);

            }
        });

    }

    public static void setBAckgroundDesconectado(boolean desconectado, Context c) {
        if (desconectado) {
            lyMain.findViewById(R.id.ly_main).setBackground(c.getDrawable(R.drawable.gradient_background_off));

            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(c.getColor(R.color.backgroundStart_off));
        } else {
            lyMain.findViewById(R.id.ly_main).setBackground(c.getDrawable(R.drawable.gradient_background));


            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(c.getColor(R.color.backgroundStart));
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 5;

        public MyPagerAdapter(FragmentManager fragmentManager, Context c) {
            super(fragmentManager);
            Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(c);
            NUM_ITEMS = kTotal;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {

            switch (position) {

                case kAplicaciones: {
                    return fragmentsList.get(kAplicaciones);
                }
                case kDesconecta: { // Fragment # 0 - This will show FirstFragment
                    if (PhoneStateManager.getInstance().isDesconectado() &&
                            ViewPagerManager.getInstance().fragmentDesconecta != null) {
                        mViewPager.setPagingEnabled(false);
                        ViewPagerManager.getInstance().fragmentDesconecta.setmViewPager(mViewPager);
                        return ViewPagerManager.getInstance().fragmentDesconecta;
                    }
                    ViewPagerManager.getInstance().fragmentDesconecta = (FragmentDesconecta) fragmentsList.get(kDesconecta);

                    return ViewPagerManager.getInstance().fragmentDesconecta;
                }
                case kEstadisticas: {
                    return fragmentsList.get(kEstadisticas);
                }
                case kNotificaciones: {
                    return fragmentsList.get(kNotificaciones);
                }
                case kDesconexionesProgramadas: {
                    ViewPagerManager.getInstance().fragmentProgramarDesconexion = (FragmentProgramarDesconexion) fragmentsList.get(kDesconexionesProgramadas);
                    return ViewPagerManager.getInstance().fragmentProgramarDesconexion;
                }
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ActivityPrincipal.class);
        startActivity(intent);
        Bungee.fade(activity);
        finish();
    }
}




















