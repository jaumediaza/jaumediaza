package com.atendus.app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.json.JSONModificarVinculacion;
import com.atendus.app.json.JSONVinculacionesPendientes;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;



public class ActivityVinculacionesPendientes extends AppCompatActivity {

    private LinearLayout lyScroll;
    private LayoutInflater inflater = null;

    private LinearLayout lyNoVinculaciones;
    private ProgressBar pgBar;

    private int idusu;
    private String iddisp;
    private SwipeRefreshLayout swipe;
    int enPantalla = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vinculaciones_pendientes);

        lyScroll = findViewById(R.id.lyScroll);
        lyNoVinculaciones = findViewById(R.id.lyNoVinculaciones);
        pgBar = findViewById(R.id.pgBar);

        inflater = LayoutInflater.from(this);

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        idusu = app.getUsuario().getId();

        iddisp = app.getUsuario().getNombrehijo();

        hiloComprobarVinculaciones();

        findViewById(R.id.arr_back3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

        swipe = findViewById(R.id.swiperefresh);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                hiloComprobarVinculaciones();
            }
        });
    }

    private void modificarVinculacion(final int id, final int estado, final View v){
        final Handler handler = new Handler();
        new Thread(){
            @Override
            public void run() {
                super.run();
                JSONModificarVinculacion jsonModificarVinculacion =
                        new JSONModificarVinculacion(ActivityVinculacionesPendientes.this);
                try {
                    jsonModificarVinculacion.run(id, estado);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        v.setVisibility(View.GONE);
                        AlertDialog.Builder dlg = new AlertDialog.Builder(ActivityVinculacionesPendientes.this);
                        if (estado == 1) {
                            dlg.setMessage(getString(R.string.vinculacionExitosa));
                        }else{
                            dlg.setMessage(getString(R.string.vinculacionNo));
                        }
                        dlg.setPositiveButton(getString(R.string.aceptar), null);
                        dlg.show();
                        enPantalla--;

                        if (enPantalla <= 0){
                            lyNoVinculaciones.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        }.start();
    }
    private void hiloComprobarVinculaciones(){
        pgBar.setVisibility(View.VISIBLE);
        enPantalla = 0;
        final Handler handler = new Handler();
        new Thread (){
            @Override
            public void run() {
                super.run();
                JSONVinculacionesPendientes jsonVinculacionesPendientes = new JSONVinculacionesPendientes(ActivityVinculacionesPendientes.this);
                final List<NuevaVinculacion> vinculaciones = new ArrayList<>();
                boolean isnull;
                try {
                    vinculaciones.addAll(jsonVinculacionesPendientes.run(idusu, iddisp));
                    isnull = false;
                } catch (Exception e){
                    e.printStackTrace();
                    isnull = true;
                }

                final boolean fIsnull = isnull;

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (!fIsnull) {
                            lyScroll.removeAllViews();
                            enPantalla = vinculaciones.size();
                            for (NuevaVinculacion vinc : vinculaciones) {
                                final NuevaVinculacion fVinc = vinc;
                                final View v = inflater.inflate(R.layout.celda_vinculacion, null);
                                TextView titulo = v.findViewById(R.id.titulo);
                                String strTxt = titulo.getText() + "\n" + getString(R.string.UsuarioSolicitudPendiente) + " " + vinc.nombreControlador;
                                titulo.setText(strTxt);

                                v.findViewById(R.id.aceptar).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        modificarVinculacion(fVinc.id, 1, v);

                                    }
                                });

                                v.findViewById(R.id.rechazar).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        modificarVinculacion(fVinc.id, 2, v);
                                    }
                                });

                                lyScroll.addView(v);
                            }
                            lyNoVinculaciones.setVisibility(View.GONE);
                            if (vinculaciones.size() == 0) {
                                lyNoVinculaciones.setVisibility(View.VISIBLE);
                            }
                            pgBar.setVisibility(View.GONE);
                            swipe.setRefreshing(false);
                        }else{
                            lyNoVinculaciones.setVisibility(View.VISIBLE);
                            pgBar.setVisibility(View.GONE);
                            if(swipe!=null)
                            swipe.setRefreshing(false);
                        }
                    }
                });
            }
        }.start();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        hiloComprobarVinculaciones();
    }

    public  static  class NuevaVinculacion{
        public int id=0;
        public int idUsuarioControlador=0;
        public int idDispositivo;
        public int idUsuario;
        public int estado = 0;
        public String nombreControlador ="";
    }
}
