package com.atendus.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.atendus.app.adapters.AdapterNotificaciones;
import com.atendus.app.utilidades.PhoneStateManager;



public class FragmentNotificaciones extends Fragment{

    private View view;
    private Context context;

    private RecyclerView rv_notif;
    private AdapterNotificaciones adapter;

    public Switch sw_bloquear_notif;

    private boolean aux = true;

    //public static

    public FragmentNotificaciones() {}

    public static FragmentNotificaciones newInstance() {
        FragmentNotificaciones fragmentFirst = new FragmentNotificaciones();
        Bundle args = new Bundle();
       // args.putInt("someInt", ActivityPrincipal.kNotificaciones);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notificaciones, container, false);

        init();
        return view;
    }

    private void init() {
        rv_notif = view.findViewById(R.id.rv_notif);
        adapter = new AdapterNotificaciones(context);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_notif.setLayoutManager(linearLayoutManager);

        rv_notif.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        sw_bloquear_notif = view.findViewById(R.id.sw_bloquear_notif);
        sw_bloquear_notif.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones());
        sw_bloquear_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones()){
                    PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(false);
                    sw_bloquear_notif.setChecked(false);
                }else{
                    PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(true);
                    sw_bloquear_notif.setChecked(true);
                }
                PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);
            }
        });

        sw_bloquear_notif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (aux){
                    aux = false;
                    sw_bloquear_notif.setChecked(PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas());
                }
            }
        });

        view.findViewById(R.id.refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.notifyDataSetChanged();
            }
        });

        view.findViewById(R.id.btn_ver_mas).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context ,ActivityDesconexionesAnteriores.class);
                startActivity(intent);
            }
        });

        if (PhoneStateManager.getInstance().getNotificaciones().getList().size() > 0){
            view.findViewById(R.id.txt_no_notif).setVisibility(View.GONE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    public void updateRootView(View v){
        view = v;
    }

}
