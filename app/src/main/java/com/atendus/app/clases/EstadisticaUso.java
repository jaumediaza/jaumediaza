package com.atendus.app.clases;

public class EstadisticaUso {
    private int desbloqueos = 0;
    private long tiempoUso = 0L;



    // Dia del año / Semana del año / Mes del año
    private int datoFecha = -1;
    private long lFecha = -1;

    public int getDesbloqueos() {
        return desbloqueos;
    }

    public void setDesbloqueos(int desbloqueos) {
        this.desbloqueos = desbloqueos;
    }

    public long getTiempoUso() {
        return tiempoUso;
    }

    public void setTiempoUso(long tiempoUso) {
        this.tiempoUso = tiempoUso;
    }

    public int getDatoFecha() {
        return datoFecha;
    }

    public void setDatoFecha(int datoFecha) {
        this.datoFecha = datoFecha;
    }

    public long getlFecha() {
        return lFecha;
    }

    public void setlFecha(long lFecha) {
        this.lFecha = lFecha;
    }
}
