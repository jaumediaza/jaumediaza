package com.atendus.app.clases;

public class EstadisticaAplicacion {
    private String nombreApp;
    private double porcentajeUso;
    private double totalUsage;
    private double lastTimeUsage;
    private int color;

    public double getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(double totalUsage) {
        this.totalUsage = totalUsage;
    }

    public double getLastTimeUsage() {
        return lastTimeUsage;
    }

    public void setLastTimeUsage(double lastTimeUsage) {
        this.lastTimeUsage = lastTimeUsage;
    }

    public String getNombreApp() {
        return nombreApp;
    }

    public void setNombreApp(String nombreApp) {
        this.nombreApp = nombreApp;
    }

    public double getPorcentajeUso() {
        return porcentajeUso;
    }

    public void setPorcentajeUso(double porcentajeUso) {
        this.porcentajeUso = porcentajeUso;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
