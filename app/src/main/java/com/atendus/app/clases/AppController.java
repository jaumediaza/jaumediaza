package com.atendus.app.clases;


import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.atendus.app.login.ActivityLogin;
import com.atendus.app.login.ActivityPassword;
import com.atendus.app.login.ActivityRegistro;

import org.solovyev.android.checkout.Billing;

import javax.annotation.Nonnull;

import static com.atendus.app.ActivityPermisos.accessibilityPermission;
import static com.atendus.app.ActivityPermisos.adminPermission;
import static com.atendus.app.ActivityPermisos.contactPermission;
import static com.atendus.app.ActivityPermisos.usageStatPermission;
import static com.atendus.app.ActivityPermisos.defaultPermission;
import static com.atendus.app.ActivityPermisos.locationPermission;
import static com.atendus.app.ActivityPermisos.callPermission;
import static com.atendus.app.ActivityPermisos.notificationPermission;

public class AppController extends Application{

    Context ctx;
    boolean justCreated;

    @Nonnull
    private final Billing mBilling = new Billing(this, new Billing.DefaultConfiguration() {
        @Nonnull
        @Override
        public String getPublicKey() {
            // encrypted public key of the app. Plain version can be found in Google Play's Developer
            // Console in Service & APIs section under "YOUR LICENSE KEY FOR THIS APPLICATION" title.
            String a = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhAAo2O3aF9SKpNtoOBlaOYP0BdWUlOFIr26+FcZ9JizukD9dJ";
            String b = "PyAeO/b6Y6OvmKtH4Gl/N50iC6YhPktJW1x+MkJlQeFfjYJp5yackERHN4ftFmG2hz7yHIKTK29cuXxGA4eFfZ+6gG+rlcn54cO26Bt/yo";
            String c = "ESNswIM1ouBL/CU8NJjDUkzDEyA/6NNrb4GUeauMNSrrzy/k8tLQEWn4Yv57/E1YPzyfaIoc916EzrKx6uT+/QByIZFR4BXdxNMl2AggFh";
            String d = "CV5cgavMAOPyMxG1EUIRVM23EoL6kFUgTw73m8LHuQtEWEFSHOmwD6tRA7s7Jh4fqgp5p3N72O+FI4s3QIDAQAB";
            final String s = a + b + c + d;

            return s;
        }
    });
    @Nonnull
    public Billing getBilling() {
        return mBilling;
    }

    public static AppController get(Activity activity) {
        return (AppController) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        justCreated = false;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifecycleListener());
        ctx = this;
    }

    public class AppLifecycleListener implements LifecycleObserver {


        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        public void onMoveToForeground() {
            Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(ctx);

            SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
            if(app.isUsuarioConectado() && preferences.getInt("padre",-1)==0){
                boolean defaultP =  preferences.getBoolean("defaultPermission",false);
                boolean accessP = preferences.getBoolean("accessibilityPermission",false);
                boolean locP = preferences.getBoolean("locationPermission",false);
                boolean usageP = preferences.getBoolean("usageStatPermission",false);
                boolean adminP = preferences.getBoolean("adminPermission",false);
                boolean contactP = preferences.getBoolean("contactPermission",false);
                boolean callP = preferences.getBoolean("callPermission",false);
                if(( defaultP && accessP && locP && usageP && adminP && contactP && callP) &&  !app.isUsuarioSinRegistro()) {

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putLong("timeloaded", System.currentTimeMillis());
                        editor.commit();
                        Intent intent = new Intent(ctx, ActivityPassword.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                   }
            }
            else if(!app.isUsuarioConectado()){
                Intent i = new Intent(ctx, ActivityLogin.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
            justCreated=true;
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        public void onMoveToBackground() {
            // app moved to background
        }


    }



}