package com.atendus.app.clases;

public class DispositivosControlados {

    private int id;
    private int id_usuario;
    private int id_dispositivo;
    private String iddisp;

    public String getIddisp() {
        return iddisp;
    }

    public void setIddisp(String iddisp) {
        this.iddisp = iddisp;
    }

    private int estado;
    private boolean ultima_desconexion_bloquear_apps;
    private boolean ultima_desconexion_bloquear_llamadas;
    private int ultima_desconexion_hh;
    private int ultima_desconexion_mm;
    private String ultima_desconexion_lista_apps;
    private String ultima_desconexion_lista_contactos;
    private String etiqueta;

    private double latitud;
    private double longitud;
    private String nombrehijo;
    private int sexohijo;
    private int edadhijo;

    private int estadoDesconexion;

    public DispositivosControlados() {
    }

    public String getNombrehijo() {
        return nombrehijo;
    }

    public void setNombrehijo(String nombrehijo) {
        this.nombrehijo = nombrehijo;
    }

    public int getSexohijo() {
        return sexohijo;
    }

    public void setSexohijo(int sexohijo) {
        this.sexohijo = sexohijo;
    }

    public int getEdadhijo() {
        return edadhijo;
    }

    public void setEdadhijo(int edadhijo) {
        this.edadhijo = edadhijo;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_dispositivo() {
        return id_dispositivo;
    }

    public void setId_dispositivo(int id_dispositivo) {
        this.id_dispositivo = id_dispositivo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public boolean isUltima_desconexion_bloquear_apps() {
        return ultima_desconexion_bloquear_apps;
    }

    public void setUltima_desconexion_bloquear_apps(boolean ultima_desconexion_bloquear_apps) {
        this.ultima_desconexion_bloquear_apps = ultima_desconexion_bloquear_apps;
    }

    public boolean isUltima_desconexion_bloquear_llamadas() {
        return ultima_desconexion_bloquear_llamadas;
    }

    public void setUltima_desconexion_bloquear_llamadas(boolean ultima_desconexion_bloquear_llamadas) {
        this.ultima_desconexion_bloquear_llamadas = ultima_desconexion_bloquear_llamadas;
    }

    public int getUltima_desconexion_hh() {
        return ultima_desconexion_hh;
    }

    public void setUltima_desconexion_hh(int ultima_desconexion_hh) {
        this.ultima_desconexion_hh = ultima_desconexion_hh;
    }

    public int getUltima_desconexion_mm() {
        return ultima_desconexion_mm;
    }

    public void setUltima_desconexion_mm(int ultima_desconexion_mm) {
        this.ultima_desconexion_mm = ultima_desconexion_mm;
    }

    public String getUltima_desconexion_lista_apps() {
        return ultima_desconexion_lista_apps;
    }

    public void setUltima_desconexion_lista_apps(String ultima_desconexion_lista_apps) {
        this.ultima_desconexion_lista_apps = ultima_desconexion_lista_apps;
    }

    public String getUltima_desconexion_lista_contactos() {
        return ultima_desconexion_lista_contactos;
    }

    public void setUltima_desconexion_lista_contactos(String ultima_desconexion_lista_contactos) {
        this.ultima_desconexion_lista_contactos = ultima_desconexion_lista_contactos;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

       public int getEstadoDesconexion() {
        return estadoDesconexion;
    }

    public void setEstadoDesconexion(int estadoDesconexion) {
        this.estadoDesconexion = estadoDesconexion;
    }


}
