package com.atendus.app.clases;


import java.util.Date;

/**
 * Created by Usuario on 27/09/2017.
 */

public class Usuario {
    private int id=-1;
    private String password="";
    private String nombre="";
    private String email="";
    private String codigo_pin="";
    private String token="";
    private int plan = -1;   //0 trial 1 free 2 premium
    private Date registerDate;
    private String nombrehijo;
    private int sexo = -1; //0 chico 1 chica -1 padre
    private int edadhijo;
    private int isPadre = -1; // -1 not set 0 hijo 1 padre

    private double latitud;
    private double longitud;

    public String getCodigo_pin() {
        return codigo_pin;
    }

    public void setCodigo_pin(String codigo_pin) {
        this.codigo_pin = codigo_pin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getPlan() {
        return plan;
    }

    public void setPlan(int plan) {
        this.plan = plan;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getNombrehijo() {
        return nombrehijo;
    }

    public void setNombrehijo(String nombrehijo) {
        this.nombrehijo = nombrehijo;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public int getEdadhijo() {
        return edadhijo;
    }

    public void setEdadhijo(int edadhijo) {
        this.edadhijo = edadhijo;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getIsPadre() {
        return isPadre;
    }

    public void setIsPadre(int isPadre) {
        this.isPadre = isPadre;
    }
}
