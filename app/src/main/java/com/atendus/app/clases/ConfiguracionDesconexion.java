package com.atendus.app.clases;

import android.content.Context;
import android.media.AudioManager;

import java.util.Calendar;

/**
 * Created by Alberto on 18/06/2018.
 */

public class ConfiguracionDesconexion {
    private boolean bloquearLlamadas=false;//Si acepta o no llamadas en la desconexión
    private boolean bloquearNotificaciones=false;//Si acepta o no notificaciones en la desconexión
    private int horasDesconexion=0;//El tiempo en horas establecido para esta desconexión
    private int minutosDesconexion=0;//El tiempo en minutos  establecido para esta desconexión
    private int segundosDesconexion=0;//El tiempo en minutos  establecido para esta desconexión
    private int tiempoDesconexion = 0; //Tiempo de desconexión establecido para esta desconexión

    //private int horasDesconexionActual=0;//El tiempo en horas que lleva desconectado
    //private int minutosDesconexionActual=0;//El tiempo en minutos que lleva desconectado
    //private int tiempoDesconexionActual = 0; //Tiempo de desconexión que lleva actualmente

    private int horasDesconexion_original =0;//El tiempo en horas establecido para esta desconexión
    private int minutosDesconexion_original =0;//El tiempo en minutos  establecido para esta desconexión
    private int segundosDesconexion_original =0;//El tiempo en minutos  establecido para esta desconexión
    private int tiempoDesconexion_original = 0; //Tiempo de desconexión establecido para esta desconexión

    private int audioStatus = AudioManager.RINGER_MODE_NORMAL; //Modo de audio previo a la desconexión

    private long lDateStart;

    public ConfiguracionDesconexion(){

    }

    public void setlDateStart() {
        lDateStart = Calendar.getInstance().getTime().getTime();
    }

    public long getlDate() {
        return lDateStart;
    }

    public boolean isBloquearLlamadas() {
        return bloquearLlamadas;
    }

    public void setBloquearLlamadas(boolean bloquearLlamadas) {
        this.bloquearLlamadas = bloquearLlamadas;
    }

    public boolean isBloquearNotificaciones() {
        return bloquearNotificaciones;
    }

    public void setBloquearNotificaciones(boolean bloquearNotificaciones) {
        this.bloquearNotificaciones = bloquearNotificaciones;
    }

    public int getHorasDesconexion() {
        return horasDesconexion;
    }


    public int getMinutosDesconexion() {
        return minutosDesconexion;
    }

    public int getSegundosDesconexion() {
        return segundosDesconexion;
    }

    public int getHorasDesconexion_original() {
        return horasDesconexion_original;
    }

    public int getMinutosDesconexion_original() {
        return minutosDesconexion_original;
    }

    public int getSegundosDesconexion_original() {
        return segundosDesconexion_original;
    }

    public int getTiempoDesconexion_original() {
        return tiempoDesconexion_original;
    }

    public int getTiempoDesconexion() {
        return tiempoDesconexion;
    }


    public void setTiempoDesconexion(int tiempoDesconexion, int hh, int mm){
        this.tiempoDesconexion = tiempoDesconexion;
        horasDesconexion = hh;
        minutosDesconexion = mm;
        segundosDesconexion = 0;
    }

    public void setTiempoDesconexion(int tiempoDesconexion, int hh, int mm, int ss){
        this.tiempoDesconexion = tiempoDesconexion;
        horasDesconexion = hh;
        minutosDesconexion = mm;
        segundosDesconexion = ss;
    }

    public void guardarTiempoOriginal(){
        tiempoDesconexion_original = tiempoDesconexion;
        horasDesconexion_original = horasDesconexion;
        minutosDesconexion_original = minutosDesconexion;
        segundosDesconexion_original = segundosDesconexion;
    }

    public void restaurarTiempoOriginal(){
         tiempoDesconexion = tiempoDesconexion_original;
         horasDesconexion = horasDesconexion_original;
         minutosDesconexion = minutosDesconexion_original;
         segundosDesconexion = segundosDesconexion_original;
    }

    public void setAudioStatus(Context c){
        AudioManager am = (AudioManager) c.getSystemService(Context.AUDIO_SERVICE);
        audioStatus = am.getRingerMode();
    }

    public void restoreAudioStatus(Context c){
        AudioManager am = (AudioManager) c.getSystemService(Context.AUDIO_SERVICE);
        am.setRingerMode(audioStatus);
    }
}
