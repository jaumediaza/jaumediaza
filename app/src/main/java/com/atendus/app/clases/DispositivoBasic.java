package com.atendus.app.clases;

public class DispositivoBasic {
    private int id;
    private String idDispositivo;
    private String nombre;
    private String nombrehijo;
    private int sexohijo;
    private int edadhijo;
    private int isPadre = -1;

    public String getNombrehijo() {
        return nombrehijo;
    }

    public void setNombrehijo(String nombrehijo) {
        this.nombrehijo = nombrehijo;
    }

    public int getSexohijo() {
        return sexohijo;
    }

    public void setSexohijo(int sexohijo) {
        this.sexohijo = sexohijo;
    }

    public int getEdadhijo() {
        return edadhijo;
    }

    public void setEdadhijo(int edadhijo) {
        this.edadhijo = edadhijo;
    }

    public int getIsPadre() {
        return isPadre;
    }

    public void setIsPadre(int isPadre) {
        this.isPadre = isPadre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(String idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
