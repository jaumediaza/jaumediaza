package com.atendus.app.clases;

import java.util.List;

public class EstadisticasUsuarioRemoto {

    private int desbloqueosDia, desbloqueosSemana, desbloqueosMes;
    private long tiempoUsoDia,tiempoUsoSemana, tiempoUsoMes;
    private long lastDia,lastSemana, lastMes;

    private List<EstadisticaAplicacion> estadisticasDia;
    private List<EstadisticaAplicacion> estadisticasSemana;
    private List<EstadisticaAplicacion> estadisticasMes;

    public long getLastDia() {
        return lastDia;
    }

    public void setLastDia(long lastDia) {
        this.lastDia = lastDia;
    }

    public long getLastSemana() {
        return lastSemana;
    }

    public void setLastSemana(long lastSemana) {
        this.lastSemana = lastSemana;
    }

    public long getLastMes() {
        return lastMes;
    }

    public void setLastMes(long lastMes) {
        this.lastMes = lastMes;
    }

    public int getDesbloqueosDia() {
        return desbloqueosDia;
    }

    public void setDesbloqueosDia(int desbloqueosDia) {
        this.desbloqueosDia = desbloqueosDia;
    }

    public int getDesbloqueosSemana() {
        return desbloqueosSemana;
    }

    public void setDesbloqueosSemana(int desbloqueosSemana) {
        this.desbloqueosSemana = desbloqueosSemana;
    }

    public int getDesbloqueosMes() {
        return desbloqueosMes;
    }

    public void setDesbloqueosMes(int desbloqueosMes) {
        this.desbloqueosMes = desbloqueosMes;
    }

    public long getTiempoUsoDia() {
        return tiempoUsoDia;
    }

    public void setTiempoUsoDia(long tiempoUsoDia) {
        this.tiempoUsoDia = tiempoUsoDia;
    }

    public long getTiempoUsoSemana() {
        return tiempoUsoSemana;
    }

    public void setTiempoUsoSemana(long tiempoUsoSemana) {
        this.tiempoUsoSemana = tiempoUsoSemana;
    }

    public long getTiempoUsoMes() {
        return tiempoUsoMes;
    }

    public void setTiempoUsoMes(long tiempoUsoMes) {
        this.tiempoUsoMes = tiempoUsoMes;
    }

    public List<EstadisticaAplicacion> getEstadisticasDia() {
        return estadisticasDia;
    }

    public void setEstadisticasDia(List<EstadisticaAplicacion> estadisticasDia) {
        this.estadisticasDia = estadisticasDia;
    }

    public List<EstadisticaAplicacion> getEstadisticasSemana() {
        return estadisticasSemana;
    }

    public void setEstadisticasSemana(List<EstadisticaAplicacion> estadisticasSemana) {
        this.estadisticasSemana = estadisticasSemana;
    }

    public List<EstadisticaAplicacion> getEstadisticasMes() {
        return estadisticasMes;
    }

    public void setEstadisticasMes(List<EstadisticaAplicacion> estadisticasMes) {
        this.estadisticasMes = estadisticasMes;
    }
}
