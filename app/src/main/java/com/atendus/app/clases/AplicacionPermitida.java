package com.atendus.app.clases;

import android.graphics.drawable.Drawable;

public class AplicacionPermitida {
    private String packageName;
    private String appLabel;
    private boolean permitida;
    private boolean usoDuranteDesconecta;
    private boolean seguimientoDeUso;
    private transient Drawable drawable;
    private int id;

    public AplicacionPermitida(String appInfo, boolean permitida, boolean usoDuranteDesconecta,
                               boolean seguimientoDeUso) {
        this.appLabel = appInfo;
        this.permitida = permitida;
        this.usoDuranteDesconecta = usoDuranteDesconecta;
        this.seguimientoDeUso = seguimientoDeUso;
    }
    public AplicacionPermitida(){}

    public String getAppLabel() {
        return appLabel;
    }

    public void setAppLabel(String appLabel) {
        this.appLabel = appLabel;
    }

    public boolean isPermitida() {
        return permitida;
    }

    public void setPermitida(boolean permitida) {
        this.permitida = permitida;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isUsoDuranteDesconecta() {
        return usoDuranteDesconecta;
    }

    public void setUsoDuranteDesconecta(boolean usoDuranteDesconecta) {
        this.usoDuranteDesconecta = usoDuranteDesconecta;
    }

    public boolean isSeguimientoDeUso() {
        return seguimientoDeUso;
    }

    public void setSeguimientoDeUso(boolean seguimientoDeUso) {
        this.seguimientoDeUso = seguimientoDeUso;
    }
}
