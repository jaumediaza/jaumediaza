package com.atendus.app.clases;

public class Telefono {
    private String nombre;
    private char letra;
    private String numero;
    private boolean permitido;
    private int id;

    public Telefono(String nombre, String numero, boolean permitido) {
        this.nombre = nombre;
        this.numero = numero;
        this.permitido = permitido;

        letra = nombre.charAt(0);
    }

    public Telefono()
    {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getLetra() {
        return letra;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isPermitido() {
        return permitido;
    }

    public void setPermitido(boolean permitido) {
        this.permitido = permitido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }
}
