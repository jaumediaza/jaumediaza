package com.atendus.app.json;


import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONPremium {
	private Context activity;
	private JSONObject jObject;
	int resultado;

	private Usuario usr=new Usuario();


	public JSONPremium(Context a){
		activity = a;
	}

	public int run(int id_usuario, int plan) throws JSONException{

		return loadJSON(id_usuario, plan);
	}


	private int loadJSON(int id_usuario, int plan) throws JSONException{

//String ruta = activity.getResources().getString(R.string.url_webservices)+"stripe/stripetest.php?premium="+plan;
		String ruta = activity.getResources().getString(R.string.url_webservices)
				+"userdata/edit_premium.php?id="+id_usuario+"&premium="+plan;

		jObject = JSONManager.getJSONfromURL(ruta);
		if(jObject != null){
			return parseJSON(jObject);
		}
		else
		{
			return -1;
		}

	}


	private int parseJSON(JSONObject resultadoJSON) throws JSONException{
		int res = -1;
		if(!resultadoJSON.isNull("resultCode")){
			res = resultadoJSON.getInt("resultCode");
		}


		return res;

	}



}
