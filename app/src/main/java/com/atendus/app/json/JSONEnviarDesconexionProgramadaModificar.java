package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.DesconexionProgramada;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class JSONEnviarDesconexionProgramadaModificar {
    private Context context;
    private JSONObject jObject;


    public JSONEnviarDesconexionProgramadaModificar(Context c){
        context = c;
    }

    public int run(final int id_usuario, String imei, DesconexionProgramada desc) throws JSONException {

        return loadJSON(id_usuario, imei, desc);
    }


    private int loadJSON(int id_usuario, String imei, DesconexionProgramada desc) throws JSONException{
        int activa = 0;
        if (desc.isEnabled())
            activa = 1;
        String titulo = "";
        try {
            titulo = URLEncoder.encode(desc.getStrTitulo(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        int dias[] = {0,0,0,0,0,0,0};
        for (int i = 0; i < 7; ++i)
            if (desc.getDias()[i])
                dias[i] = 1;

        int notif = 0;
        if (desc.isBloquearNotificaciones())
            notif = 1;

        int llamd = 0;
        if (desc.isBloquearLlamadas())
            llamd = 1;

        String ruta="disconnections/scheduled/update_scheduled_disconnection.php?" +
                "id="+id_usuario+
                "&imei="+imei+
                "&hh_start="+ desc.getHoraInicio()+
                "&mm_start="+ desc.getMinutoInicio()+
                "&hh_end="+ desc.getHoraFin()+
                "&mm_end="+ desc.getMinutoFin()+
                "&enabled="+ activa +
                "&title="+ titulo+
                "&repeat="+ (desc.getRepetir()-1)+
                "&monday="+  dias[0] +
                "&tuesday="+ dias[1] +
                "&wednesday="+ dias[2] +
                "&thursday="+ dias[3] +
                "&friday="+ dias[4] +
                "&saturday="+ dias[5] +
                "&sunday=" + dias[6] +
                "&notifAllowed=" + notif +
                "&callsAllowed=" + llamd +
                //TODO 3A hacer esto por post
                "&pckAllowed=" + desc.getAplicacionesPermitidas() +
                "disconnection_id"+desc.getId() +
                "&phonesAllowed=" + desc.getTfnosPermitidos();




        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return -1;
        }

    }


    private int parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode");

    }



}
