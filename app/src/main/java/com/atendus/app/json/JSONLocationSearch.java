package com.atendus.app.json;

import android.content.Context;
import android.location.Location;

import com.atendus.app.R;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONLocationSearch {
    private Context activity;

	public JSONLocationSearch(Context a) {
        activity = a;
    }

    public Location run(int id_usuario, String iddisp) throws JSONException {
        return loadJSON(id_usuario, iddisp);
    }

    private Location loadJSON(int id_usuario, String iddisp) throws JSONException {
        String path = activity.getResources().getString(R.string.url_webservices) + "userdata/query_location.php?id=" + id_usuario + "&iddisp=" + iddisp;
		JSONObject jObject = JSONManager.getJSONfromURL(path);
        if (jObject != null) {
            return parseJSON(jObject);
        } else {
            return null;
        }
    }

    private Location parseJSON(JSONObject json) throws JSONException {
        Location loc = null;
        if (!json.isNull("latitud")) {
            if (json.getLong("latitud") != -1) {
                loc = new Location("");
                loc.setLatitude(json.getDouble("latitud"));
                loc.setLongitude(json.getDouble("longitud"));
            }
        }

        return loc;
    }
}