package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.AplicacionPermitida;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alberto on 13/06/2018.
 */
public class JSONDispositivosControladosApps {
    private Context activity;

    public JSONDispositivosControladosApps(Context a) {
        activity = a;
    }

    public List<AplicacionPermitida> run(final int userId, String deviceId) throws JSONException {
        return loadJSON(userId, deviceId);
    }

    private List<AplicacionPermitida> loadJSON(int userId, String deviceId) throws JSONException {
        JSONObject jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices) + "userdata/query_applist.php?id=" + userId + "&device_id=" + deviceId);
        if (jObject != null) {
            return parseJSON(jObject);
        } else {
            return null;
        }
    }

    private List<AplicacionPermitida> parseJSON(JSONObject json) throws JSONException {
        List<AplicacionPermitida> apps = new ArrayList<>();
        int i = 0;

        if (json.getInt("resultCode") == 0) return null;

        JSONArray jArray = json.getJSONArray("apps");
        while (jArray.length() > i) {
            AplicacionPermitida app = new AplicacionPermitida();

            if (!jArray.getJSONObject(i).isNull("id"))
                app.setId(jArray.getJSONObject(i).getInt("id"));

            try {
                if (!jArray.getJSONObject(i).isNull("label"))
                    app.setAppLabel(jArray.getJSONObject(i).getString("label"));
            } catch (Exception e) {
                app.setAppLabel(jArray.getJSONObject(i).getString("label"));
            }
            try {
                if (!jArray.getJSONObject(i).isNull("package"))
                    app.setPackageName(jArray.getJSONObject(i).getString("package"));
            } catch (Exception e) {
                app.setPackageName(jArray.getJSONObject(i).getString("package"));
            }

            app.setPermitida(false);
            app.setUsoDuranteDesconecta(false);

            apps.add(app);
            i++;
        }

        return apps;
    }
}
