package com.atendus.app.json;


import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class JSONChild {
	private Context activity;
	private JSONObject jObject;
	int resultado;

	private Usuario usr=new Usuario();


	public JSONChild(Context a){
		activity = a;
	}

	public int run(int id_usuario, String iddisp, int isPadre, String nombrehijo, int sexohijo, int edadhijo) throws JSONException{

		return loadJSON(id_usuario, iddisp, isPadre, nombrehijo, sexohijo, edadhijo);
	}


	private int loadJSON(int id_usuario, String iddisp, int isPadre, String nombrehijo, int sexohijo, int edadhijo) throws JSONException{

		try {
			if(nombrehijo!=null)
			nombrehijo = URLEncoder.encode(nombrehijo, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		String ruta = activity.getResources().getString(R.string.url_webservices)
				+"userdata/edit_child.php?id="+id_usuario+"&iddisp="+iddisp+"&isPadre="+isPadre+"&nombrehijo="+nombrehijo+"&sexohijo="+sexohijo+"&edadhijo="+edadhijo;

		jObject = JSONManager.getJSONfromURL(ruta);
		if(jObject != null){
			return parseJSON(jObject);
		}
		else
		{
			return -1;
		}

	}


	private int parseJSON(JSONObject resultadoJSON) throws JSONException{
		int res = -1;
		if(!resultadoJSON.isNull("resultCode")){
			res = resultadoJSON.getInt("resultCode");
		}


		return res;

	}



}
