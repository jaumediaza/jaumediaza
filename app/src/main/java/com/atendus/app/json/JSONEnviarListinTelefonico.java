package com.atendus.app.json;


import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class JSONEnviarListinTelefonico {
	private Context activity;
	private JSONObject jObject;

	private Usuario usr=new Usuario();


	public JSONEnviarListinTelefonico(Context a){
		activity = a;
	}
	
	public int run(int id_usuario, String id_dispositivo, String nombres, String numeros) throws JSONException{

		return loadJSON(id_usuario, id_dispositivo, nombres, numeros);
	}
		
		
	private int loadJSON(int id_usuario, String id_dispositivo, String nombres, String numeros) throws JSONException{

        try {
        	if(id_dispositivo!=null) {
				id_dispositivo = URLEncoder.encode(id_dispositivo, "UTF-8");
				String ruta = activity.getResources().getString(R.string.url_webservices)
						+"userdata/send_phone_list.php?id="+id_usuario+"&device_id="+id_dispositivo;

				try {
					jObject = JSONManager.getJSONfromURLPost(ruta, nombres, numeros);
				}
				catch(Exception e){
					jObject = null;
				}
				if(jObject != null){
					return parseJSON(jObject);
				}
				else
				{
					return -1;
				}
			}
        	else{
        		return -1;
			}
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
			return -1;
        }



	}
		

	private int parseJSON(JSONObject resultadoJSON) throws JSONException{
		int res = -1;
		if(!resultadoJSON.isNull("resultCode")){
			res = resultadoJSON.getInt("resultCode");
		}


		return res;

	}



}
