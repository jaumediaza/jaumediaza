package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONEnviarTokenPush{
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONEnviarTokenPush(Context c){
        context = c;
    }

    public boolean run(final int id_usuario,final String token, String oldtoken, String imei) throws JSONException {

        return loadJSON(id_usuario,token, oldtoken, imei);
    }


    private boolean loadJSON(int id_usuario,String token, String oldtoken, String imei) throws JSONException{

        String ruta="userdata/send_token.php?id="+id_usuario+"&token="+token+"&oldtoken="+oldtoken+"&imei="+imei;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}

