package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONLocation {
    private Context activity;

    public JSONLocation(Context a) {
        activity = a;
    }

    public int run(int id_usuario, String iddisp, double latitud, double longitud) throws JSONException {
        return loadJSON(id_usuario, iddisp, latitud, longitud);
    }

    private int loadJSON(int id_usuario, String iddisp, double latitud, double longitud) throws JSONException {
        String url = activity.getResources().getString(R.string.url_webservices)
                + "userdata/edit_location.php?id=" + id_usuario + "&iddisp=" + iddisp + "&latitud=" + latitud + "&longitud=" + longitud;

        JSONObject jObject = JSONManager.getJSONfromURL(url);
        if (jObject != null) {
            return parseJSON(jObject);
        } else {
            return -1;
        }
    }

    private int parseJSON(JSONObject json) throws JSONException {
        int res = -1;
        if (!json.isNull("resultCode")) {
            res = json.getInt("resultCode");
        }
        return res;
    }
}