package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.ActivityVinculacionesPendientes;
import com.atendus.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONVinculacionesPendientes {
    private Context activity;
    private JSONObject jObject;


    public JSONVinculacionesPendientes(Context a) {
        activity = a;
    }

    public List<ActivityVinculacionesPendientes.NuevaVinculacion> run(final int id_usuario, String imei) throws JSONException {

        return loadJSON(id_usuario, imei);
    }


    private List<ActivityVinculacionesPendientes.NuevaVinculacion> loadJSON(int id_usuario, String imei) throws JSONException {
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                + "link/query_pending_links.php?id=" + id_usuario + "&imei=" + imei);
        if (jObject != null) {
            return parseJSON(jObject);
        } else {
            return null;
        }
    }

    private List<ActivityVinculacionesPendientes.NuevaVinculacion> parseJSON(JSONObject resultadoJSON) throws JSONException {
        if (resultadoJSON.getInt("resultCode") == 0) {
            return null;
        }

        JSONArray jArray = resultadoJSON.getJSONArray("link");

        List<ActivityVinculacionesPendientes.NuevaVinculacion> v = new ArrayList<>();
        int i = 0;
        while (jArray.length() > i) {
            ActivityVinculacionesPendientes.NuevaVinculacion resultado = new ActivityVinculacionesPendientes.NuevaVinculacion();


            if (!jArray.getJSONObject(i).isNull("id"))
                resultado.id = jArray.getJSONObject(i).getInt("id");

            if (!jArray.getJSONObject(i).isNull("controller_id"))
                resultado.idUsuarioControlador = jArray.getJSONObject(i).getInt("controller_id");

            if (!jArray.getJSONObject(i).isNull("device_id"))
                resultado.idDispositivo = jArray.getJSONObject(i).getInt("device_id");

            if (!jArray.getJSONObject(i).isNull("user_id"))
                resultado.idUsuario = jArray.getJSONObject(i).getInt("user_id");

            if (!jArray.getJSONObject(i).isNull("status"))
                resultado.estado = jArray.getJSONObject(i).getInt("status");

            if (!jArray.getJSONObject(i).isNull("controller_name"))
                resultado.nombreControlador = jArray.getJSONObject(i).getString("controller_name");

            v.add(resultado);
            i++;
        }

        return v;

    }

}
