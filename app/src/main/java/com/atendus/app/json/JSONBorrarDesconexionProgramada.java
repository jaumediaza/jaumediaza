package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONBorrarDesconexionProgramada {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONBorrarDesconexionProgramada(Context c){
        context = c;
    }

    public boolean run(final int id_usuario, String imei, int id_desconexion) throws JSONException {

        return loadJSON(id_usuario, imei, id_desconexion);
    }


    private boolean loadJSON(int id_usuario, String imei, int id_desconexion) throws JSONException{

        String ruta="disconnections/scheduled/delete_device_disconnection.php?id="+id_usuario+"&imei="
                +imei+"&disconnection_id="+id_desconexion;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
