package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.DispositivosControlados;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONBorrarVinculacionDisposiivo {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONBorrarVinculacionDisposiivo(Context c){
        context = c;
    }

    public boolean run(DispositivosControlados disp, int idControlador) throws JSONException {

        return loadJSON(disp, idControlador);
    }


    private boolean loadJSON(DispositivosControlados disp, int idControlador) throws JSONException{

        String ruta="remote/delete_linked_device.php?controller_id="+idControlador+"&id="
                +disp.getId()+"&device_id="+disp.getId_dispositivo();

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
