package com.atendus.app.json;


import android.app.Activity;

import com.atendus.app.R;
import com.atendus.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;



public class JSONVincularDispositivo {
	private Activity activity;
	private JSONObject jObject;

	private Usuario usr=new Usuario();


	public JSONVincularDispositivo(Activity a){
		activity = a;
	}

	public int run(int id_usuario, String id_dispositivo, String nombre_dispositivo) throws JSONException{

		return loadJSON(id_usuario, id_dispositivo, nombre_dispositivo);
	}


	private int loadJSON(int id_usuario, String id_dispositivo, String nombre_dispositivo) throws JSONException{

		try {
			id_dispositivo = URLEncoder.encode(id_dispositivo, "UTF-8");
			nombre_dispositivo = URLEncoder.encode(nombre_dispositivo, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			id_dispositivo = "0000000";
			nombre_dispositivo = "0000000";
		}

		String ruta = activity.getResources().getString(R.string.url_webservices)
				+"userdata/save_device.php?id="+id_usuario+"&device_id="+id_dispositivo+"&device_name="+nombre_dispositivo;
		jObject = JSONManager.getJSONfromURL(ruta);
		if(jObject != null){
			return parseJSON(jObject);
		}
		else
		{
			return -1;
		}

	}


	private int parseJSON(JSONObject resultadoJSON) throws JSONException{
		int res = -1;
		if(!resultadoJSON.isNull("resultCode")){
			res = resultadoJSON.getInt("resultCode");
		}


		return res;

	}



}

