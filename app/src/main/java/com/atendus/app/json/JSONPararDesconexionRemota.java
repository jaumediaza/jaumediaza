package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.DispositivosControlados;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONPararDesconexionRemota {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONPararDesconexionRemota(Context c){
        context = c;
    }

    public boolean run(DispositivosControlados dispositivo) throws JSONException {

        return loadJSON(dispositivo);
    }


    private boolean loadJSON(DispositivosControlados dispositivo) throws JSONException{

        String ruta="cancelar_desconexion.php?" +
                "id_usuario="+dispositivo.getId_usuario()+
                "&id_dispositivo="+dispositivo.getId_dispositivo();

        JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);

        String ruta2 = "comprobar_estado.php?" +
                "id_usuario="+dispositivo.getId_usuario()+
                "&id_dispositivo="+dispositivo.getId_dispositivo();
        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta2);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
