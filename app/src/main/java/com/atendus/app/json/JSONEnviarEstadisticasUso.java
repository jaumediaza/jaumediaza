package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.EstadisticaUso;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONEnviarEstadisticasUso {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONEnviarEstadisticasUso(Context c){
        context = c;
    }

    public boolean run(final int id_usuario, String imei, EstadisticaUso dia, EstadisticaUso semana
        , EstadisticaUso mes) throws JSONException {

        return loadJSON(id_usuario, imei, dia, semana, mes);
    }


    private boolean loadJSON(int id_usuario, String imei,  EstadisticaUso dia,  EstadisticaUso
            semana, EstadisticaUso mes) throws JSONException{

        String ruta="userstats/update_usage_statistics.php?id=" +id_usuario+
                "&imei="+imei
                +"&unlocks_day="+dia.getDesbloqueos()
                +"&usage_day="+dia.getTiempoUso()
                +"&unlocks_week="+semana.getDesbloqueos()
                +"&usage_week="+semana.getTiempoUso()
                +"&unlocks_month="+mes.getDesbloqueos()
                +"&usage_month="+mes.getTiempoUso()
                ;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
