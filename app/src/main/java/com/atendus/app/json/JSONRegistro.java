package com.atendus.app.json;

import android.app.Activity;

import com.atendus.app.R;
import com.atendus.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Usuario on 28/09/2017.
 */

public class JSONRegistro {
    private Activity activity;
    private JSONObject jObject;

    int resultado;//-1 -> Correo en uso 0 -> Error 1-> Registro satisfactorio

    public JSONRegistro(Activity a){
        activity = a;
    }

    public int run(final Usuario usuario) throws JSONException {

        return loadJSON(usuario);
    }


    private int loadJSON(Usuario usuario) throws JSONException{

        String ruta="authentication/register.php?name="+usuario.getNombre()+"&password="+usuario.getPassword()+"&mail="+usuario.getEmail();

        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }
    }


    private int parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode");
    }



}
