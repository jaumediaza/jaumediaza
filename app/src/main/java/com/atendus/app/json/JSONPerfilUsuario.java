package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.ResultadoPerfilUsuario;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONPerfilUsuario {
    private Context activity;
    private JSONObject jObject;

    private ResultadoPerfilUsuario resultado = new ResultadoPerfilUsuario();


    public JSONPerfilUsuario(Context a) {
        activity = a;
    }

    public ResultadoPerfilUsuario run(final int id_usuario) throws JSONException {

        return loadJSON(id_usuario);
    }


    private ResultadoPerfilUsuario loadJSON(int id_usuario) throws JSONException {
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                + "userdata/user_profile.php?id=" + id_usuario);
        if (jObject != null) {
            return parseJSON(jObject);
        } else {
            return resultado;
        }

    }


    private ResultadoPerfilUsuario parseJSON(JSONObject resultadoJSON) throws JSONException {
        if (resultadoJSON.getInt("resultCode") == 0) {
            return resultado;
        }

        resultado.setResultado(1);

        JSONObject jsonUser = resultadoJSON.getJSONObject("user");

        if (!jsonUser.isNull("id"))
            resultado.getUsuario().setId(jsonUser.getInt("id"));

        if (!jsonUser.isNull("name"))
            resultado.getUsuario().setNombre(jsonUser.getString("name"));

        if (!jsonUser.isNull("mail"))
            resultado.getUsuario().setEmail(jsonUser.getString("mail"));

        if (!jsonUser.isNull("pinCode"))
            resultado.getUsuario().setCodigo_pin(jsonUser.getString("pinCode"));


        return resultado;

    }

}
