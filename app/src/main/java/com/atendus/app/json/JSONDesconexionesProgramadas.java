package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.DesconexionProgramada;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONDesconexionesProgramadas {

    private Context context;

    public JSONDesconexionesProgramadas(Context a) {
        context = a;
    }

    public DesconexionProgramada.DesconexionProgramadaList run(final int userId, String imei) throws JSONException {
        return loadJSON(userId, imei);
    }

    private DesconexionProgramada.DesconexionProgramadaList loadJSON(int userId, String imei) throws JSONException {
        JSONObject jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                + "disconnections/scheduled/query_list.php?id=" + userId + "&imei=" + imei);
        if (jObject != null) {
            return parseJSON(jObject);
        }
        return null;
    }

    private DesconexionProgramada.DesconexionProgramadaList parseJSON(JSONObject json) throws JSONException {
        if (json.getInt("resultCode") == 0) {
            return null;
        }

        JSONArray jArray = json.getJSONArray("disconnection");

        DesconexionProgramada.DesconexionProgramadaList desc = new DesconexionProgramada.DesconexionProgramadaList();
        DesconexionProgramada result = new DesconexionProgramada();
        int i = 0;
        while (jArray.length() > i) {

            if (!jArray.getJSONObject(i).isNull("id"))
                result.setId(jArray.getJSONObject(i).getInt("id"));

            result.setBloquearNotificaciones(false);
            if (!jArray.getJSONObject(i).isNull("notif_allowed"))
                if (jArray.getJSONObject(i).getInt("notif_allowed") == 1)
                    result.setBloquearNotificaciones(true);

            result.setBloquearLlamadas(false);
            if (!jArray.getJSONObject(i).isNull("calls_allowed"))
                if (jArray.getJSONObject(i).getInt("calls_allowed") == 1)
                    result.setBloquearLlamadas(true);

            if (!jArray.getJSONObject(i).isNull("hh_init"))
                result.setHoraInicio(jArray.getJSONObject(i).getInt("hh_init"));

            if (!jArray.getJSONObject(i).isNull("mm_init"))
                result.setMinutoInicio(jArray.getJSONObject(i).getInt("mm_init"));

            if (!jArray.getJSONObject(i).isNull("hh_end"))
                result.setHoraFin(jArray.getJSONObject(i).getInt("hh_end"));

            if (!jArray.getJSONObject(i).isNull("mm_end"))
                result.setMinutoFin(jArray.getJSONObject(i).getInt("mm_end"));

            result.setEnabled(false);
            if (!jArray.getJSONObject(i).isNull("enabled"))
                if (jArray.getJSONObject(i).getInt("enabled") == 1)
                    result.setEnabled(true);

            if (!jArray.getJSONObject(i).isNull("title"))
                result.setStrTitulo(jArray.getJSONObject(i).getString("title"));

            if (!jArray.getJSONObject(i).isNull("repeat"))
                result.setRepetir(jArray.getJSONObject(i).getInt("repeat") + 1);

            boolean[] days = {false, false, false, false, false, false, false};

            if (!jArray.getJSONObject(i).isNull("monday"))
                if (jArray.getJSONObject(i).getInt("monday") == 1)
                    days[0] = true;

            if (!jArray.getJSONObject(i).isNull("tuesday"))
                if (jArray.getJSONObject(i).getInt("tuesday") == 1)
                    days[1] = true;

            if (!jArray.getJSONObject(i).isNull("wednesday"))
                if (jArray.getJSONObject(i).getInt("wednesday") == 1)
                    days[2] = true;

            if (!jArray.getJSONObject(i).isNull("thursday"))
                if (jArray.getJSONObject(i).getInt("thursday") == 1)
                    days[3] = true;

            if (!jArray.getJSONObject(i).isNull("friday"))
                if (jArray.getJSONObject(i).getInt("friday") == 1)
                    days[4] = true;

            if (!jArray.getJSONObject(i).isNull("saturday"))
                if (jArray.getJSONObject(i).getInt("saturday") == 1)
                    days[5] = true;

            if (!jArray.getJSONObject(i).isNull("sunday"))
                if (jArray.getJSONObject(i).getInt("sunday") == 1)
                    days[6] = true;

            if (!jArray.getJSONObject(i).isNull("editable"))
                if (jArray.getJSONObject(i).getInt("editable") == 0)
                    result.setEditable(false);

            result.setDias(days);

            if (!jArray.getJSONObject(i).isNull("pckg_allowed"))
                result.setAplicacionesPermitidas(jArray.getJSONObject(i).getString("pckg_allowed"));

            if (!jArray.getJSONObject(i).isNull("phones_allowed"))
                result.setTfnosPermitidos(jArray.getJSONObject(i).getString("phones_allowed"));

            desc.getList().add(result);
            i++;
        }

        return desc;
    }
}
