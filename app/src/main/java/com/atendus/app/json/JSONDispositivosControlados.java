package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.DispositivosControlados;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONDispositivosControlados {
    private Context activity;
    private JSONObject jObject;



    public JSONDispositivosControlados(Context a){
        activity = a;
    }

    public List<DispositivosControlados> run(final int id_usuario) throws JSONException {

        return loadJSON(id_usuario);
    }


    private List<DispositivosControlados> loadJSON(int id_usuario) throws JSONException{
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +"link/query_controlled_devices.php?id="+id_usuario);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return null;
        }

    }


    private List<DispositivosControlados> parseJSON(JSONObject resultadoJSON) throws JSONException{
        if(resultadoJSON.getInt("resultCode")==0)
        {
            return null;
        }

        JSONArray jArray = resultadoJSON.getJSONArray("user");

        List <DispositivosControlados> dispositivosControlados = new ArrayList<>();
        int i = 0;
        while (jArray.length() > i) {
            DispositivosControlados dispositivo = new DispositivosControlados();


            if (!jArray.getJSONObject(i).isNull("id"))
                dispositivo.setId(jArray.getJSONObject(i).getInt("id"));

            if (!jArray.getJSONObject(i).isNull("user_id"))
                dispositivo.setId_usuario(jArray.getJSONObject(i).getInt("user_id"));

            if (!jArray.getJSONObject(i).isNull("device_id"))
                dispositivo.setIddisp(jArray.getJSONObject(i).getString("device_id"));

            if (!jArray.getJSONObject(i).isNull("status"))
                dispositivo.setEstado(jArray.getJSONObject(i).getInt("status"));

            dispositivo.setUltima_desconexion_bloquear_apps(false);
            if (!jArray.getJSONObject(i).isNull("last_disconnection_block_apps"))
                if (jArray.getJSONObject(i).getInt("last_disconnection_block_apps") == 1)
                    dispositivo.setUltima_desconexion_bloquear_apps(true);

            dispositivo.setUltima_desconexion_bloquear_llamadas(false);
            if (!jArray.getJSONObject(i).isNull("last_disconnection_block_calls"))
                if (jArray.getJSONObject(i).getInt("last_disconnection_block_calls") == 1)
                    dispositivo.setUltima_desconexion_bloquear_llamadas(true);

            if (!jArray.getJSONObject(i).isNull("last_disconnection_hh"))
                dispositivo.setUltima_desconexion_hh(jArray.getJSONObject(i).getInt("last_disconnection_hh"));

            if (!jArray.getJSONObject(i).isNull("last_diconnection_mm"))
                dispositivo.setUltima_desconexion_mm(jArray.getJSONObject(i).getInt("last_diconnection_mm"));

            if (!jArray.getJSONObject(i).isNull("label"))
                dispositivo.setEtiqueta(jArray.getJSONObject(i).getString("label"));

            if (!jArray.getJSONObject(i).isNull("nombrehijo"))
                dispositivo.setNombrehijo(jArray.getJSONObject(i).getString("nombrehijo"));

            if (!jArray.getJSONObject(i).isNull("sexohijo"))
                dispositivo.setSexohijo(jArray.getJSONObject(i).getInt("sexohijo"));

            if (!jArray.getJSONObject(i).isNull("latitud"))
                dispositivo.setLatitud(jArray.getJSONObject(i).getDouble("latitud"));

            if (!jArray.getJSONObject(i).isNull("longitud"))
                dispositivo.setLongitud(jArray.getJSONObject(i).getDouble("longitud"));

            try {
                if (!jArray.getJSONObject(i).isNull("last_disconnection_applist"))
                    dispositivo.setUltima_desconexion_lista_apps(jArray.getJSONObject(i).getString("last_disconnection_applist"));

                if (!jArray.getJSONObject(i).isNull("last_disconnection_phone_list"))
                    dispositivo.setUltima_desconexion_lista_contactos(jArray.getJSONObject(i).getString("last_disconnection_phone_list"));
            }
            catch(Exception e){

            }

            if (!jArray.getJSONObject(i).isNull("disconnection_status"))
                dispositivo.setEstadoDesconexion(jArray.getJSONObject(i).getInt("disconnection_status"));


                dispositivosControlados.add(dispositivo);
            i++;
        }

        return dispositivosControlados;

    }

}
