package com.atendus.app.json;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Usuario on 11/04/2017.
 */

public class JSONManager extends JSONObject {


    public static JSONObject getJSONfromURL(String url){
        InputStream is = null;
        String result = "";
        JSONObject json = null;
        try{
            //pruebas para aumentar la velocidad
            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            HttpClient httpclient = new DefaultHttpClient(params);
            //HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        }catch(Exception e){
            int a=0;
            a=10;
        }

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result=sb.toString();
        } catch(Exception e){}

        try{
            json = new JSONObject(result);
        }catch(JSONException e)
        {

        }

        return json;
    }

    public static JSONObject getJSONfromURLPost(String url, String p1, String p2){
        InputStream is = null;
        String result = "";
        JSONObject json = null;
        try{
            //pruebas para aumentar la velocidad
            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            HttpClient httpclient = new DefaultHttpClient(params);
            //HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            List<NameValuePair> params2 = new ArrayList<NameValuePair>();
            params2.add(new BasicNameValuePair("names",p1));
            params2.add(new BasicNameValuePair("phone_numbers",p2));
            httppost.setEntity(new UrlEncodedFormEntity(params2));


            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        }catch(Exception e){
            int a=0;
            a=10;
        }

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result=sb.toString();
        } catch(Exception e){}

        try{
            json = new JSONObject(result);
        }catch(JSONException e)
        {

        }

        return json;
    }

}
